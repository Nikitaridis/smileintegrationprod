<?php
/**
 * The template for displaying full width pages.
 * Template Name: Full width
 * @package storefront
 */
?>

<?php get_header(); ?>

<?php
    // Page post object
    $Page = get_post();
    $PageMetaData = get_post_meta($Page->ID);
?>

<div class="row">
    <div class="twelve columns">

        <?php the_post_thumbnail( 'full' ); ?>

        <h1><?php echo $Page->post_title; ?></h1>

        <?php echo apply_filters('the_content', $Page->post_content); ?>

        <?php
            // Check form needs to be added
            if(isset($PageMetaData['ninja_forms_form'][0]) && $PageMetaData['ninja_forms_form'][0] > 0) {
                ninja_forms_display_form($PageMetaData['ninja_forms_form'][0]);
            }
        ?>

    </div>
</div>

<?php get_footer(); ?>