jQuery(document).ready(function ($) {

    var checkEmailButton = $('a.check-email');
    var emailMsisdnInput = $('input[name="recharge-emailMsisdn"]');
    var accountsSelect = $('select[name="recharge-account"]');
    var serviceSelect = $('select[name="recharge-service"]');
    var rechargeButton = $('button#recharge-submit');
    var error = $('#recharge-form .error');

    checkEmailButton.on('click', function (e) {
        e.preventDefault();

        getUserInformation();
    });

    emailMsisdnInput.keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            emailMsisdnInput.blur();
            getUserInformation();
            return false;
        }
    });

    accountsSelect.on('change', function () {
        addServicesToSelect();
    });

    function getUserInformation() {

        var successCallback = function (data) {
            $('#fullscreen-loader').css('display', 'none');
            var parsedData = JSON.parse(data);
            if (typeof parsedData.error === "undefined") {
                error.hide();
                if (parsedData.data !== "undefined") {
                    accountsSelect.prop('disabled', false);
                    serviceSelect.prop('disabled', false);
                    rechargeButton.prop('disabled', false);

                    accountsSelect.find('option').remove().end();

                    $.each(parsedData.data, function (index, value) {

                        var serviceArray = [];
                        $.each(value, function (serviceIndex, serviceValue) {
                            var tempServiceObj = {};
                            tempServiceObj.serviceICCID = serviceIndex;
                            tempServiceObj.accountHash = serviceValue.accountHash;
                            if (serviceValue.productFriendlyName) {
                                tempServiceObj.productFriendlyName = serviceValue.productFriendlyName;
                            }
                            tempServiceObj.productInstanceId = serviceValue.productInstanceId;
                            serviceArray.push(tempServiceObj);
                        });
                        var optionObj = $("<option/>").attr('data-services', JSON.stringify(serviceArray)).html(index);
                        accountsSelect.append(optionObj);
                    });

                    addServicesToSelect();
                } else {
                    showRechargeError('Something went wrong');
                }
            } else {
                showRechargeError(parsedData.error);
            }
        }

        var faliureCallback = function (errorThrown) {
            $('#fullscreen-loader').css('display', 'none');
            showRechargeError('Something went wrong')
        }

        if (emailMsisdnInput.val()) {
            $.ajax({
                url: WebsiteConfig.ajaxurl,
                method: 'POST',
                data: {
                    action: 'getRechargeAccounts',
                    email: emailMsisdnInput.val()
                },
                success: successCallback,
                error: faliureCallback
            });
        } else {
            showRechargeError("please enter either valid email address or msisdn")
        }
        $('#fullscreen-loader').css('display', 'block');
    }

    function addServicesToSelect() {
        serviceSelect.find('option').remove().end();

        var data = accountsSelect.find(':selected').data('services');
        $.each(data, function (index, value) {
            var tempServiceObj = {};
            tempServiceObj.accountHash = value.accountHash;
            tempServiceObj.productInstanceId = value.productInstanceId;
            var optionValue = JSON.stringify(tempServiceObj);
            var optionText = value.serviceICCID + ((value.productFriendlyName) ? (" (" + value.productFriendlyName + ")") : "");
            serviceSelect.append($("<option/>").html(optionText).val(optionValue));
        });
    }


    function showRechargeError(errorString) {

        var windowOverlayContent = $('.window-overlay-content');
        $('body').css('overflow', 'hidden');
        $('.window-overlay').addClass('window-open');
        windowOverlayContent.show();

        windowOverlayContent.addClass('window-overlay-centered-in-screen');
        windowOverlayContent.find('.recharge-error-popup').remove();
        windowOverlayContent.append('' +
            '<div class="recharge-error-popup"><h3>We don’t recognise your email address.</h3>' + RechargeConfig.unrecognizedEmailText + '</div>');

        windowOverlayContent.css("top", Math.max(0, (($(window).height() - windowOverlayContent.outerHeight()) / 2) +
            $(window).scrollTop()) + "px");
        windowOverlayContent.css("left", Math.max(0, (($(window).width() - windowOverlayContent.outerWidth()) / 2) +
            $(window).scrollLeft()) + "px");

        accountsSelect.find('option').remove().end();
        serviceSelect.find('option').remove().end();
        accountsSelect.prop('disabled', true);
        serviceSelect.prop('disabled', true);
        rechargeButton.prop('disabled', true);

        //error.show();
        //error.empty();
        //error.append(errorString);
    }
});