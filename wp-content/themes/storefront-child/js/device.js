jQuery(document).ready(function($) {

    if(typeof(Smile_Device) === "undefined") {
        Smile_Device = {};
    }

    Smile_Device.init = function() {

        // Populate dropdown with devices depending on selected brand
        $("select#deviceBrand").change(function(event) {

            $.ajax({
                url: WebsiteConfig.ajaxurl,
                method: 'POST',
                data: {
                    action: 'getDevicesByBrand',
                    brand: this.value
                },
                success:function(data) {

                    $("select#devicesByBrand option:gt(0)").remove().end();

                    var devicesSelect = $("select#devicesByBrand");

                    if(data.length == 0) return;

                    $.each(JSON.parse(data), function(index, value) {
                        devicesSelect.append(new Option(value.post_title, value.post_name));
                    });
                },
                error: function(errorThrown){
                }
            });
        }).trigger('change');


        // Show check when device is selected
        $("select#devicesByBrand").change(function(event) {

            if(this.value.length > 0) {

                if($(window).width() < 768) {
                    $('.devices-select-container .check-container p').css('display', 'block');
                }
                else {
                    $('.devices-select-container .check-container p').css('display', 'none');
                }

                $('.devices-select-container .check-container').css('display', 'inline');
            }
            else {
                $('.devices-select-container .check-container').css('display', 'none');
            }
        });
    };

    Smile_Device.init();
});