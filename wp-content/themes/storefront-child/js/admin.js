jQuery(document).ready(function($){
    var enquiry_smileservice_wrapper         = $(".enquiry_smileservice_input_fields");
    var enquiry_smileservice_add_button      = $("#enquiry-smileservice-add-input");

    var enquiry_bandwidth_wrapper         = $(".enquiry_bandwidth_input_fields");
    var enquiry_bandwidth_add_button      = $("#enquiry-bandwidth-add-input");

    var enquiry_locations_wrapper         = $(".enquiry_locations_input_fields");
    var enquiry_locations_add_button      = $("#enquiry-locations-add-input");

    $(enquiry_smileservice_add_button).click(function(e) {
        e.preventDefault();

        $(enquiry_smileservice_wrapper).append('<div><input type="text" name="enquiry_smileservice_item[]"><a href="#" class="remove_field">Remove</a><br><br></div>')
    });

    $(enquiry_smileservice_wrapper).on("click", ".remove_field", function(e) {
        e.preventDefault();
        $(this).parent('div').remove();
    });


    $(enquiry_bandwidth_add_button).click(function(e) {
        e.preventDefault();

        $(enquiry_bandwidth_wrapper).append('<div><input type="text" name="enquiry_bandwidth_item[]"><a href="#" class="remove_field">Remove</a><br><br></div>')
    });

    $(enquiry_bandwidth_wrapper).on("click", ".remove_field", function(e) {
        e.preventDefault();
        $(this).parent('div').remove();
    });


    $(enquiry_locations_add_button).click(function(e) {
        e.preventDefault();

        $(enquiry_locations_wrapper).append('<div><input type="text" name="enquiry_locations_item[]"><a href="#" class="remove_field">Remove</a><br><br></div>')
    });

    $(enquiry_locations_wrapper).on("click", ".remove_field", function(e) {
        e.preventDefault();
        $(this).parent('div').remove();
    });
});