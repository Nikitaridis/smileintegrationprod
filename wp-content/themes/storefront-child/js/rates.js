jQuery(document).ready(function($) {

    if(typeof(Smile_Rates) === "undefined") {
        Smile_Rates = {};
    }

    Smile_Rates.init = function() {

        // Populate dropdown with devices depending on selected brand
        $("select#country-rates-countries").change(function(event) {
			Smile_Rates.ajaxCall();           
        }).trigger('change');
        
         // Populate dropdown with devices depending on selected brand
        $("select#country-rates-bundles").change(function(event) {
			Smile_Rates.ajaxCall();           
        }).trigger('change');
    };
    
    Smile_Rates.ajaxCall =  function(){
    	$.ajax({
            url: WebsiteConfig.ajaxurl,
            method: 'POST',
            data: {
                action: 'getRateForCountry',
                country: $("select#country-rates-countries option:selected").text(),
                bundle: $("select#country-rates-bundles option:selected").text()
            },
            success:function(data) {

                $('#country-rate tbody').empty();

                var countryRate = $("#country-rate tbody");

                if(data.length == 0) return;

                $.each(JSON.parse(data), function(index, value) {
                    countryRate.append(
                        "<tr>" +
                        "<td>" +
                        value.SERVICE +
                        "</td>" +
                        "<td>" +
                        Math.round(value.PRICE) +
                        "</td>" +
                         "<td>" +
                        value.MiB +
                        "</td>" +
                        "</tr>");
                });
            },
            error: function(errorThrown){
            }
        });
    };

    Smile_Rates.init();
});