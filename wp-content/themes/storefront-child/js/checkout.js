jQuery(document).ready(function($) {
    var dateOfBirthContainer = $('#billing_date_of_birth_field');

    var billingNationality = $('#billing_nationality');
    var billingIdentificationType = $('#billing_identification_type');
    var billingState = $('#billing_state');
    var customBillingStates = $('#custom_billing_state');
    var billingZone = $('#billing_zone');

    var shippingState = $('#shipping_state');
    var customShippingStates = $('#custom_shipping_state');
    var shippingZone = $('#shipping_zone');

    if(dateOfBirthContainer) {
        dateOfBirthContainer.find('#billing_date_of_birth').remove();

        dateOfBirthContainer.birthdaypicker({
            dateFormat: "littleEndian",
            monthFormat: "long",
            fieldName: "billing_date_of_birth",
            fieldId: "billing_date_of_birth"
        });

        // dateOfBirthContainer.find('.birthdate').attr('id', 'billing_date_of_birth');
    }

    billingState.on('change', function() {
        var selectedOption = customBillingStates.find('option[value="' + this.value + '"]');
        selectedOption.prop('selected', 'selected');

        billingZone.find('option').remove().end();

        if(typeof selectedOption.data('zones') === 'undefined') {
            billingZone.prop('disabled', true);
        } else {
            billingZone.prop('disabled', false);
            $.each(selectedOption.data('zones'), function(index, value) {
                billingZone.append("<option value='" + value + "'>" + value + "</option>");
            });
        }
    }).change();

    shippingState.on('change', function() {
        var selectedOption = customShippingStates.find('option[value="' + this.value + '"]');
        selectedOption.prop('selected', 'selected');

        shippingZone.find('option').remove().end();

        if(typeof selectedOption.data('zones') === 'undefined') {
            shippingZone.prop('disabled', true);
        } else {
            shippingZone.prop('disabled', false);
            $.each(selectedOption.data('zones'), function(index, value) {
                shippingZone.append("<option value='" + value + "'>" + value + "</option>");
            });
        }
    }).change();

    billingNationality.find('option[value="' + CheckoutParams.country + '"]').prop('selected', 'selected');

    billingNationality.on('change', function () {
        var selectedOption = $(this).find("option:selected");
        if(selectedOption.val() === CheckoutParams.country) {
            billingIdentificationType.find("option:gt(0)").prop('disabled', false);
        } else {
            billingIdentificationType.find("option:gt(0)").prop('disabled', true);
            billingIdentificationType.find("option:eq(0)").prop('selected', 'selected');
        }
    }).change();
});
