jQuery(document).ready(function($) {

    /*----------------------------------------------------*/
    /* Home carousel
     ------------------------------------------------------ */
    $('.header-carousel').slick({
        autoplay:true,
        autoplaySpeed:5000,
        arrows:true,
        dots:true,
        draggable:true
    });

    var headerHeight = ($(window).height() * 1) - 0;   /* 70 = height nav bar */
    $('header .header-banner').css({ 'height': headerHeight });

    setCarouselImage($(window).width(), 0);

    // Change image dependent on screen width
    function setCarouselImage(width, slide) {

        if($('header .header-banner').length > 1) {
            slide = slide+1; // because slick makes a clone of the first slide so infinite loop is supported
        }

        $('header .header-banner').each(function(index, value) {

            // Always load the first image immediately
            if(index === slide) {

                // Check if background image exist.
                // Note: Background will not refresh when window is resized
                if($($(this)).css('background-image') === 'none') {

                    if(width < 480) {
                        $($(this)).css('background-image', 'url('+ $($(this)).attr('data-smartphone-image') +')');
                    }
                    else if(width < 768) {
                        $($(this)).css('background-image', 'url('+ $($(this)).attr('data-tablet-image') +')');
                    }
                    else {
                        $($(this)).css('background-image', 'url('+ $($(this)).attr('data-desktop-image') +')');
                    }
                }
            }
        });
    }


    // Load next frames only if they are about to appear
    $('.header-carousel').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        setCarouselImage($(window).width(), nextSlide);
    });


    $(window).on('resize', function() {
        headerHeight = ($(window).height() * 1) - 0;
        $('header .header-banner').css({ 'height': headerHeight });
        $('body').css({ 'width': $(window).width() });

        var currentSlide = $('.header-carousel').slick('slickCurrentSlide');
        setCarouselImage($(window).width(), currentSlide);
    });



//    Smooth Scrolling
    $(document).on('click', '.smoothscroll', function(e)
    {
        e.preventDefault();

        var target = this.hash,
            $target = jQuery(target);

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top + 0
        }, 800, 'swing', function () {
            window.location.hash = target;
        });
    });

    $('.no-scroll').on('touchmove', function(event) {
        event.preventDefault();
    });


//    Product block hover state
    $( "#products .product, #products .product-circle" ).hover(function() {
            $(this).addClass('hover');
        }, function() {
            $(this).removeClass('hover');
        }
    );


//    Product block clickable
    // Selector on all the id elements that start with 'product-item-'
    jQuery('#products [id^=product-item-]').on('click',function (e) {
        if(typeof($(this).find('a.link').attr('href')) !== 'undefined') {
            window.open($(this).find('a.link').attr('href'), '_self');
        }
    });


    // Home databundle clickable
    jQuery('#products .data-bundle-item').on('click',function (e) {
        var productId = $(this).find('[id^=product-item-]').attr('id');
        var url = $(this).find('[id^=product-item-]').data('databundles-page-url');
        if(productId && url) {
            window.open(url + '#' + productId, '_self');
        }
    });

    // Databundles page. Databundle row clickable
    jQuery('#data-bundles ul li.data-bundle-row').on('click',function (e) {

        // voor TZ en UG
        //if($(this).find('a').attr('href') !== 'undefined') {
        //    window.open($(this).find('a').attr('href'), '_blank');
        //    return false;
        //}

        var productItem = $(this).attr('id');
        if(productItem) {
            $(this).find('form[name="recharge-' + productItem +'"]').submit();
        }
    });

    // Databundles template page with anchor scroll a bit down
    if(jQuery('.page-template-template-data-bundles').length) {
        if(window.location.hash) {
            var productItem = window.location.hash;
            var navBarHeight = jQuery('#site-nav nav.main-nav').height();
            if($(this).find(productItem) && typeof(navBarHeight) === 'number') {
                navBarHeight = (navBarHeight > 0) ? navBarHeight : 60;
                $('html, body').animate({
                    scrollTop: $( $(this).find(productItem) ).offset().top - navBarHeight
                }, 500);
            }
        }
    }




    /*----------------------------------------------------*/
    /* Linkjes
     ------------------------------------------------------ */
    $('.open-with-href').click(function(e) {
        if(typeof($(this).find('a').attr('href')) !== 'undefined') {
            var target = $(this).find('a.link').attr('target');
            if(typeof(target) === 'undefined')
            {
                target = '_self';
            }

            window.open($(this).find('a').attr('href'), target);

            e.preventDefault();
        }
    });



    /*----------------------------------------------------*/
    /* Window overlay
     ------------------------------------------------------ */

    $('.open-window-overlay').on('click', function() {

        close_overlay();

        var windowOverlayContent = $('.window-overlay-content');

        $('body').css('overflow', 'hidden');
        $('.window-overlay').addClass('window-open');
        windowOverlayContent.show();
        $('.window-overlay-content > section').hide();
        $('.window-overlay-content #' + $(this).attr('data-overlay-identifier')).show();

        if($(this).hasClass('window-overlay-fill-screen')) {
            $('.window-overlay-content').addClass('window-overlay-fill-screen');
        }

        if($(this).attr('data-overlay-identifier') == 'coverage-map') {
            // Reset map to Nigeria
            Smile_Coverage.loadMap();
            Smile_Coverage.setSearchFieldValue('');
            Smile_Coverage.setCityFieldValue('');
        }


        windowOverlayContent.css("top", Math.max(0, (($(window).height() - windowOverlayContent.outerHeight()) / 2) +
                $(window).scrollTop()) + "px");
        windowOverlayContent.css("left", Math.max(0, (($(window).width() - windowOverlayContent.outerWidth()) / 2) +
                $(window).scrollLeft()) + "px");
    });

    $('.close-window-overlay').on('click', function() {
        close_overlay();
    });

    $('.window-overlay-content').on('click', function(event) {
        event.stopPropagation(); // Parent can't be clicked
    });

    $('.window-overlay-content .device-not-found').on('click', function(event) {
        //console.log('clicked device not found');
        $('.devices-list-container').hide();
        $('.devices-list-contact').show();
        event.preventDefault();
    });

    function close_overlay()
    {
        $('body').css('overflow', 'auto');
        $('.window-overlay').removeClass('window-open');

        $('.window-overlay-content').hide();
        $('#overlay-devices-list .devices-list-container').show();
        $('#overlay-devices-list .devices-list-contact').hide();
        $('#overlay-devices-list .ninja-forms-response-msg').hide();
        $('.window-overlay-content').removeClass('window-overlay-fill-screen');
        $('.to-bottom').removeClass('to-bottom');
    }

    $('.window-overlay, .window-overlay-content').on('touchmove', function(event) {
        event.preventDefault();
    });

    $('.window-overlay-content .dropdown').on('touchstart', function() {
        $(document).on('focus', 'textarea,input,select', function() {
            $('.window-overlay').css('position', 'absolute');
            $('.window-overlay').css('top', $(document).scrollTop() + 'px');
        }).on('blur', 'textarea,input,select', function() {
            $('.window-overlay').css('position', '');
            $('.window-overlay').css('top', '');
        });
    });

    /*----------------------------------------------------*/
    /* Home webcare
     ------------------------------------------------------ */
    $( "#webcare ul li a" ).hover(function() {
            $(this).addClass('hover');
        }, function() {
            $(this).removeClass('hover');
        }
    );


    /*----------------------------------------------------*/
    /* Coverage carousel
     ------------------------------------------------------ */
    $('.coverage-carousel').slick({
        autoplay:true,
        arrows:false,
        dots:true,
        draggable:true
    });


    /*----------------------------------------------------*/
    /* FAQ accordion
     ------------------------------------------------------ */
    function close_accordion_section() {
        $('.accordion .accordion-section-title').removeClass('active');
        $('.accordion .accordion-section-content').slideUp(300).removeClass('open');
    }

    $('.accordion-section-title').on('click', function(e) {
        // Grab current anchor value
        var currentAttrValue = $(this).attr('href');

        if($(e.target).is('.active')) {
            $(this).removeClass('active');
            $(this).siblings().slideUp(300).removeClass('open');

        }else {
            //close_accordion_section();

            // Add active class to section title
            $(this).addClass('active');
            // Open up the hidden content panel
            $('.accordion ' + currentAttrValue).slideDown(300).addClass('open');
        }

        e.preventDefault();
    });


    /*----------------------------------------------------*/
    /* Footer
     ------------------------------------------------------ */
    $('footer ul.socials li').on('click',function (e) {
        e.preventDefault();
        window.open($(this).find('a').attr('href'));
    });

    $('footer #newsletter button').on('click',function (e) {
        e.preventDefault();
    });


    /*----------------------------------------------------*/
    /* Mobile menu
     ------------------------------------------------------ */
    $('#site-nav button.mobile-btn').on('click',function (e) {
        $('body').append('<div class="mobile-nav-background"></div>');
        $('#site-nav nav.mobile-nav').css({
            'left' : '0',
            'display' : 'block'
        });
        $('body').addClass('no-scroll');
    });

    $('body').on('click', '#site-nav button.close-menu, .mobile-nav-background', function (e) {
        $('.mobile-nav-background').remove();
        $('#site-nav nav.mobile-nav').css({
            'left' : '-500px',
            'display' : 'none'
        });
        $('body').removeClass('no-scroll');
    });

    //$('#site-nav ul#menu-handheld-menu li.smile-menu-item a').on('click', function (event)
    //{
    //    $('.mobile-nav-background').remove();
    //    $('#site-nav nav.mobile-nav').css({
    //        'left' : '-500px',
    //        'display' : 'none'
    //    });
    //    $('body').removeClass('no-scroll');
    //});

    //$('#menu-handheld-menu li').click(function() {
    //    if(typeof($(this).find('a').attr('href')) !== 'undefined') {
    //        var target = $(this).find('a').attr('target');
    //        if(typeof(target) === 'undefined') {
    //            target = '_self';
    //        }
    //        $(this).addClass('clicked');
    //        window.open($(this).find('a').attr('href'), target);
    //    }
    //});

    $('#menu-handheld-menu li a').click(function(e) {
        if(typeof($(this).attr('href')) !== 'undefined') {
            var target = $(this).attr('target');
            if(typeof(target) === 'undefined') {
                target = '_self';
            }

            e.preventDefault();

            var parent = $(this).parent();
            var submenu = $(this).siblings('.sub-menu');
            if(submenu.length > 0) {
                if(parent.hasClass('expand')) {
                    $(this).addClass('clicked');
                    $('.mobile-nav-background').remove();
                    $('#site-nav nav.mobile-nav').css({
                        'left' : '-500px',
                        'display' : 'none'
                    });
                    $('body').removeClass('no-scroll');
                    window.open($(this).attr('href'), target);
                } else {
                    $('#menu-handheld-menu > li').removeClass('expand');
                    parent.addClass('expand');
                }
            } else {
                $(this).addClass('clicked');
                $('.mobile-nav-background').remove();
                $('#site-nav nav.mobile-nav').css({
                    'left' : '-500px',
                    'display' : 'none'
                });
                $('body').removeClass('no-scroll');
                window.open($(this).attr('href'), target);
            }
        }
    });


    /*----------------------------------------------------*/
    /* Careers section
     ------------------------------------------------------ */
    $('.mobile-search-jobs').click(function(e) {
        $(this).css('display', 'none');
        $('#career-search').css('display', 'block');
        e.preventDefault();
    });


    /*----------------------------------------------------*/
    /* Placeholder
     ------------------------------------------------------ */
    $('input, textarea').placeholder();



    /*----------------------------------------------------*/
    /* Webcare
     ------------------------------------------------------ */
    $('.open-chat').click(function(event){
        $zopim(function() {
            $zopim.livechat.window.toggle();
        });
        event.preventDefault();
    });




    /*----------------------------------------------------*/
    /* Product item page
     ------------------------------------------------------ */

    /**
     * Magnific Popup
     */

    // Find gallery images
    var galleryImages = [];
    $( "#main .product .five ul.gallery li a img" ).each(function( index, value ) {
        var obj = {
            src:$(value).attr('src'),
            type:'image'
        };
        galleryImages.push(obj);
    });

    if(galleryImages.length > 0) {

        // Show popup from featured image
        $('#main .product .five figure img').magnificPopup({
            items:galleryImages,
            type: 'image',
            gallery:{
                enabled: true,
                navigateByImgClick: true,
                arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
                tPrev: 'Previous (Left arrow key)',
                tNext: 'Next (Right arrow key)',
                tCounter: '<span class="mfp-counter">%curr% of %total%</span>'
            }
        });

        // Show popup from gallery
        $('#main .product .five ul.gallery li').magnificPopup({
            type: 'image',
            delegate: 'a',
            gallery:{
                enabled: true,
                navigateByImgClick: true,
                arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
                tPrev: 'Previous (Left arrow key)',
                tNext: 'Next (Right arrow key)',
                tCounter: '<span class="mfp-counter">%curr% of %total%</span>'
            }
        });
    }

    /*----------------------------------------------------*/
    /* Zopim show/hide on mobile
     ------------------------------------------------------ */
    var ua = navigator.userAgent.toLowerCase(),
		platform = navigator.platform.toLowerCase();
		platformName = ua.match(/ip(?:ad|od|hone)/) ? 'ios' : (ua.match(/(?:webos|android)/) || platform.match(/mac|win|linux/) || ['other'])[0],
		isMobile = /ios|android|webos/.test(platformName);
		if (!isMobile) {

		}
		else{
			if ($("body").hasClass("home")){
				$zopim(function() {
		            $zopim.livechat.hideAll();
		        });
				$( window ).scroll(function() {
					if($(window).width() <= 768 && $(window).scrollTop() < 50){
						// store Display toggle
						$zopim.livechat.hideAll();
					}else{
						$zopim.livechat.button.show();
					}
				});
			}else{
				$zopim(function() {
					$zopim.livechat.button.show();
		       	});
			}
		}


    /*----------------------------------------------------*/
    /* Analytics triggers
     ------------------------------------------------------ */
    // On element
    $('body').on('click', '.trigger', function() {
        var title = ($(this).attr('title')) ? $(this).attr('title') : $(this).data('trigger-title');
        var category = $(this).data('trigger-category');
        if(title === 'undefined' || category === 'undefined') {
            return;
        }
        ga('send', 'event', category, 'Click', title);
    });

    // On form submit
    $('body').on('submit', '.trigger', function() {
        var category = $(this).data('trigger-category');

        var values = '';
        $.each($(this).serializeArray(), function(i, field) {
            values = field.name + ' = ' + field.value + ' ';
        });

        if(values === '' || category === 'undefined') {
            return;
        }
        ga('send', 'event', category, 'Submit', values);
    });


    /*----------------------------------------------------*/
    /* Waypoints triggers
     ------------------------------------------------------ */

    if ($('#site-nav nav.main-nav li.smile-home-menu-item').hasClass('current_page_item') || $('#site-nav ul#menu-handheld-menu li.smile-home-menu-item').hasClass('current_page_item'))
    {
        $('#site-nav nav.main-nav li.current_page_item.smile-home-menu-item').addClass('selected_home_menu_item');

        //mobile
        $('#site-nav ul#menu-handheld-menu li.current_page_item.smile-home-menu-item').addClass('selected_home_menu_item');

        //smooth scroll
        $('#site-nav li.current_page_item .smile-menu-item a').each(function()
        {
            var href=$(this).attr("href");
            if(href.indexOf("#")>-1)
            {
                $(this).attr("href", href.substring(href.indexOf("#")));
                $(this).addClass('smoothscroll');
            }
        });
    }

    $('#products, #business').waypoint({
        handler: function(direction)
        {
            if (direction === 'down') {
                $('#site-nav nav.main-nav li.current_page_item li.smile-menu-item').removeClass('selected_home_menu_item');
                $('#site-nav nav.main-nav li.current_page_item li.smile-products-menu-item').addClass('selected_home_menu_item');

                //mobile
                $('#site-nav ul#menu-handheld-menu li.current_page_item li.smile-menu-item').removeClass('selected_home_menu_item');
                $('#site-nav ul#menu-handheld-menu li.current_page_item li.smile-products-menu-item').addClass('selected_home_menu_item');
            }else{
                $('#site-nav nav.main-nav li.current_page_item li.smile-menu-item').removeClass('selected_home_menu_item');

                //mobile
                $('#site-nav ul#menu-handheld-menu li.current_page_item li.smile-menu-item').removeClass('selected_home_menu_item');
            }
        }
    });

    $('#products, #business').waypoint({
        handler: function(direction)
        {
            if (direction === 'up') {
                $('#site-nav nav.main-nav li.current_page_item li.smile-menu-item').removeClass('selected_home_menu_item');
                $('#site-nav nav.main-nav li.current_page_item li.smile-products-menu-item').addClass('selected_home_menu_item');

                //mobile
                $('#site-nav ul#menu-handheld-menu li.current_page_item li.smile-menu-item').removeClass('selected_home_menu_item');
                $('#site-nav ul#menu-handheld-menu li.current_page_item li.smile-products-menu-item').addClass('selected_home_menu_item');
            } else {
                $('#site-nav nav.main-nav li.current_page_item li.smile-menu-item').removeClass('selected_home_menu_item');

                //mobile
                $('#site-nav ul#menu-handheld-menu li.current_page_item li.smile-menu-item').removeClass('selected_home_menu_item');
            }
        },
        offset: 'bottom-in-view'
    });

    // $('#coverage').waypoint({
    //     handler: function()
    //     {
    //         $('#site-nav nav.main-nav li.smile-menu-item').removeClass('selected_home_menu_item');
    //         $('#site-nav nav.main-nav li.smile-coverage-menu-item').addClass('selected_home_menu_item');
    //
    //         //mobile
    //         $('#site-nav ul#menu-handheld-menu li.smile-menu-item').removeClass('selected_home_menu_item');
    //         $('#site-nav ul#menu-handheld-menu li.smile-coverage-menu-item').addClass('selected_home_menu_item');
    //     }
    // });
    //
    // $('#coverage').waypoint({
    //     handler: function()
    //     {
    //         $('#site-nav nav.main-nav li.smile-menu-item').removeClass('selected_home_menu_item');
    //         $('#site-nav nav.main-nav li.smile-coverage-menu-item').addClass('selected_home_menu_item');
    //
    //         //mobile
    //         $('#site-nav ul#menu-handheld-menu li.smile-menu-item').removeClass('selected_home_menu_item');
    //         $('#site-nav ul#menu-handheld-menu li.smile-coverage-menu-item').addClass('selected_home_menu_item');
    //     },
    //     offset: 'bottom-in-view'
    // });

});
