jQuery(document).ready(function($) {

    if(typeof(Shop) === "undefined") {
        Shop = {
            numberOfResults: 3
        };
    }


    /**
     * @desc get user geo location
     */
    $( "#user-geo-location" ).click(function( event ) {

        // Try W3C Geolocation (Preferred)
        if(navigator.geolocation) {

            $('#fullscreen-loader').css('display', 'block');

            browserSupportFlag = true;
            navigator.geolocation.getCurrentPosition(function(position) {

                Shop.fetchShops('fetchShopsOnGeo', '', position.coords.latitude, position.coords.longitude);

            }, function() {
                handleNoGeolocation(browserSupportFlag);
            });
        }
        // Browser doesn't support Geolocation
        else {
            browserSupportFlag = false;
            handleNoGeolocation(browserSupportFlag);
        }

        event.preventDefault();
    });


    /**
     * @desc Handle geolocatoin failure
     * @param errorFlag
     */
    function handleNoGeolocation(errorFlag) {
        if (errorFlag == true) {
            alert("Geolocation service failed. Please enter your location in the field.");
        } else {
            alert("Your browser doesn't support geolocation. Please enter your location in the field.");
        }

        $('#fullscreen-loader').css('display', 'none');
    }


    // Seach form
    $( "form.find-shop-form" ).submit(function( event ) {

        var searchQuery = $('form.find-shop-form input').val();
        Shop.fetchShops('fetchShopsOnGeo', searchQuery, null, null);

        event.preventDefault();
    });



    // Handles clicks on the Nigeria image
    Shop.imageHandler = function() {

        $('#shops-info .page-image-container .city').click(function() {
            var panLocation = $(this).attr('data-pan-title');
            Shop.fetchShops('fetchShopsOnGeo', panLocation, null, null);
            $('#shops form input#search').val(panLocation);


            $('html, body').animate({
                scrollTop: $("#shops").offset().top - 80
            }, 1000);
        });

        //$('#shops-info .page-image-container .abuja, ' +
        //    '#shops-info .page-image-container .ibadan, ' +
        //    '#shops-info .page-image-container .lagos, ' +
        //    '#shops-info .page-image-container .benin-city, ' +
        //    '#shops-info .page-image-container .port-harcourt, ' +
        //    '#shops-info .page-image-container .kaduna, ' +
        //    '#shops-info .page-image-container .onitsha-asaba').click(function()
        //{
        //
        //    var panLocation = '';
        //    if($(this).hasClass('abuja')) {
        //        panLocation = 'Abuja';
        //    }
        //    else if($(this).hasClass('ibadan')) {
        //        panLocation = 'Ibadan';
        //    }
        //    else if($(this).hasClass('lagos')) {
        //        panLocation = 'Lagos';
        //    }
        //    else if($(this).hasClass('benin-city')) {
        //        panLocation = 'Benin City';
        //    }
        //    else if($(this).hasClass('port-harcourt')) {
        //        panLocation = 'Port Harcourt';
        //    }
        //    else if($(this).hasClass('kaduna')) {
        //        panLocation = 'Kaduna';
        //    }
        //    else if($(this).hasClass('onitsha-asaba')) {
        //        panLocation = 'Onitsha';
        //    }
        //    if(panLocation.length == 0) return;
        //
        //    Shop.fetchShops('fetchShopsOnGeo', panLocation, null, null);
        //    $('#shops form input#search').val(panLocation);
        //
        //
        //    $('html, body').animate({
        //        scrollTop: $("#shops").offset().top - 80
        //    }, 1000);
        //});
    }




    // Show all shops link
    $('#view-all-stores').click(function(event) {

        Shop.fetchShops('fetchAllShops', null, null);

        event.preventDefault();
    });


    // Unfold to show all shops
    $('section#shops').on('click', '.unfold-shops', function(event) {

        // Show all shops
        var shops = $('section#shops .shops-container .shop-container');
        $.each( shops, function( key, element ) {

            $($(element)).fadeIn("normal", function() {
                $($(element)).removeClass("hidden");
            });
        });

        // Remove show all link
        $($(this)).addClass('hidden');

        event.preventDefault();
    });


    // Show the form
    $('#show-search-form').click(function(event) {
        $('#shops .form-container form.find-shop-form, #shops .form-container p.view-all-stores').css('display', 'inline-block');
        $($(this)).parent().css('display', 'none');
        event.preventDefault();
    });

    $('a.find-shop').click(function(e) {
        $('html, body').animate({
            scrollTop: $("#shops").offset().top - 80
        }, 1000);
        e.preventDefault();
    });



    // Fetch the shops
    Shop.fetchShops = function(action, searchQuery, lat, lng) {
//    function fetchShops(action, searchQuery, lat, lng) {

        // Validate actions
        if(action !== 'fetchShopsOnGeo' && action !== 'fetchAllShops') {
            return;
        }

        $('#fullscreen-loader').css('display', 'block');

        // Find nearest shops
        $.ajax({
            url: ShopParams.ajaxurl,
            method: 'POST',
            data: {
                security: ShopParams.security,
                action: action,
                searchQuery: searchQuery,
                lat: lat,
                lng: lng,
                numberOfResults: Shop.numberOfResults
            },
            success:function(shopHtml) {

                $('section#shops .shops-container').html(shopHtml);
                $('#fullscreen-loader').css('display', 'none');
            },
            error: function(errorThrown){

                var errorMsg = '<div class="row">' +
                    '<div class="twelve columns">' +
                    '<p>Something went wrong. Please try another search</p>' +
                    '</div>' +
                    '</div>';

                $('section#shops .shops-container').html(errorMsg);
                $('#fullscreen-loader').css('display', 'none');
            }
        });
   };


    Shop.imageHandler();
    Shop.fetchShops('fetchAllShops');


});