jQuery(document).ready(function($) {

    if(typeof google !== 'object' || typeof google.maps !== 'object') {
        return;
    }

    if(typeof(Smile_Coverage) === "undefined") {
        Smile_Coverage = {
            Markers: [],
            LocationMarker: '',
            Infowindow: new google.maps.InfoWindow(),
            GPSLatitude: CoverageParams.maps.lat,
            GPSLongitude: CoverageParams.maps.lng,
            ZoomLevel: CoverageParams.maps.zoom,
            map: null,
            kml: CoverageParams.maps.kmz_url,
            kmlLayer: '',
            googleKey: CoverageParams.maps.google_key
        };
    }


    Smile_Coverage.loadMap = function(callback) {

        var map_canvas = document.getElementById('map');
        var map_options = {
            backgroundColor: '#e8e8e8',
            draggable: true,
            scrollwheel: true,
            streetViewControl: false,
            mapTypeControl: false,
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.RIGHT_BOTTOM
            },
            zoom: Smile_Coverage.ZoomLevel,
            center: new google.maps.LatLng(Smile_Coverage.GPSLatitude, Smile_Coverage.GPSLongitude),
            //center: new google.maps.LatLng(9.1414218,6.5212957), // Nigeria
            //center: new google.maps.LatLng(6.5481425,3.1173043), // Lagos
            mapTypeId: google.maps.MapTypeId.TERRAIN,
            styles: [
                {"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},
                {"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},
                {"featureType":"road.highway",elementType: 'geometry',"stylers":[{"color": "#333333"}, {"lightness":40},{"visibility":"on"}]},
                {"featureType":"road", elementType: 'labels', stylers: [{"visibility":"on"}]},
                {"featureType":"road.arterial",elementType: 'geometry',"stylers":[{"color":"#333333"},{"lightness":40},{"visibility":"on"}]},
                {"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},
                {"featureType":"administrative.province","stylers":[{"visibility":"off"}]},
                {"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"}, {"lightness":-25},{"saturation":-100}]},
                {"featureType":"water","elementType":"geometry","stylers":[{"hue":"#b0dcec"}, {"lightness":-50},{"saturation":97}]}]
        }

        Smile_Coverage.map = new google.maps.Map(map_canvas, map_options);
        //Smile_Coverage.loadFusiontable();
        Smile_Coverage.loadKML();
        Smile_Coverage.showForm();


        if (callback && typeof(callback) === "function") {
            callback.call(this);
        }
    }


    Smile_Coverage.loadFusiontable = function() {
        var world_geometry = new google.maps.FusionTablesLayer({
            query: {
                select: 'geometry',
                from: '1N2LBk4JHwWpOY4d9fobIn27lfnZ5MDy-NoqqRpk',
                where: "ISO_2DIGIT NOT IN ('NG')"
            },
            styles: [{
                polygonOptions: {
                    fillColor: '#000000',
                    fillOpacity: 0.1
                }
            }],
            map: Smile_Coverage.map,
            suppressInfoWindows: true
        });
    }


    Smile_Coverage.loadKML = function(){
        Smile_Coverage.kmlLayer = new google.maps.KmlLayer({
            url: Smile_Coverage.kml,
            preserveViewport: true,
            suppressInfoWindows:true,
            map: Smile_Coverage.map
        });
    }


    Smile_Coverage.showForm = function() {
        $('#coverage-map #form-container').removeClass('hidden');
    }


    Smile_Coverage.panTo = function(location, callback){

        if(location){
            // if it has a value
            var geoCodeAPI = "https://maps.googleapis.com/maps/api/geocode/json?key=" + Smile_Coverage.googleKey + "&address=" + location + ',+' + CoverageParams.country;
            $.getJSON( geoCodeAPI, {
                tagmode: "any",
                format: "json"
            })
                .done(function( data ) {

                    if(typeof(data) === 'object' && data.status == "OK") {
                        var lat = data.results[0].geometry.location.lat;
                        var lng = data.results[0].geometry.location.lng;
                        var LatLngBounds = new google.maps.LatLngBounds(data.results[0].geometry.viewport.southwest, data.results[0].geometry.viewport.northeast);

                        Smile_Coverage.map.fitBounds(LatLngBounds);
                        Smile_Coverage.map.panTo({lat: lat, lng: lng});
                        Smile_Coverage.showLocationMarker(new google.maps.LatLng(lat, lng));
                    }
                    else if(typeof(data) === 'object' && data.status == "ZERO_RESULTS") {
                        alert('Location could not be found. Please try another location');
                    }
                });
        }

        if (callback && typeof(callback) === "function") {
            callback.call(this);
        }
    }


    Smile_Coverage.showLocationMarker = function(LatLng) {

        // Remove old locationmarker
        if(typeof(Smile_Coverage.LocationMarker) === 'object') {
            Smile_Coverage.LocationMarker.setMap(null);
        }

        Smile_Coverage.LocationMarker = new google.maps.Marker({
            position: LatLng,
            animation: google.maps.Animation.DROP,
            draggable:false,
            icon: WebsiteConfig.ROOT_URL + '/images/map/location-marker.png'
        });
        Smile_Coverage.LocationMarker.setMap(Smile_Coverage.map);
    }


    /**
     * @desc get user geo location
     */
    Smile_Coverage.userGeoPos = function() {

        // Try W3C Geolocation (Preferred)
        if(navigator.geolocation) {

            $('#fullscreen-loader').css('display', 'block');

            browserSupportFlag = true;
            navigator.geolocation.getCurrentPosition(function(position) {

                Smile_Coverage.map.panTo({lat: position.coords.latitude, lng: position.coords.longitude});
                Smile_Coverage.showLocationMarker(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
                $('#fullscreen-loader').css('display', 'none');

            }, function() {
                Smile_Coverage.handleNoGeolocation(browserSupportFlag);
            });
        }
        // Browser doesn't support Geolocation
        else {
            browserSupportFlag = false;
            Smile_Coverage.handleNoGeolocation(browserSupportFlag);
        }
    }

    /**
     * @desc Handle geolocatoin failure
     * @param errorFlag
     */
    Smile_Coverage.handleNoGeolocation = function(errorFlag) {
        if (errorFlag == true) {
            alert("Geolocation service failed. Please enter your location in the field.");
        } else {
            alert("Your browser doesn't support geolocation. Please enter your location in the field.");
        }

        $('#fullscreen-loader').css('display', 'none');
    }



    Smile_Coverage.updateURL = function() {
        //history.pushState(null, null, WebsiteConfig.REQUEST_URI + '?location=' + locationInputField.val());
    }


    Smile_Coverage.formHandler = function() {

        // Handle panning from url param
        if(typeof(Coverage) === 'object' && Coverage.location.length > 0) {
            // @TODO
            //Smile_Coverage.panTo(Coverage.location);
        }

        // Form fields
        var citySelectField = $("select#coverage-city");
        var locationInputField = $("input[name='location']");

        citySelectField.change(function(event) {
            updateMap();
            event.preventDefault();
        });

        // Do not refresh page when press enter
        locationInputField.keypress(function(event) {
            if(event.keyCode == 10 || event.keyCode == 13) {
                updateMap();
                event.preventDefault();
            }
        });

        $("form#coverage-search button").click(function(event) {
            updateMap();
            event.preventDefault();
        });

        function updateMap() {
            var panLocation = locationInputField.val();
            if(citySelectField.val().length > 0) {
                panLocation += ',+' + citySelectField.val();
            }
            Smile_Coverage.panTo(panLocation);
            Smile_Coverage.updateURL();
        }


        // Fetch user GPS location
        $("form#coverage-search #use-my-location").click(function(event) {
            Smile_Coverage.userGeoPos();
            Smile_Coverage.updateURL();
            event.preventDefault();
        });
    }


    Smile_Coverage.setSearchFieldValue = function(value) {
        $("input[name='location']").val(value);
    }

    Smile_Coverage.setCityFieldValue = function(value) {
        $("select#coverage-city").val(value);
    }


    // Handles clicks on the Nigeria image
    Smile_Coverage.imageHandler = function() {

        $('#coverage .page-image-container .city').click(function(e) {
            e.stopPropagation();
            Smile_Coverage.setCityFieldValue($(this).attr('data-slug'));

            // Check if map already exists
            if(Smile_Coverage.map === null) {
                Smile_Coverage.loadMap();
            }
            Smile_Coverage.panTo($(this).attr('data-pan-title'));
        });
    };


    Smile_Coverage.imageHandler();
    Smile_Coverage.formHandler();
});