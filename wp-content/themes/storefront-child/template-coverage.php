<?php
/**
 * The template for displaying the coverage page
 *
 * Template Name: Coverage
 *
 * @package storefront-child
 */

wp_enqueue_script('smile-coverage-script', get_stylesheet_directory_uri() . '/js/coverage.js', array(), SMILE_VERSION, true);
wp_localize_script('smile-coverage-script', 'CoverageParams',
    array(
        'country' => COUNTRY,
        'maps' => array(
            'kmz_url' => MAPS_KMZ_URL,
            'lat' => MAPS_CENTER_LAT,
            'lng' => MAPS_CENTER_LNG,
            'zoom' => MAPS_CENTER_ZOOM,
            'google_key' => (defined('MAPS_API_KEY')?MAPS_API_KEY:'')
        )
    )
);
?>

<?php get_header();

$cities_utils = new Smile_Cities_Utils();
$cities_query = $cities_utils->getCitiesOnMapQuery();

?>


<section id="<?php echo get_post()->post_name; ?>" class="fill-screen">

    <div class="page-container row center-object">

        <div class="twelve columns section-title section-text-center">
            <h1><?php echo get_post()->post_title; ?></h1>
            <?php echo get_the_subtitle(); ?>
        </div>

        <div data-overlay-identifier="coverage-map" class="page-image-container open-window-overlay window-overlay-fill-screen six columns">
            <?php
            if($cities_query->have_posts()) {
                while($cities_query->have_posts()) :
                    $cities_query->the_post();
                    $pan_title = get_post_meta(get_the_ID(), 'smile_cities_pan_title', true);
                    $map_title = get_post_meta(get_the_ID(), 'smile_cities_map_title', true);
                    if(empty($map_title)) {
                        $map_title = get_the_title();
                    }
                    ?>
                    <div data-overlay-identifier="coverage-map" data-pan-title="<?php echo $pan_title; ?>" data-slug="<?php echo get_post()->post_name; ?>"
                         class="open-window-overlay window-overlay-fill-screen city <?php echo get_post()->post_name; ?>">
                        <div class="city-title"><?php echo $map_title; ?></div>
                        <img class="city-dot" src="<?php echo get_stylesheet_directory_uri(); ?>/images/map/city-dot.png"/>
                    </div>
                <?php endWhile;
                wp_reset_postdata();
            } ?>
            <?php echo get_the_post_thumbnail(); ?>
        </div>
        <div class="page-content-container six columns">
            <?php echo apply_filters('the_content', get_post()->post_content); ?>
            <a data-overlay-identifier="coverage-map" class="button open-window-overlay window-overlay-fill-screen">Check our coverage</a>
        </div>

    </div>
</section>


<?php
add_filter('smile_window_overlays', 'smile_window_overlay_coverage');


if (!function_exists('smile_window_overlay_coverage')) {
    function smile_window_overlay_coverage($overlays)
    {
        $location = '';
        if (isset($_GET['location']) && !empty($_GET['location'])) {
            $location = $_GET['location'];

            wp_localize_script('smile-coverage-script', 'Coverage',
                array(
                    'location' => $location
                )
            );
        }
        $coverage_query = new WP_Query(array(
            'name' => 'coverage',
            'post_type' => 'page',
            'post_status' => 'publish',
            'numberposts' => 1
        ));
        if ($coverage_query->have_posts()) {
            while ($coverage_query->have_posts()) {
                $coverage_query->the_post();
                ob_start();
                do_action('smile_coverage_map_html');
                $overlays[] = ob_get_clean();
            }
        }
        return $overlays;
    }
}


?>
<?php //do_action('smile_coverage_page'); ?>

<?php get_footer(); ?>
