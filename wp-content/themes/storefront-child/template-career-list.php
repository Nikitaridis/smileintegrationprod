<?php
/**
 * The template for careers
 * Template Name: Career list
 * @package storefront
 */
?>

<?php get_header();

$Page = get_post();

$Smile_Jobs_Search = new Smile_Jobs_Search();
$jobs = $Smile_Jobs_Search->getSearchResult();

$Smile_Jobs = new Smile_Jobs();
$departments = $Smile_Jobs->getTerms(Smile_Jobs_Admin::TAX_DEPARTMENT_KEY);
$locations = $Smile_Jobs->getTerms(Smile_Jobs_Admin::TAX_LOCATION_KEY);
?>

    <section id="<?php echo $Page->post_name; ?>" class="career-listing narrow big-title">
        <div class="row center-object">
            <div class="twelve columns section-title section-text-center">
                <h1><?php echo $Page->post_title; ?></h1>
            </div>

            <div class="twelve columns">
                <a href="" class="button mobile-search-jobs">Search all jobs</a>
                <form id="career-search" method="GET" name="career-search">
                    <div>
                        <input type="text" placeholder="Enter Keyword" id="q" name="q" value="<?php echo $Smile_Jobs_Search->getSearchQuery(); ?>">
                    </div>
                    <div>

                        <select id="department" name="department">
                            <option value="all-departments">All Departments</option>
                            <?php
                            foreach($departments as $key => $department) {
                                $selected = (strtolower($department->slug) == strtolower($Smile_Jobs_Search->getDepartmentQuery())) ? 'selected' : '';
                                echo '<option '. $selected .' value="'. $department->slug .'">'. $department->name .'</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div>
                        <select id="location" name="location">
                            <option value="all-locations">All Locations</option>
                            <?php
                            foreach($locations as $key => $location) {
                                $selected = (strtolower($location->slug) == strtolower($Smile_Jobs_Search->getLocationQuery())) ? 'selected' : '';
                                echo '<option '. $selected .' value="'. $location->slug .'">'. $location->name .'</option>';
                            }
                            ?>

                        </select>
                    </div>
                    <div class="submit-container">
                        <button class="button" type="submit" form="career-search"><span class="button-txt">Search all jobs</span><span class="icon-search"></span></button>
                    </div>
                </form>

            </div>
            <!--            <div class="twelve columns">-->
            <!--                --><?php //echo apply_filters('the_content', $Page->post_content); ?>
            <!--            </div>-->

            <?php if(count($jobs)) { ?>
                <div class="twelve columns">
                    <ul class='careers-list'>
                        <li class="careers-list-title">
                            <div class="careers-list-column four columns">Job title</div>
                            <div class="careers-list-column four columns">Department</div>
                            <div class="careers-list-column four columns">Location</div>
                            <div style="clear: both"></div>
                        </li>
                        <?php
                        foreach ($jobs as $key => $job) {
                            $jobDepartment = $Smile_Jobs->getPostFirstTerm($job->ID, Smile_Jobs_Admin::TAX_DEPARTMENT_KEY, 'name');
                            $jobLocation = $Smile_Jobs->getPostFirstTerm($job->ID, Smile_Jobs_Admin::TAX_LOCATION_KEY, 'name');
                            ?>
                            <li id="<?php echo $job->post_name; ?>" class="careers-list-container open-with-href collapsed">
                                <a href="<?php echo get_permalink($job->ID); ?>">
                                    <div class="careers-list-column four columns">
                                        <span><?php echo $job->post_title; ?></span>
                                    </div>
                                    <div class="careers-list-column four columns">
                                        <span><?php echo $jobDepartment; ?></span>
                                    </div>
                                    <div class="careers-list-column four columns">
                                        <span><?php echo $jobLocation; ?></span>
                                    </div>
                                    <div style="clear: both"></div>
                                </a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
            <?php } else { ?>
                <div class="twelve columns no-search-results">
                    <div class="">No results for your search criteria</div>
                </div>
            <?php } ?>
        </div>
    </section>

<?php get_footer(); ?>