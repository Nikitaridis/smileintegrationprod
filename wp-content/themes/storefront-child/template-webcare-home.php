<?php
/**
 * The template for displaying webcare homepage
 * Template Name: Webcare home
 * @package storefront
 */
?>

<?php require_once( WP_PLUGIN_DIR . '/smile-faq/admin/class-smile-faq-ordering.php' ); ?>

<?php get_header(); ?>

<?php
    // Page post object
    $Page = get_post();
    $PageMetaData = get_post_meta($Page->ID);
?>

<section id="webcare">

    <div class="row intro">
        <div class="eight columns section-title">
            <h1><?php echo $Page->post_title; ?></h1>
            <?php echo apply_filters('the_content', $Page->post_content); ?>
        </div>
        <div class="four columns"></div>
    </div>

    <div class="row">
        <div class="eight columns collapsed">

            <?php
                $ordering = new Smile_FAQ_Ordering();
                // Fetch FAQ categories
                $tax_terms = $ordering->redo_ordering();//get_terms(array('faq_category'));
                $FAQ_block = '';
                foreach($tax_terms as $key => $term) {

                    $url = add_query_arg( 'category', $term->slug, PageType::getPageUrl('knowledgebase'));
                    $FAQ_block .= '
                        <div class="six columns">
                            <div class="block open-with-href">
                                <h3>'. $term->name .'</h3>
                                <p class="description">'. $term->description .'</p>
                                <a class="trigger" data-trigger-category="Knowledgebase" href="' . $url . '" title="'. $term->name .' Learn more">Learn more</a>
                            </div>
                        </div>';
                }
            echo $FAQ_block;
            ?>

        </div>

        <div class="four columns right-column">

            <?php do_action('smile_webcare_menu') ?>

            <?php the_post_thumbnail( 'full' ); ?>

        </div>
    </div>

</section>

<?php get_footer(); ?>
