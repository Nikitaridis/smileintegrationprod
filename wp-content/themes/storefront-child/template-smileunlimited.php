<?php
/**
 * The template for displaying the SmileUnlimited page
 * Template Name: SmileUnlimited
 * @package storefront
 */
?>

<?php get_header(); ?>

<?php
// Page post object
$Page = get_post();
$PageMetaData = get_post_meta($Page->ID);

//$isBusiness = PageType::isPageType('business');
//$PlatformMeta = $isBusiness ? 'business' : 'consumer';
$Category = get_term_by('slug', 'data-bundle', 'product_cat');

$ProductObj = new Product();
$ProductObj->setTaxQuery('product_cat', 'term_id', $Category->term_id, 'IN');
$ProductObj->setTaxQuery('product_visibility', 'slug', 'exclude-from-catalog', 'NOT EXISTS');
//$ProductObj->setMetaQuery('_visibility', array('catalog', 'visible'), 'IN');
//$ProductObj->setMetaQuery('smile_platform_type', $PlatformMeta, '=');
$Products = $ProductObj->getProduct(4);
?>
<section id="smileunlimited" class="extra-margin">
    <div class="row">
        <div class="twelve columns section-title section-text-center">
            <h1><?php echo $Page->post_title; ?></h1>
            <?php echo the_subtitle(); ?>
        </div>
    </div>

    <div class="row">
        <div class="six columns">
            <?php the_post_thumbnail( 'full' ); ?>
        </div>

        <div class="six columns">
            <?php echo apply_filters('the_content', $Page->post_content); ?>
        </div>
    </div>
</section>

<?php if ( $Products->have_posts() ) : ?>
    <section id="products">
        <div class="products-block<?php echo ' ' . $Category->slug ?> fill-screen">

        <div class="row">
            <div class="twelve columns section-title section-text-center">
                <h1><?php echo $Category->name; ?></h1>
            </div>
            <?php while ( $Products->have_posts() ) : $Products->the_post(); ?>

                <?php
                    include(locate_template('inc/template/product/display-data-bundle.php'));
                ?>

            <?php endwhile; ?>
        </div>

        <div id="data-bundles">
            <div class="row bundles">
                <div class="six columns offset-3">
                    <a class="all-bundles" href="<?php echo $isBusiness ? PageType::getPageUrl('data-bundles-business') : PageType::getPageUrl('data-bundles'); ?>">Show me the full list of data plans</a>
                    <div class="support">
                        <p>Can’t find what you are looking for?</p>
                        <a href="" class="open-chat">Chat with us (24/7)</a>
                    </div>
                </div>
            </div>
        </div>

        </div>
    </section>

<?php endif; ?>

<?php get_footer(); ?>