<?php
/**
 * The template for displaying the shops page.
 *
 * Template Name: Shops
 *
 * @package storefront-child
 */
?>

<?php
wp_enqueue_script('smile-shop-script', get_stylesheet_directory_uri() . '/js/shop.js', array(), SMILE_VERSION, true);
wp_localize_script('smile-shop-script', 'ShopParams',
    array(
        'ajaxurl' => admin_url( 'admin-ajax.php', 'relative'),
        'security' => wp_create_nonce( "ajax_fetchShops" )
    )
);
?>

<?php get_header(); ?>

<?php do_action('smile_shop_page'); ?>

<?php get_footer(); ?>
