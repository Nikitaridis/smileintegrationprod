<?php
/**
 * The template for careers
 * Template Name: Career main
 * @package storefront
 */
?>

<?php get_header();

$Page = get_post();

$Smile_Jobs = new Smile_Jobs();
$departments = $Smile_Jobs->getTerms(Smile_Jobs_Admin::TAX_DEPARTMENT_KEY);

?>

    <section id="<?php echo $Page->post_name; ?>" class="fill-screen narrow">
        <div class="row center-object">
            <div class="twelve columns section-title section-text-center section-text-small">
                <h1><?php echo $Page->post_title; ?></h1>

                <div class="career-search-banner">
                    <h3>Search all listings</h3>
                    <a href="list" class="button">Find a job</a>
                </div>
            </div>
            <div class="ten columns offset-1 section-text-center">
                <?php echo apply_filters('the_content', $Page->post_content); ?>
            </div>
            <div class="twelve columns">
                <ul class='block-list'>
                <?php
                foreach ($departments as $key => $department) {
                    ?>
                    <li id="<?php echo $department->slug; ?>" class="container open-with-href six columns collapsed">
                        <div class="inner">
                            <h3><?php echo $department->name; ?></h3>
                            <div class="subtitle"><?php echo $department->description; ?></div>
                            <a class="link" href="list?department=<?php echo $department->slug; ?>"><?php echo $department->count . ' positions'; ?></a>
                        </div>
                    </li>
                    <?php
                }
                ?>
                </ul>
            </div>
        </div>
    </section>

<?php get_footer(); ?>