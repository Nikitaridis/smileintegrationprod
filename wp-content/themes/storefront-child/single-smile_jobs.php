<?php
/**
 * The template for displaying smile_jobs pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package storefront-child
 *
 */
?>

<?php get_header();

$Page = get_post();

$Smile = new Smile_Jobs();
$jobDepartment = $Smile->getPostFirstTerm(get_the_ID(), Smile_Jobs_Admin::TAX_DEPARTMENT_KEY, 'name');
$jobLocation = $Smile->getPostFirstTerm(get_the_ID(), Smile_Jobs_Admin::TAX_LOCATION_KEY, 'name');

?>

    <article id="<?php echo $Page->post_name; ?>" class="fill-screen no-image-banner narrow">

        <div class="row center-object">

            <div class="twelve columns section-title">
                <h2><?php echo get_post()->post_title; ?></h2>
            </div>

            <div class="nine columns">
                <?php echo apply_filters('the_content', get_post()->post_content); ?>

                <div class="five columns collapsed apply-button">
                    <a href="mailto:<?php echo CAREER_EMAIL; ?>?Subject=Job application: <?php echo get_post()->post_title; ?>" class="button trigger" data-trigger-category="Career apply" data-trigger-title="<?php echo get_post()->post_title; ?>" title="Apply now">Apply now</a>
                    <p>Send a copy of your CV</p>
                </div>

            </div>
            <div class="three columns side-extra-outer grey-background">
                <div class="side-extra-inner">

                    <?php if(!empty($jobLocation)) : ?>
                        <div class="twelve columns label">Location:</div>
                        <div class="twelve columns value"><?php echo $jobLocation; ?></div>
                    <?php endif; ?>

                    <?php if(!empty(get_post_meta(get_the_ID(), 'smile_jobs_employmenttype', true))) : ?>
                        <div class="twelve columns label">Employment type:</div>
                        <div class="twelve columns value"><?php echo get_post_meta(get_the_ID(), 'smile_jobs_employmenttype', true); ?></div>
                    <?php endif; ?>

                    <?php if(!empty($jobDepartment)) : ?>
                        <div class="twelve columns label">Department:</div>
                        <div class="twelve columns value"><?php echo $jobDepartment; ?></div>
                    <?php endif; ?>

                    <?php if(!empty(get_post_meta(get_the_ID(), 'smile_jobs_closingdate', true))) : ?>
                        <div class="twelve columns label">Closing date:</div>
                        <div class="twelve columns value"><?php echo get_post_meta(get_the_ID(), 'smile_jobs_closingdate', true); ?></div>
                    <?php endif; ?>

                </div>
            </div>

            <div class="three columns collapsed apply-button">
                <a href="mailto:<?php echo CAREER_EMAIL; ?>?Subject=Job application: <?php echo get_post()->post_title; ?>" class="button trigger" data-trigger-category="Career apply" data-trigger-title="<?php echo get_post()->post_title; ?>" title="Apply now">Apply now</a>
                <p>Send a copy of your CV</p>
            </div>

        </div>

    </article>


<?php get_footer(); ?>