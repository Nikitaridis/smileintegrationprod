<?php
/**
 * The template for displaying 2 column pages.
 * Template Name: 2 columns
 * @package storefront
 */
?>

<?php get_header(); ?>

<?php
    // Page post object
    $Page = get_post();
    $PageMetaData = get_post_meta($Page->ID);
?>

<div class="row">
    <div class="twelve columns">
        <h1><?php echo $Page->post_title; ?></h1>
    </div>
    <div class="seven columns">
        <?php echo apply_filters('the_content', $Page->post_content); ?>

        <?php
            // Check form needs to be added
            if(isset($PageMetaData['ninja_forms_form'][0]) && $PageMetaData['ninja_forms_form'][0] > 0) {
                ninja_forms_display_form($PageMetaData['ninja_forms_form'][0]);
            }
        ?>

    </div>
    <div class="five  columns">
        <?php the_post_thumbnail( 'full' ); ?>
    </div>
</div>

<?php get_footer(); ?>
