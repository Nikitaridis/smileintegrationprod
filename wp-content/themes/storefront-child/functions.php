<?php
/**
 * Smile - Child theme
 *
 * storefront engine room
 *
 * @package storefront
 */

/**
 * Initialize all the things.
 */
require get_stylesheet_directory() . '/inc/init.php';

function maintenance_redirect(){
    if( !is_user_logged_in() ){
        wp_redirect( site_url( 'maintenance.html' ), 302 );
        exit();
    }
}
//add_action( 'init', 'maintenance_redirect' );

if(!function_exists('is_post_empty')) {
    function is_post_empty($post) {
        return isset($_POST[$post]) && empty($_POST[$post]);
    }
}

if(!function_exists('is_post_not_empty')) {
    function is_post_not_empty($post) {
        return isset($_POST[$post]) && !empty($_POST[$post]);
    }
}

if (!function_exists('write_log')) {
    function write_log ( $log )  {
        if ( true === WP_DEBUG ) {
            if ( is_array( $log ) || is_object( $log ) ) {
                error_log( print_r( $log, true ) );
            } else {
                error_log( $log );
            }
        }
    }
}

/**
 * Remove Woocommerce Select2 - Woocommerce 3.2.1+
 */
if (!function_exists('woo_dequeue_select2')) {
    function woo_dequeue_select2()
    {
        if (class_exists('woocommerce')) {
            wp_dequeue_style('select2');
            wp_deregister_style('select2');

            wp_dequeue_script('selectWoo');
            wp_deregister_script('selectWoo');
        }
    }
}
add_action( 'wp_enqueue_scripts', 'woo_dequeue_select2', 100 );

/** Remove categories from shop and other pages
 * in Woocommerce
 */
function wc_hide_selected_terms( $terms, $taxonomies, $args ) {
    $new_terms = array();
    if ( in_array( 'product_cat', $taxonomies ) && !is_admin() && is_shop() ) {
        foreach ( $terms as $key => $term ) {
            if ( ! in_array( $term->slug, array( 'uncategorized' ) ) ) {
                $new_terms[] = $term;
            }
        }
        $terms = $new_terms;
    }
    return $terms;
}
add_filter( 'get_terms', 'wc_hide_selected_terms', 10, 3 );

?>
