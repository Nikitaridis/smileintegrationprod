<?php
/**
 * The template for displaying a business sub page
 * Template Name: Business sub page
 * @package storefront
 */
?>

<?php get_header(); ?>

<?php
// Page post object
$Page = get_post();
$PageMetaData = get_post_meta($Page->ID);
?>
<section id="business-sub" class="extra-margin">
    <div class="row">
        <div class="twelve columns section-title section-text-center">
            <h1><?php echo $Page->post_title; ?></h1>
            <?php echo the_subtitle(); ?>
        </div>
    </div>

    <div class="row">
        <div class="six columns">
            <?php the_post_thumbnail( 'full' ); ?>
        </div>

        <div class="six columns">
            <?php echo apply_filters('the_content', $Page->post_content); ?>
        </div>
    </div>
</section>

<section id="business-sub-how-can-we-help-you">
    <div class="row">
        <div class="twelve columns section-title section-text-center">
            <h1>How can we help you?</h1>
        </div>
    </div>

    <ul class="business-sub-links block-list wide row">
        <li class="container four columns">
            <a href="#" class="open-chat trigger" data-trigger-category="Business-sub-link" title="Chat with us">
                <div class="inner">
                    <h3>Chat with us</h3>
                    <p class="subtitle">Please chat with one of our business experts</p>
                </div>
            </a>
        </li>
        <li class="container four columns">
            <a href="<?php echo PageType::getPageUrl('business-enquiry-form'); ?>" class="trigger" data-trigger-category="Business-link" title="How can we help you?">
                <div class="inner">
                    <h3>How can we help you?</h3>
                    <p class="subtitle">Please click here to leave your details and we will be in contact within the hour</p>
                </div>
            </a>
        </li>
        <li class="container four columns">
            <a href="<?php echo get_home_url();?>/help/knowledgebase/?category=smilebusiness" class="trigger" data-trigger-category="Business-link" title="View Knowledge Base">
                <div class="inner">
                    <h3>SmileBusiness FAQ</h3>
                    <p class="subtitle">Read more about the solutions from SmileBusiness in our FAQs</p>
                </div>
            </a>
        </li>
    </ul>
</section>

<?php get_footer(); ?>