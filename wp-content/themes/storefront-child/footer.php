<?php
/**
 * The template for displaying the footer.
 *
 * @package storefront
 */
?>

<?php do_action( 'storefront_before_footer' ); ?>



        </div><!-- .col-full -->
    </div><!-- #content -->

    <footer>

        <?php do_action( 'smile_footer' ); ?>

    </footer><!-- Footer End-->

</div><!-- #page -->


<div class="window-overlay close-window-overlay">

</div>

<div class="window-overlay-content">
    <span class="close-window-overlay icon-cross"></span>
    <?php
    $window_overlays = apply_filters('smile_window_overlays', array());
    foreach($window_overlays as $overlay)
    {
        echo $overlay;
    }
    ?>
</div>

<?php wp_footer(); ?>

<?php do_action('smile_google_adwords'); ?>
<?php do_action('smile_google_analytics'); ?>
<?php do_action('smile_google_analytics_ecommerce'); ?>
<?php do_action('smile_google_analytics_conversion_tracking'); ?>

</body>
</html>