<?php
?>

<?php get_header(); ?>

<?php if ( $order ) :
    $order_items = $order->get_items();?>
    <p>Your XpressRecharge transaction was successful.</p>
    <ul class="order_details">
        <li class="order">
            <?php _e( 'Order Number:', 'woocommerce' ); ?>
            <strong><?php echo $order->get_order_number(); ?></strong>
        </li>
        <li class="date">
            <?php _e( 'Date:', 'woocommerce' ); ?>
            <strong><?php echo date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ); ?></strong>
        </li>
        <li class="total">
            <?php _e( 'Total:', 'woocommerce' ); ?>
            <strong><?php echo $order->get_formatted_order_total(); ?></strong>
        </li>
        <?php if ( $order->payment_method_title ) : ?>
            <li class="method">
                <?php _e( 'Payment Method:', 'woocommerce' ); ?>
                <strong><?php echo $order->payment_method_title; ?></strong>
            </li>
        <?php endif; ?>
    </ul>
    <div class="clear"></div>

    <p>Your <?php echo reset($order_items)['name'];?> data bundle is immediately available.</p>
    <p>You will receive a confirmation and proof of payment of this transaction, sent to your registered Smile email address.</p>
    <p>Thank you for choosing Smile!</p>
<?php endif; ?>

<?php get_footer(); ?>
