<?php
/**
 * The template for displaying webcare pages.
 * Template Name: Webcare
 * @package storefront
 */
?>

<?php get_header(); ?>

<?php
    // Page post object
    $Page = get_post();
    $PageMetaData = get_post_meta($Page->ID);
?>

<div class="webcare-questions">
    <div class="row">
        <div class="twelve columns section-title section-text-center">
            <h1><?php echo $Page->post_title; ?></h1>
            <p><?php echo $Page->post_content; ?></p>
        </div>

        <div class="eight columns">

            <?php
            /* @see smile_webcare_faq() */
            do_action('smile_webcare_faq');
            ?>

            <?php
                // Check form needs to be added
                if(isset($PageMetaData['ninja_forms_form'][0]) && $PageMetaData['ninja_forms_form'][0] > 0) {
                    ninja_forms_display_form($PageMetaData['ninja_forms_form'][0]);
                }
            ?>

        </div>
        <div class="four columns">

            <?php
            /* @see  smile_webcare_menu() */
            do_action('smile_webcare_menu')
            ?>

            <?php the_post_thumbnail( 'full' ); ?>

        </div>
    </div>
</div>


<?php get_footer(); ?>
