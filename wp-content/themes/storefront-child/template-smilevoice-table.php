<?php
/**
 * The template for displaying the table on the smilevoice page
 * Template Name: SmileVoice Table
 * @package storefront
 */
?>

<?php
// Page post object
$Page = get_post();
$isBusiness = PageType::getTopParentPageType() === 'business';
$termsConditionsPageType = $isBusiness ? 'terms-and-conditions-products-business' : 'terms-and-conditions-products';

$args = array(
    'post_status' => 'publish',
    'post_type' => 'product',
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'posts_per_page' => -1
);
$products = new WP_Query($args);
$smileRates = new Smile_International_Rates();
$countries = $smileRates->getCountries();
$bundles = $smileRates->getBundles();

wp_enqueue_script('smile-rates-script', get_stylesheet_directory_uri() . '/js/rates.js', array(), SMILE_VERSION, true);
?>

<div class="row" xmlns="http://www.w3.org/1999/html">
    <div class="twelve columns section-title section-text-center">
        <h1><?php echo $Page->post_title; ?></h1>
        <?php echo the_subtitle(); ?>
    </div>
</div>

<div class="row smilevoice-table-container">
    <div class="twelve columns">
        <table class="smilevoice-table">
            <thead>
            <tr>
                <th rowspan="2">SmileData plan</th>
                <th rowspan="2">Validity period</th>
                <th rowspan="2">Plan price (<?php echo get_woocommerce_currency_symbol(); ?>)</th>
                <th rowspan="1">SmileVoice call rate</th>
                <th rowspan="2"><?php echo BUNDLE_VOICE_RATES_HEADER; ?></th>
                <th rowspan="1">SmileVoice SMS rate</th>
            </tr>
            <tr>
                <th>MB/minutes</th>
                <th>MB/SMS</th>
            </tr>
            </thead>
            <tbody>
            <?php if ($products->have_posts()):
                while ($products->have_posts()):
                    $products->the_post();
                    if (!empty(wc_get_product_terms(get_the_ID(), 'pa_data-bundle-title', null))):
                        $linkedProduct = new WC_Product(get_the_ID());

                        if (empty($linkedProduct->get_attribute('pa_data-bundle-voice-call-rate')) ||
                            empty($linkedProduct->get_attribute('pa_data-bundle-voice-mb-minute')) ||
                            empty($linkedProduct->get_attribute('pa_data-bundle-voice-mb-sms')) ||
                            empty($linkedProduct->get_attribute('pa_data-bundle-title')) ||
                            !(get_post_meta($linkedProduct->get_id(), '_regular_price', true))
                        ) {
                            continue;
                        }
                        $circleColor = $linkedProduct->get_attribute('pa_data-bundle-color');
                        ?>

                        <tr class="<?php echo $circleColor;?>">
                            <td><span class="responsive-header">SmileData plan</span><?php echo $linkedProduct->get_title();?></td>
                            <td><span class="responsive-header">Validity period</span><?php echo $linkedProduct->get_attribute('pa_data-bundle-validity');?></td>
                            <td><span class="responsive-header">Plan price (<?php echo get_woocommerce_currency_symbol(); ?>)</span><?php echo get_woocommerce_currency_symbol(); ?><?php echo wc_get_price_to_display( $linkedProduct );?></td>
                            <td><span class="responsive-header">MB/minutes</span><?php echo $linkedProduct->get_attribute('pa_data-bundle-voice-mb-minute');?> MB/min</td>
                            <td><span class="responsive-header"><?php echo BUNDLE_VOICE_RATES_HEADER; ?></span><?php echo ($linkedProduct->get_attribute('pa_data-bundle-voice-call-rate') . ' ' . BUNDLE_VOICE_RATES_SYMBOL); ?></td>
                            <td><span class="responsive-header">MB/SMS</span><?php echo $linkedProduct->get_attribute('pa_data-bundle-voice-mb-sms');?> MB/SMS</td>
                        </tr>
                    <?php
                    endif;
                endwhile;
            endif; ?>
            </tbody>
        </table>
        <div class="smilevoice-table-info">
        <?php echo apply_filters('the_content', $Page->post_content); ?>
        </div>
    </div>
</div>

<div class="row">
    <div id="smilevoice-country-rates-title-container" class="twelve columns section-title section-text-center">
        <?php if($isBusiness):?>
            <h1>International call rates</h1>
            With SmileVoice you can make calls to anyone in the world. To view the call rate per second and MBs that will be charged to your active data plan, please select the country you want to call to and your active data plan.
        <?php else:?>
            <h1>International call rates</h1>
            With SmileVoice you can make calls to anyone in the world. To view the call rate per second and MBs that will be charged to your active data plan, please select the country you want to call to and your active data plan.
        <?php endif;?>
    </div>
</div>


<div id="smilevoice-country-rates" class="row">
    <div class="twelve columns">
        <div id="smilevoice-country-rates-select-container">
            <select id="country-rates-countries">
                <?php foreach($countries as $country):?>
                    <option value="<?php echo $country->country; ?>"><?php echo $country->country; ?></option>
                <?php endforeach;?>
            </select>
            <select id="country-rates-bundles">
                <?php foreach($bundles as $bundle):?>
                    <option value="<?php echo $bundle->bundle; ?>"><?php echo $bundle->bundle; ?></option>
                <?php endforeach;?>
            </select>
            <?php if($isBusiness):?>
            <div class="info">SMEUnlimited cannot be used to make SmileVoice calls</div>
          	<?php else:?>
          	<div class="info">SmileUnlimited and MidNite plans cannot be used to make SmileVoice calls</div>
          	<?php endif;?>
        </div>
        <div id="country-rate">
            <table>
                <thead>
                <tr>
                    <th>Service</th>
                    <th><?php echo INT_VOICE_RATES_SYMBOL; ?></th>
                    <th>MB</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>


<div id="smilevoice-terms">
    <?php $termsConditions = PageType::getPagePost($termsConditionsPageType);
    if ($termsConditions != null):?>
        <div class="accordion row">
            <div class="accordion-section">
                <a class="accordion-section-title" href="#accordion-0"><?php echo $termsConditions->post_title ?></a>
                <div id="accordion-0" class="accordion-section-content">
                    <?php echo apply_filters('the_content', $termsConditions->post_content); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>