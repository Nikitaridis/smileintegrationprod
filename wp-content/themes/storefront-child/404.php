<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package storefront-child
 */

get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">


        <section class="error-404 not-found">
            <header class="page-header">
                <h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'storefront' ); ?></h1>
            </header><!-- .page-header -->

            <div class="page-content">

                <br>

                <p class="return-to-shop">
                    <a class="button wc-backward" href="<?php echo get_option('home') . '#products'; ?>">
                        <?php _e( 'Return To Shop', 'woocommerce' ) ?>
                    </a>
                </p>





            </div><!-- .page-content -->
        </section><!-- .error-404 -->

    </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
