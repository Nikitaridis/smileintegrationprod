<?php
/**
 * The template for displaying the data bundles page.
 *
 * Template Name: Data bundles
 *
 * @package storefront-child
 */
?>


<?php get_header(); ?>

<?php
    // Page post object
    $Page = get_post();

    $args = array(
        'post_status'       => 'publish',
        'post_type'         => 'product',
        'meta_key'          => 'smile_platform_type',
        'meta_value'        => PageType::isPageType('business', get_post()->post_parent) ? 'business' : 'consumer',
        'orderby'           => 'menu_order',
        'order'             => 'ASC',
        'posts_per_page'    => -1
    );
    $products = new WP_Query($args);
?>

<section id="data-bundles">


    <div class="row">
        <div class="twelve columns section-title section-text-center">
            <h1><?php echo $Page->post_title; ?></h1>
            <?php echo the_subtitle(); ?>
<!--            <p>--><?php //echo apply_filters('the_content', $Page->post_content); ?><!--</p>-->
        </div>
    </div>


    <div class="bundles fulllist">
        <div class="row ">
            <div class="twelve columns">
                <ul>

                    <?php
                    $bundlesHTML = '';

                    if($products->have_posts())
                    {
                        while($products->have_posts())
                        {
                            $products->the_post();
                            $product_id = get_the_ID();
                            if(!empty(wc_get_product_terms($product_id, 'pa_data-bundle-title', null))) {
                                $linkedProduct = new WC_Product(get_the_ID());

                                // Skip data bundles with no title
                                if(empty($linkedProduct->get_attribute('pa_data-bundle-title')) ||
                                    (empty($linkedProduct->get_attribute('pa_data-bundle-gb')) && empty($linkedProduct->get_attribute('pa_data-bundle-mb')) && empty($linkedProduct->get_attribute('pa_data-bundle-unlimited'))) ||
                                    !(get_post_meta($linkedProduct->get_id(), '_regular_price', true)))
                                {
                                    continue;
                                }

                                // Databundle
                                $dataBundleHTML = '';
                                $dataBundleGB = $linkedProduct->get_attribute('pa_data-bundle-gb');
                                $dataBundleMB = $linkedProduct->get_attribute('pa_data-bundle-mb');
                                $dataBundleUnlimited = $linkedProduct->get_attribute('pa_data-bundle-unlimited');
                                if(!empty($dataBundleGB)) {
                                    $dataBundleHTML = $dataBundleGB . '<span class="data-bundle-size">GB</span>';
                                }
                                elseif(!empty($dataBundleMB)) {
                                    $dataBundleHTML = $dataBundleMB . '<span class="data-bundle-size">MB</span>';
                                }
                                elseif(!empty($dataBundleUnlimited)) {
                                    $dataBundleHTML = '<span class="text">' . $dataBundleUnlimited . '</span>';
                                }

                                // Circle color
                                $cssClasses = array('starter', 'basic', 'premier', 'deluxe');
                                $circleColor = $linkedProduct->get_attribute('pa_data-bundle-color');
                                $outOfBundle = $linkedProduct->get_attribute('pa_data-bundle-out-of-bundle');

                                $bundlesHTML .= '
                                <li class="data-bundle-row" id="product-item-'.$product_id.'">
                                    <div class="four columns data-bundle-column">
                                        <div class="circle '. $circleColor .'">
                                            <span class="circle-back"></span>
                                            <!-- <h3 class="title">'. $linkedProduct->get_attribute('pa_data-bundle-title') .'</h3> -->
                                            <span class="data-bundle">'. $dataBundleHTML .'</span>
                                            <!--<p class="data-bundle-inner">'. $dataBundleHTML .'</p>-->
                                            <!--<p class="data-bundle-subtitle">'. $linkedProduct->get_attribute('pa_data-bundle-subtitle') .'</p>-->
                                        </div>
                                        <p class="subtitle">'. $linkedProduct->get_attribute('pa_data-bundle-subtitle') .'</p>
                                    </div>
                                    <!-- <div class="three columns data-bundle-column">
                                        <p class="column-title">Package:</p>
                                        <span class="data-bundle">'. $dataBundleHTML .'</span>
                                    </div> -->
                                    <div class="four columns data-bundle-column">
                                        <span class="column-title">Includes:</span>
                                         <ul>'.
                                            (empty($linkedProduct->get_attribute('pa_data-bundle-limit'))?'':('<li>'. $linkedProduct->get_attribute('pa_data-bundle-limit') .'</li>')).
                                            '<li>'. $linkedProduct->get_attribute('pa_data-bundle-validity') .' validity</li>'.
                                            ((!empty($outOfBundle)) ? '<li>'. $outOfBundle .'/GB Out of Bundle</li>' : '') .
                                            (empty($linkedProduct->get_attribute('pa_data-bundle-text')) ? '' : ('<li>'. $linkedProduct->get_attribute('pa_data-bundle-text') .'</li>')) .
                                            (empty($linkedProduct->get_attribute('pa_data-bundle-text2')) ? '' : ('<li>'. $linkedProduct->get_attribute('pa_data-bundle-text2') .'</li>')).
					    (empty($linkedProduct->get_attribute('pa_data-bundle-usageterms-link')) ? '' : ('<li><a href="'. $linkedProduct->get_attribute('pa_data-bundle-usageterms-link') .'">Terms of use</a></li>')).
					    (empty($linkedProduct->get_attribute('pa_data-bundle-fuplink')) ? '' : ('<li><a href="'. $linkedProduct->get_attribute('pa_data-bundle-fuplink') .'">FUP applies</a></li>')).'
                                        </ul>
                                    </div>
                                    <div class="four columns data-bundle-column">
                                        <span class="column-title price">Price:</span>
                                        '. $linkedProduct->get_price_html() .'
                                        <div class="data-bundle-price-subtitle">'. $linkedProduct->get_attribute('pa_data-bundle-price-subtitle') .'</div>
                                        <form name="recharge-product-item-' .$product_id. '" method="post" action="'. PageType::getPageUrl('recharge') .'">
                                            <input type="hidden" name="recharge-product-id" value="'.$product_id. '" />
                                            <button type="submit" class="data-bundle-recharge button">Recharge</button>
                                        </form>
                                    </div>
                                </li>';
                            }
                        }
                    }
                    wp_reset_postdata();

                    echo $bundlesHTML;
                    ?>

                </ul>
            </div>
        </div>
<!--		<span class='data-bundle-disclaimer'>* Percentage saved per GB is calculated in comparison to the price per GB of the 3GB plan</span>-->

        <?php $termsConditions = PageType::getPagePost('terms-and-conditions-products');
        if($termsConditions != null):?>
        <div class="accordion row">
            <div class="accordion-section">
                <a class="accordion-section-title" href="#accordion-0"><?php echo $termsConditions->post_title ?></a>
                <div id="accordion-0" class="accordion-section-content">
                    <?php echo apply_filters('the_content', $termsConditions->post_content); ?>
                </div>
            </div>
        </div>
        <?php endif;?>

        <div class="row ">
            <div class="six columns offset-3">
                <div class="support">
                    <p>Can’t find what you are looking for?</p>
                    <a href="" class="open-chat">Chat with us (24/7)</a>
                </div>
            </div>
        </div>
    </div>


</section>

<?php get_footer(); ?>
