<?php
/**
 * The template for displaying the SmileVoice page
 * Template Name: SmileVoice
 * @package storefront
 */
?>

<?php get_header(); ?>

<?php
// Page post object
$Page = get_post();
$PageMetaData = get_post_meta($Page->ID);
?>
<section id="smilevoice" class="extra-margin">
    <div class="row">
        <div class="twelve columns section-title section-text-center">
            <h1><?php echo $Page->post_title; ?></h1>
            <?php echo the_subtitle(); ?>
        </div>
    </div>

    <div class="row">
        <div class="six columns">
            <?php the_post_thumbnail( 'full' ); ?>
        </div>

        <div class="six columns">
            <?php echo apply_filters('the_content', $Page->post_content); ?>
        </div>
    </div>
</section>

<section id="smilevoice-badges">
    <div class="row">
        <div class="six columns">
            <a target="_blank" class="app-store-container" href="https://itunes.apple.com/us/app/smilevoice/id1038360615?mt=8#">
                <img class="link-to-store" alt="Download on the App Store" src="<?php echo get_stylesheet_directory_uri();?>/images/app-store-badge.svg" />
            </a>
        </div>

        <div class="six columns">
            <a target="_blank" class="play-store-container" href="https://play.google.com/store/apps/details?id=com.smile.android.app">
                <img class="link-to-store" alt="Get it on Google Play" src="<?php echo get_stylesheet_directory_uri();?>/images/google-play-badge-get-it-on.png" />
            </a>
        </div>
    </div>
</section>

<?php
$args = array(
    'post_type'      => 'page',
    'post_parent'    => $Page->ID,
    'order'          => 'ASC',
    'orderby'        => 'menu_order'
);

$pagekids = new WP_Query($args);
if($pagekids->have_posts()) {
    $index = 0;
    while ($pagekids->have_posts()) {
        $pagekids->the_post();

        if($index % 2 != 0) $class = 'ltgray';
        else $class = '';

        $page_template = get_post_meta(get_the_ID(), '_wp_page_template', true);
        ?>
        <section id="<?php echo get_post()->post_name; ?>" class="smilevoice-child <?php echo $class; ?>">
            <?php
            if(!empty($page_template)) {
                require get_stylesheet_directory() . '/' . $page_template;
            }
            else {
                ?>

                <div class="twelve columns section-title section-text-center">
                    <h2><?php echo get_post()->post_title; ?></h2>
                    <?php echo get_the_subtitle(); ?>
                </div>
                <div class="twelve columns">
                    <?php echo apply_filters('the_content', get_post()->post_content); ?>
                </div>
                <?php
            }
            ?>
        </section>
        <?php

        $index++;
    }
}
?>

<?php get_footer(); ?>