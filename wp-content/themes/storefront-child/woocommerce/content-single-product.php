<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
    echo get_the_password_form(); // WPCS: XSS ok.
    return;
}

// Add coverage js
wp_enqueue_script('smile-coverage-script', get_stylesheet_directory_uri() . '/js/coverage.js', array(), SMILE_VERSION, true);
wp_localize_script('smile-coverage-script', 'CoverageParams',
    array(
        'country' => COUNTRY,
        'maps' => array(
            'kmz_url' => MAPS_KMZ_URL,
            'lat' => MAPS_CENTER_LAT,
            'lng' => MAPS_CENTER_LNG,
            'zoom' => MAPS_CENTER_ZOOM
        )
    )
);

$product = new WC_Product( get_the_ID() );
$show_check_device_attr = $product->get_attribute('sim-show-check-device');
$show_check_device = !empty($show_check_device_attr) && $show_check_device_attr == 'True';
if($show_check_device) {
    add_filter('smile_window_overlays', 'smile_window_overlay');
    wp_enqueue_script('smile-device-script', get_stylesheet_directory_uri() . '/js/device.js', array(), SMILE_VERSION, true);
}
add_filter('smile_window_overlays', 'smile_window_overlay_coverage');
if(!function_exists('smile_window_overlay'))
{
    function smile_window_overlay($overlays)
    {
        $Smile = new Smile_Devices();
        $brands = $Smile->getBrands();
        ob_start();
        ?>
        <section id="overlay-devices-list">
            <div class="devices-list-container">
                <h2>Compatible Devices</h2>
                <p class="subtitle">We are constantly adding devices. Please let us know if your device is not listed</p>
                <div class="devices-select-container row centered">
                    <p>Select your device</p>

                    <select id="deviceBrand" class="dropdown">
                        <option value="">Select brand</option>
                        <?php
                        foreach($brands as $brand)
                        {
                            echo '<option value="' . $brand->slug . '">' . $brand->name . '</option>';
                        }
                        ?>
                    </select>

                    <select id="devicesByBrand" class="dropdown">
                        <option value="">Select device</option>
                    </select>

                    <div class="check-container">
                        <p><span class="icon-check"></span>Your device is compatible</p>
                        <span class="icon-check"></span>
                    </div>
                </div>
                <a class="device-not-found"><span class="help">Help!</span> My device is not listed</a>
            </div>
            <div class="devices-list-contact">
                <h2>Contact</h2>
                <p>Please fill in this form and we will contact you if your device is compatible with this item.</p>
                <?php if( function_exists( 'ninja_forms_display_form' ) ){ ninja_forms_display_form( 9 ); } ?>
            </div>
        </section>
        <?php
        $overlays[] = ob_get_clean();
        return $overlays;
    }
}

if(!function_exists('smile_window_overlay_coverage'))
{
    function smile_window_overlay_coverage($overlays)
    {
        $location = '';
        if(isset($_GET['location']) && !empty($_GET['location'])) {
            $location = $_GET['location'];

            wp_localize_script('smile-coverage-script', 'Coverage',
                array(
                    'location' => $location
                )
            );
        }
        $coverage_query = new WP_Query(array(
            'name' => 'coverage',
            'post_type' => 'page',
            'post_status' => 'publish',
            'numberposts' => 1
        ));
        if($coverage_query->have_posts()) {
            while($coverage_query->have_posts()) {
                $coverage_query->the_post();
                ob_start();
                do_action('smile_coverage_map_html');
                $overlays[] = ob_get_clean();
            }
        }
        return $overlays;
    }
}
?>

<section id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php
    //wc_get_template('inc/template/product/add-to-cart/simple.php');
    //        $product = new WC_Product( get_the_ID() );
    ?>

    <div class="row">

        <div class="seven columns offset-5">
            <h1 class="padding-left"><?php echo $product->get_title(); ?></h1>
        </div><!-- end twelve colums -->

        <div class="five columns">
            <figure><?php echo $product->get_image('full'); ?></figure>

            <?php
            $productImages = $product->get_gallery_image_ids();

            $productGallery = '<ul class="gallery">';

            // Add the big image to the gallery. Css will hide it
            $productImageData = wp_get_attachment_image_src( get_post_thumbnail_id( $product->get_id() ), 'full' );
            if(isset($productImageData[0]) && !empty($productImageData[0])) {
                $productGallery .= '<li class="hidden">';
                $productGallery .= '<a title="'. $product->get_title() .'" href="'. $productImageData[0] .'">';
                $productGallery .= '<img src="'. $productImageData[0] .'" alt="'. $product->get_title() .'">';
                $productGallery .= '</a>';
                $productGallery .= '</li>';
            }

            if(is_array($productImages) && count($productImages)) {

                foreach($productImages as $productImageKey => $productImageID) {
                    if($productImageKey > 2) break;
                    $imageUrl = wp_get_attachment_url( $productImageID );
                    $imageMeta = wp_get_attachment_metadata($productImageID);

                    $productGallery .= '<li>';
                    $productGallery .= '<a title="'. $imageMeta['image_meta']['title'] .'" href="'. $imageUrl .'">';
                    $productGallery .= '<img src="'. $imageUrl .'" alt="'. $imageMeta['image_meta']['title'] .'">';
                    $productGallery .= '</a>';
                    $productGallery .= '</li>';
                    $productImageKey++;
                }
            }

            $productGallery .= '</ul>';
            echo $productGallery;

            ?>
        </div><!-- end five colums -->

        <div class="seven columns">
            <div class="product-details-title padding-left">Product details</div>

            <?php
            $productDetails = $product->get_attribute('product-details');
            $productDetailsArray = explode('|', $productDetails);
            if(!empty($productDetails) && count($productDetailsArray)) {

                $productDetailsHtml = '<ul class="product-details">';
                foreach($productDetailsArray as $productDetailKey => $productDetail) {
                    $class = ' class="padding-left';
                    if ($productDetailKey % 2 == 0) {
                        $class .= ' even';
                    }
                    $class .= '"';
                    $productDetailsHtml .='<li'. $class .'>'. $productDetail .'</li>';
                }
                $productDetailsHtml .= '</ul>';

                echo $productDetailsHtml;
            }
            ?>

            <?php echo $product->get_price_html(); ?>

            <?php if($product->get_attribute('data-bundle-gb') != '') : ?>
                <div class="special padding-left">
                    <span class="gb"><?php echo $product->get_attribute('data-bundle-gb'); ?>GB</span> INCLUDED
                </div>
            <?php endif; ?>
            <?php if($product->get_attribute('data-bundle-mb') != '') : ?>
                <div class="special padding-left">
                    <span class="gb"><?php echo $product->get_attribute('data-bundle-mb'); ?>MB</span> INCLUDED
                </div>
            <?php endif; ?>

            <?php
            $MySmileUrl = get_post_meta(get_the_ID(), '_product_mysmile_url', true);
            if(!empty($MySmileUrl)) {
                echo '<div style="clear:both;margin-bottom:10px;"></div>';
                echo '<a style="font-size:26px;" target="_blank" class="button green" href="'. $MySmileUrl .'"><span class="icon-chevron-circle-right" style="margin-right:10px;font-size:25px;vertical-align:top:"></span>Go to MySmile</a>';
            } else if(!empty($product->get_attribute('data-bundle-title'))) {
                ?>
                <form method="post" action="<?php echo PageType::getPageUrl('recharge');?>">
                    <input type="hidden" name="recharge-product-id" value="<?php echo $product->get_id();?>" />
                    <button type="submit" class="button alt">Recharge</button>
                </form>
                <?php
            } else {
                wc_get_template('woocommerce/single-product/add-to-cart/simple.php');
            }
            ?>

            <div class="overlay-buttons-container">
                <?php if($show_check_device): ?>
                    <div class="six columns">
                        <a title="Check your device" class="button check-device open-window-overlay trigger" data-overlay-identifier="overlay-devices-list" data-trigger-category="Product checks">Check your device</a>
                    </div>
                <?php endif; ?>
                <div class="six columns">
                    <a title="Check our coverage" class="button check-coverage open-window-overlay window-overlay-fill-screen trigger" data-overlay-identifier="coverage-map" data-trigger-category="Product checks">Check our coverage</a>
                </div>
            </div>
        </div><!-- end seven colums -->


        <div class="twelve columns content">
            <?php the_content(); ?>
            <?php
            $productBrochureUrl = get_post_meta(get_the_ID(), '_product_brochure_url', true);
            if(!empty($productBrochureUrl)) {
                echo '<a target="_blank" class="green" href="'. $productBrochureUrl .'"><span class="icon-chevron-circle-right"></span>Brochure</a>';
            }
            ?>
        </div><!-- end twelve colums -->

        <meta itemprop="url" content="<?php the_permalink(); ?>" />

    </div><!-- end div row -->

</section><!-- end section #product-<?php the_ID(); ?> -->


<?php
$upsells = $product->get_upsell_ids();
if(count($upsells) > 0) {
    set_query_var( 'upsells', $upsells );
    get_template_part( 'inc/template/data-bundle/display', 'shortlist' );
}
?>



<?php //do_action( 'woocommerce_after_single_product' ); ?>
