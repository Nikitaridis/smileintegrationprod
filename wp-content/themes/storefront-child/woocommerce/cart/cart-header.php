<?php
/**
 * Template for showing the cart in the header
 *
 * @package storefront-child
 */

/**
 * Cart Link
 * Displayed a link to the cart including the number of items present and the cart total
 * @param  array $settings Settings
 * @return array           Settings
 * @since  1.0.0
 */
if ( ! function_exists( 'smile_cart_link' ) ) {
    function smile_cart_link() {
        ?>

            <a data-trigger-category="Mainnav" title="Shopping basket" class="trigger" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php _e( 'View your shopping cart', 'storefront' ); ?>">

                <span class="icon-shopping-cart"></span>

                <?php if(WC()->cart->get_cart_contents_count() > 0):?>
                <span class="count">
                    <?php
                        echo sprintf( _n( '%d <span class="item-label">item</span>', '%d <span class="item-label">items</span>', WC()->cart->get_cart_contents_count(), 'storefront' ), WC()->cart->get_cart_contents_count() );
                    ?>
                </span>
                <?php endif;?>
            </a>
    <?php
    }
}


/**
 * Display Header Cart
 * @since  1.0.0
 * @uses  is_woocommerce_activated() check if WooCommerce is activated
 * @return void
 */
if ( ! function_exists( 'smile_header_cart' ) ) {
    function smile_header_cart() {
        if ( is_woocommerce_activated() ) {
        ?>
            <div class="shopping-basket">
                <?php smile_cart_link(); ?>
            </div>
        <?php
        }
    }
}