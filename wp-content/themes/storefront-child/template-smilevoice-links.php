<?php
/**
 * The template for displaying the links on the smilevoice page
 * Template Name: SmileVoice Links
 * @package storefront
 */
?>

<?php
// Page post object
$Page = get_post();
$PageMetaData = get_post_meta($Page->ID);
$isBusiness = PageType::getTopParentPageType() === 'business';
?>

    <div class="row">
        <div class="twelve columns section-title section-text-center">
            <h1><?php echo $Page->post_title; ?></h1>
            <?php echo the_subtitle(); ?>
            <?php echo apply_filters('the_content', $Page->post_content); ?>
        </div>
    </div>

    <ul class="smilevoice-links block-list wide row">
        <?php if($isBusiness):?>
            <li class="container four columns">
                <a href="#" class="open-chat trigger" data-trigger-category="SmileVoice-link" title="Chat with us">
                    <div class="inner">
                        <h3>Chat with us</h3>
                        <p class="subtitle">Please chat with one of our business experts</p>
                    </div>
                </a>
            </li>
            <li class="container four columns">
                <a href="<?php echo PageType::getPageUrl('business-enquiry-form'); ?>" class="trigger" data-trigger-category="Business-link" title="How can we help you?">
                    <div class="inner">
                        <h3>How can we help you?</h3>
                        <p class="subtitle">Please click here to leave your details and we will be in contact within the hour</p>
                    </div>
                </a>
            </li>
            <li class="container four columns">
                <a href="<?php echo get_home_url();?>/help/knowledgebase/?category=smilebusiness" class="trigger" data-trigger-category="Business-link" title="View Knowledge Base">
                    <div class="inner">
                        <h3>SmileBusiness FAQ</h3>
                        <p class="subtitle">Read more about the solutions from SmileBusiness in our FAQs</p>
                    </div>
                </a>
            </li>
        <?php else:?>
            <li class="container four columns">
                <a href="#" class="open-chat trigger" data-trigger-category="SmileVoice-link" title="Chat with us">
                    <div class="inner">
                        <h3>Chat with us</h3>
                        <p class="subtitle">We are 24/7 available to help you with picking the right product and plan</p>
                    </div>
                </a>
            </li>
            <li class="container four columns">
                <a href="<?php echo PageType::getPageUrl('contact'); ?>" class="trigger" data-trigger-category="SmileVoice-link" title="Send an email">
                    <div class="inner">
                        <h3>Send an e-mail</h3>
                        <p class="subtitle">E-mail us your questions and we respond the same day</p>
                    </div>
                </a>
            </li>
            <li class="container four columns">
                <a href="<?php echo PageType::getPageUrl('help'); ?>" class="trigger" data-trigger-category="SmileVoice-link" title="View Knowledge Base">
                    <div class="inner">
                        <h3>SmileVoice FAQ</h3>
                        <p class="subtitle">Read more about SmileVoice on the Frequently Asked Questions</p>
                    </div>
                </a>
            </li>
        <?php endif;?>
    </ul>