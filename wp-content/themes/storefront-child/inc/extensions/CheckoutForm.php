<?php



// Hook in
add_filter('woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
add_action('woocommerce_after_checkout_billing_form', 'smile_woocommerce_after_checkout_billing_form');
add_action('woocommerce_after_checkout_form', 'smile_woocommerce_after_checkout_form');

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
    unset($fields['order']['order_comments']);
    return $fields;
}

function smile_woocommerce_after_checkout_billing_form() {
    ?>
    <p style="float: left"><abbr class="required" title="required">*</abbr> Mandatory fields to be completed for KYC compliance</p>
    <?php
}

function smile_woocommerce_after_checkout_form() {
    wp_enqueue_script('smile-checkout-script', get_stylesheet_directory_uri() . '/js/checkout.js', array(), SMILE_VERSION, true);
    wp_localize_script('smile-checkout-script', 'CheckoutParams',
        array(
            'country' => COUNTRY
        )
    );
}