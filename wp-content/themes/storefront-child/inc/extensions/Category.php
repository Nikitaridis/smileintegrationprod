<?php
/**
 * Category class
 * @desc for easy access to categories
 */

class Category {

    public $taxonomy = 'product_cat';
    public $orderby = 'menu_order';
    public $order = 'ASC';
    public $show_count = 1;
    public $pad_counts = 1;
    public $hierarchical = 1;
    public $title_li = '';
    public $hide_empty = 0;

    public function __construct() {
    }


    /**
     * Fetch categories
     * @return array
     */
    public function getCategory() {
		// use default order
        $args = array(
            'taxonomy'     => $this->taxonomy,
        //   'orderby'      => $this->orderby,
        //    'order'        => $this->order,
            'show_count'   => $this->show_count,
            'pad_counts'   => $this->pad_counts,
            'hierarchical' => $this->hierarchical,
            'title_li'     => $this->title_li,
            'hide_empty'   => $this->hide_empty
        );
        $all_categories = get_categories( $args );
        return $all_categories;
    }

}