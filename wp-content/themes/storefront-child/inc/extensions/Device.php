<?php

if(!function_exists('getDevicesByBrand')) {
    function getDevicesByBrand()
    {
        if (!isset($_REQUEST['brand']) || empty($_REQUEST['brand'])) {
            echo '';
            exit;
        }

        $args = array(
            'post_type' => Smile_Devices_Admin::POST_TYPE,
            'post_status' => 'publish',
            'orderby' => 'title',
            'order' => 'ASC',
            'nopaging' => true,
            'tax_query' => array(
                array(
                    'taxonomy' => Smile_Devices_Admin::TAX_BRAND_KEY,
                    'field' => 'slug',
                    'terms' => sanitize_text_field($_REQUEST['brand'])
                )
            )
        );

        echo json_encode(get_posts($args));
        exit;
    }
}

if(is_user_logged_in()) {
    add_action( 'wp_ajax_getDevicesByBrand', 'getDevicesByBrand' );
}else {
    add_action( 'wp_ajax_nopriv_getDevicesByBrand', 'getDevicesByBrand' );
}

?>