<?php

/**
 * @desc Ajax request from shops search form
 * @return list of nearest shops
 */
function fetchShopsOnGeo() {

    validateAjaxCall();

    $ShopHtml = '';
    $latitude = 0;
    $longitude = 0;

    if (isset($_REQUEST['searchQuery']) && !empty($_REQUEST['searchQuery'])) {

//        $country = 'Nigeria';
//        if(ENV == 'DEVELOPMENT') {
//            $country = 'Nederland';
//        }
        $oGeocode = new Geocode($_REQUEST['searchQuery'], '', COUNTRY);
        $oGeocode->convertToLatLng();
        $Location = $oGeocode->getLatLng();
        if($Location) {
            $latitude = $Location->lat;
            $longitude = $Location->lng;
        }
    }
    elseif( isset($_REQUEST['lat']) && !empty($_REQUEST['lat']) && isset($_REQUEST['lng']) && !empty($_REQUEST['lng']) ) {
        $latitude = $_REQUEST['lat'];
        $longitude = $_REQUEST['lng'];
    }

    else {
        // No results
        set_query_var( 'Message', 'Please enter your location in the search field' );
        $ShopHtml .= get_template_part('inc/template/shop/display', 'message');
        echo $ShopHtml;
        exit;
    }

    $numberOfResults = 3;
    if( isset($_REQUEST['numberOfResults']) && !empty($_REQUEST['numberOfResults']) ) {
        $numberOfResults = $_REQUEST['numberOfResults'];
    }


    if($latitude && $longitude) {

        $iDistanceInKm = 1000;
        $iAantal = 1000;

        global $wpdb;
        $sql = $wpdb->prepare("
            SELECT          p.ID,
                            (
                                ((acos(sin((%f*pi()/180)) *
                                sin((lat.meta_value*pi()/180))+cos((%f*pi()/180)) *
                                cos((lat.meta_value*pi()/180)) * cos(((%f- lng.meta_value)*
                                pi()/180))))*180/pi())*60*1.1515
                            ) AS distance

            FROM            wp_posts p

            LEFT JOIN       wp_postmeta lat
            ON              lat.`meta_key`='shop_latitude' and lat.post_ID = p.ID

            LEFT JOIN       wp_postmeta lng
            ON              lng.`meta_key`='shop_longitude' and lng.post_ID = p.ID

            WHERE           p.post_type = 'smile_shops' and p.post_status = 'publish'

            HAVING 			distance <= %d

            ORDER BY 		distance ASC

            LIMIT 			%d
        ",
            $latitude,
            $latitude,
            $longitude,
            $iDistanceInKm,
            $iAantal
        );

        $NearestShops = $wpdb->get_results( $sql );

        $NearestShopIDs = '';
        foreach($NearestShops as $key => $NearestShop) {
            $NearestShopIDs[] = $NearestShop->ID;
        }

        // Fetch shops
        $args = array(
            'post_status'       => 'publish',
            'posts_per_page'    => 1000,
            'post_type'         => 'smile_shops',
            'post__in'          => $NearestShopIDs
        );
        $ShopData = new WP_Query($args);

        if($ShopData->have_posts()) {

            // Add distance to the Shop object
            foreach($ShopData->posts as $ShopKey => $Shop) {
                foreach($NearestShops as $NearestShopKey => $NearestShop) {
                    if($Shop->ID == $NearestShop->ID) {
                        $Shop->distance = $NearestShop->distance;
                        $Shop->displayDistance = displayDistance($NearestShop->distance);
                    }
                }
            }

            // Sort by distance
            $Shops = $ShopData->posts;
            usort($Shops, function ($a,$b){
                return $a->distance > $b->distance;
            });


            // Html of shop rows
            if($Shops) {

                if(isset($_REQUEST['searchQuery']) && !empty($_REQUEST['searchQuery'])) {
                    $searchQuery = '"' . $_REQUEST['searchQuery'] . '"';
                }
                elseif( isset($_REQUEST['lat']) && !empty($_REQUEST['lat']) && isset($_REQUEST['lng']) && !empty($_REQUEST['lng']) ) {
                    $searchQuery = 'your location';
                }
                set_query_var( 'Message', 'The next shops are closest to <b>' . $searchQuery . '</b>');
                $ShopHtml .= get_template_part('inc/template/shop/display', 'message');

                foreach($Shops as $ShopKey => $Shop) {

                    set_query_var( 'DisplayClass', 'hidden');
                    if($ShopKey < $numberOfResults) {
                        set_query_var( 'DisplayClass', '');
                    }

                    $ShopMetaData = get_post_meta($Shop->ID);
                    $Shop->directionsUrl = getDirectionsUrl($latitude, $longitude, $ShopMetaData);

                    set_query_var( 'searchQuery', $searchQuery );
                    set_query_var( 'Shop', $Shop );
                    set_query_var( 'ShopMetaData', $ShopMetaData );
                    $ShopHtml .= get_template_part( 'inc/template/shop/display', 'row' );
                }
            }

            // Add a row to unfold more shops
            if(count($Shops) > $numberOfResults) {
                $ShopHtml .= get_template_part('inc/template/shop/display-unfold', 'shops');
            }

        }
        else {
            // No results
            set_query_var( 'Message', 'No shops found. Please try another search.' );
            $ShopHtml .= get_template_part('inc/template/shop/display', 'message');
        }
    }
    else {
        // No results
        set_query_var( 'Message', 'No shops found. Please try another search.' );
        $ShopHtml .= get_template_part('inc/template/shop/display', 'message');
    }

    echo $ShopHtml;
    exit;
}


if(is_user_logged_in()) {
    add_action( 'wp_ajax_fetchShopsOnGeo', 'fetchShopsOnGeo' );
}else {
    add_action( 'wp_ajax_nopriv_fetchShopsOnGeo', 'fetchShopsOnGeo' );
}





/**
 * @desc get all the shops
 */
function fetchAllShops() {

    validateAjaxCall();

    // Fetch shops
    $args = array(
        'post_status'       => 'publish',
        'posts_per_page'    => 1000,
        'post_type'         => 'smile_shops',
        'orderby'           => 'title',
        'order'             => 'ASC'
    );
    $ShopData = new WP_Query($args);

    $ShopHtml = '';
    if($ShopData->posts) {

        set_query_var( 'Message', 'All Smile shops in ' . COUNTRY );
        $ShopHtml .= get_template_part('inc/template/shop/display', 'message');

        foreach($ShopData->posts as $ShopKey => $Shop) {

            $ShopMetaData = get_post_meta($Shop->ID);
            $Shop->directionsUrl = getDirectionsUrl(null, null, $ShopMetaData);

            set_query_var( 'ShopMetaData', $ShopMetaData );
            set_query_var( 'Shop', $Shop );
            $ShopHtml .= get_template_part( 'inc/template/shop/display', 'row' );
        }
    }

    echo $ShopHtml;
    exit;
}
if(is_user_logged_in()) {
    add_action( 'wp_ajax_fetchAllShops', 'fetchAllShops' );
}else {
    add_action( 'wp_ajax_nopriv_fetchAllShops', 'fetchAllShops' );
}



/**
 * @param $latitude
 * @param $longitude
 * @param $ShopMetaData
 * @return string
 */
function getDirectionsUrl($latitude, $longitude, $ShopMetaData) {

    $directionsUrl = '';
    if( isset($ShopMetaData['shop_latitude'][0]) && !empty($ShopMetaData['shop_latitude'][0]) &&
        isset($ShopMetaData['shop_longitude'][0]) && !empty($ShopMetaData['shop_longitude'][0]) &&
        isset($latitude) && !empty($latitude) &&
        isset($longitude) && !empty($longitude))
    {
        $directionsUrl = 'https://maps.google.com?saddr='. $latitude .','. $longitude .'&daddr='. $ShopMetaData['shop_latitude'][0] .','. $ShopMetaData['shop_longitude'][0];
    }
    elseif( isset($ShopMetaData['shop_latitude'][0]) && !empty($ShopMetaData['shop_latitude'][0]) &&
        isset($ShopMetaData['shop_longitude'][0]) && !empty($ShopMetaData['shop_longitude'][0]))
    {
        $directionsUrl = 'https://maps.google.com?saddr=Current+Location&daddr='. $ShopMetaData['shop_latitude'][0] .','. $ShopMetaData['shop_longitude'][0];
    }

    return $directionsUrl;
}


/**
 * @desc Show distance nicely to user
 * @param $NearestShop
 */
function displayDistance($distance) {

    $DistanceInMeter = floor($distance * 1000);
    if($DistanceInMeter < 1000) {
        $DisplayDistance = $DistanceInMeter . ' meter';
    }
    else {
        $DisplayDistance = round(($DistanceInMeter / 1000), 1) . ' km';
    }
    return $DisplayDistance;
}

/**
 * Validate the ajax call
 */
function validateAjaxCall() {

    if(check_ajax_referer('ajax_fetchShops', 'security') == 0) {
        set_query_var( 'Message', 'Something went wrong. Please try again.' );
        echo get_template_part('inc/template/shop/display', 'message');
        exit;
    }
}