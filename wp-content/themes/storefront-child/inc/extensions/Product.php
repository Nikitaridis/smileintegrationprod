<?php
/**
 * Product class
 * @desc for easy access to products
 */

class Product {

    public $posts_per_page = 3;
    public $post_type = 'product';
    public $orderby = 'menu_order';
    public $order = 'ASC';
    public $tax_query = array();
    public $metaQuery = array();

    public function __construct() {
    }


    /**
     * @param string $taxonomy
     * @param string $field - Defaults to 'term_id'
     * @param int $terms
     * @param $operator - Possible values are 'IN', 'NOT IN', 'AND'.
     */
    public function setTaxQuery($taxonomy = 'product_cat', $field = 'term_id', $terms = 0, $operator) {

        $this->tax_query[] =
            array(
                'taxonomy'      => $taxonomy,
                'field'         => $field,
                'terms'         => $terms,
                'operator'      => $operator
            );
    }


    /**
     * @url: https://codex.wordpress.org/Class_Reference/WP_Query (Custom Field Parameters)
     * @param $key - Custom field key (eg. _visibility)
     * @param $value string|array
     * @param $compare - string
     * @param $type - string default 'char'
     */
    public function setMetaQuery($key, $value, $compare, $type = 'char') {

        $this->metaQuery[] =
            array(
                'key'       => $key,
                'value'     => $value,
                'compare'   => $compare,
                'type'      => $type
            );
    }


    /**
     * Fetch products
     * @return array
     */
    public function getProduct($tmp_posts_per_page = 3) {

        $this->posts_per_page = $tmp_posts_per_page;

        $args = array(
            'post_status'       => 'publish',
            'posts_per_page'    => $this->posts_per_page,
            'post_type'         => $this->post_type,
            'orderby'           => $this->orderby,
            'order'             => $this->order
        );


        if(count($this->metaQuery) > 0) {
            $args['meta_query'] = array($this->metaQuery);
        }

        if(count($this->tax_query) > 0) {
            $args['tax_query'] = array($this->tax_query);
        }

        $product = new WP_Query($args);

        return $product;
    }

}