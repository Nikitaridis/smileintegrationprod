<?php

if(!function_exists('getRechargeAccounts')) {
    function getRechargeAccounts()
    {

        $data = array();
        $data['data'] = array();

        if((isset($_REQUEST['email']) || !empty($_REQUEST['email'])) && (!is_numeric($_REQUEST['email']))) {
            $customer_data = apply_filters('smile_communication_api_get_customer_by_email_with_services', $_REQUEST['email']);
        } else if (isset($_REQUEST['msisdn']) || !empty($_REQUEST['msisdn']) && (is_numeric($_REQUEST['email']))) {
            $customer_data = apply_filters('smile_communication_api_get_customer_by_msisdn_with_services', $_REQUEST['msisdn']);
        } else  {
            echo json_encode(array('error' => 'please enter a valid email address or msisdn'));
            exit;
        }

        if(property_exists($customer_data, 'error')) {
            echo json_encode(array('error' => $customer_data->error));
            exit;
        }

        if(property_exists($customer_data, 'customer')) {
            $customer = $customer_data->customer;
//            $data['customerId'] = $customer->customerId;
            if(property_exists($customer, 'products')) {
                $products = $customer->products;
                foreach ($products as $product) {
                    if(property_exists($product, 'services')) {
                        $services = $product->services;
                        foreach ($services as $service) {
			  if($service->status !== "TD") {
                            $hiddenAccountId = substr_replace($service->accountId, "XXXXXX", 0, 6);
                            $hiddenServiceICCID = substr_replace($service->serviceICCID, "XXXXXXXXXXXXXXXX", 0, 16);
                            $data['data'][$hiddenAccountId] = array();
                            $data['data'][$hiddenAccountId][$hiddenServiceICCID] = array();
                            $data['data'][$hiddenAccountId][$hiddenServiceICCID]['productFriendlyName'] = $service->productFriendlyName;
                            $data['data'][$hiddenAccountId][$hiddenServiceICCID]['productInstanceId'] = $service->productInstanceId;
                            $data['data'][$hiddenAccountId][$hiddenServiceICCID]['accountHash'] = md5($service->accountId . $service->serviceICCID);
//                            $data['data'][$hiddenAccountId][$hiddenServiceICCID]['accountId'] = $service->accountId;
			  }
                        }
                    }
                }
            }
        }

        if(empty($data['data'])) {
            echo json_encode(array('error' => 'There are no accounts available'));
            exit;
        }

        write_log(print_r(json_encode($data), true));

        echo json_encode($data);
        exit;
    }
}

if(is_user_logged_in()) {
    add_action( 'wp_ajax_getRechargeAccounts', 'getRechargeAccounts' );
}else {
    add_action( 'wp_ajax_nopriv_getRechargeAccounts', 'getRechargeAccounts' );
}

?>
