<?php

class Geocode {

    private $sAdres;
    private $sLand;
    private $oLatLng	= false;

    const GEOCODE_URL 	= 'http://maps.googleapis.com/maps/api/geocode/json?';
	
    public function __construct($sAdres, $sLand) {
        $this->sAdres 		= $sAdres;
        $this->sLand		= $sLand;
    }


    public function convertToLatLng() {

        $sAdres 	= str_replace(' ', '+', $this->sAdres);

        $sUrl  = self::GEOCODE_URL;
        $sUrl .= 'address=' . $sAdres;
        $sUrl .= '+' . $this->sLand;
        if(defined('GEOCODING_API_KEY')){
            $sUrl .= '&key=' . GEOCODING_API_KEY;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $sUrl);

		if(defined('WP_PROXY_HOST') && defined('WP_PROXY_PORT') ){
			curl_setopt($ch, CURLOPT_PROXY, WP_PROXY_HOST);
			curl_setopt($ch, CURLOPT_PROXYPORT, WP_PROXY_PORT);
		}
		
		
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close ($ch);

        $oOutput = json_decode($server_output);
        if(isset($oOutput->results)) {

            $aResults = $oOutput->results;
            if(isset($aResults[0]) && !empty($aResults[0])) {
                $oLocation = $aResults[0];
                $this->oLatLng = $oLocation->geometry->location;
            }
            else {
                $this->oLatLng = false;
            }
        }
        else {
            $this->oLatLng = false;
        }


    }


    public function getLatLng() {
        return $this->oLatLng;
    }
}

