<?php
/**
 * Created by PhpStorm.
 * User: kevinvanmierlo
 * Date: 29/02/16
 * Time: 11:59
 */
function add_country_settings($settings_pages)
{
    $settings_page = array();
    $settings_page['slug'] = 'country-settings';
    $settings_page['title'] = 'Country';

    $selected_country = get_option('smile_settings_country');
    $registration_number = get_option('smile_settings_registration_number');

    ob_start();?>
    <table>
        <tr>
            <td>Current Country</td>
            <td>
                <select name="smile_settings_country">
                    <option value="Nigeria" <?php if($selected_country == "Nigeria") echo "selected";?>>Nigeria</option>
                    <option value="Tanzania" <?php if($selected_country == "Tanzania") echo "selected";?>>Tanzania</option>
                    <option value="Uganda" <?php if($selected_country == "Uganda") echo "selected";?>>Uganda</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Registration number</td>
            <td>
                <input name="smile_settings_registration_number" type="text" value="<?php echo $registration_number; ?>">
            </td>
        </tr>
    </table>
    <?php
    $settings_page['content'] = ob_get_clean();
    $settings_pages[] = $settings_page;

    return $settings_pages;
}

function save_country_settings()
{
    if(isset($_POST['smile_settings_country'])) {
        update_option('smile_settings_country', $_POST['smile_settings_country']);
    }

    if(isset($_POST['smile_settings_registration_number'])) {
        update_option('smile_settings_registration_number', $_POST['smile_settings_registration_number']);
    }
}

//======================================================

function add_enquiry_settings($settings_pages)
{
    $settings_page = array();
    $settings_page['slug'] = 'enquiry-settings';
    $settings_page['title'] = 'Enquiry';

    $enquiry_smileservice_options = get_option('smile_enquiry_smileservice', array());
    $enquiry_bandwidth_options = get_option('smile_enquiry_bandwidth', array());
    $enquiry_locations_options = get_option('smile_enquiry_locations', array());

    ob_start();?>
    <div class="enquiry-settings-parent">
        <div id="enquiry-settings-smileservice" class="enquiry-settings">
            <p>Enter the Smile service options here</p>
            <input id="enquiry-smileservice-add-input" type="submit" class="button button-secondary" value="Add Input Field"/><br><br>
            <div class="enquiry_smileservice_input_fields">
                <?php foreach ($enquiry_smileservice_options as $enquiry_smileservice_option): ?>
                    <div><input type="text" name="enquiry_smileservice_item[]" value="<?php echo $enquiry_smileservice_option; ?>"><a href="#" class="remove_field">Remove</a><br><br></div>
                <?php endforeach; ?>
            </div>
        </div>
        <div id="enquiry-settings-bandwidth" class="enquiry-settings">
            <p>Enter the bandwidth options here</p>
            <input id="enquiry-bandwidth-add-input" type="submit" class="button button-secondary" value="Add Input Field"/><br><br>
            <div class="enquiry_bandwidth_input_fields">
                <?php foreach ($enquiry_bandwidth_options as $enquiry_bandwidth_option): ?>
                    <div><input type="text" name="enquiry_bandwidth_item[]" value="<?php echo $enquiry_bandwidth_option; ?>"><a href="#" class="remove_field">Remove</a><br><br></div>
                <?php endforeach; ?>
            </div>
        </div>
        <div id="enquiry-settings-locations" class="enquiry-settings">
            <p>Enter the locations options here</p>
            <input id="enquiry-locations-add-input" type="submit" class="button button-secondary" value="Add Input Field"/><br><br>
            <div class="enquiry_locations_input_fields">
                <?php foreach ($enquiry_locations_options as $enquiry_locations_option): ?>
                    <div><input type="text" name="enquiry_locations_item[]" value="<?php echo $enquiry_locations_option; ?>"><a href="#" class="remove_field">Remove</a><br><br></div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <?php
    $settings_page['content'] = ob_get_clean();
    $settings_pages[] = $settings_page;

    return $settings_pages;
}

function save_enquiry_settings()
{
    if (isset($_POST['enquiry_smileservice_item'])) {
        update_option('smile_enquiry_smileservice', $_POST['enquiry_smileservice_item']);
    } else if (isset($_POST['save'])) {
        update_option('smile_enquiry_smileservice', array());
    }

    if (isset($_POST['enquiry_bandwidth_item'])) {
        update_option('smile_enquiry_bandwidth', $_POST['enquiry_bandwidth_item']);
    } else if (isset($_POST['save'])) {
        update_option('smile_enquiry_bandwidth', array());
    }

    if (isset($_POST['enquiry_locations_item'])) {
        update_option('smile_enquiry_locations', $_POST['enquiry_locations_item']);
    } else if (isset($_POST['save'])) {
        update_option('smile_enquiry_locations', array());
    }
}

add_filter('smile_settings', 'add_country_settings');
add_action('save_smile_settings_country-settings', 'save_country_settings');

add_filter('smile_settings', 'add_enquiry_settings');
add_action('save_smile_settings_enquiry-settings', 'save_enquiry_settings');