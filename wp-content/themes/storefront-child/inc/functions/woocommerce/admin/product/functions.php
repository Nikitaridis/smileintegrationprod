<?php

/**
 * Woocommerce admin - product section
 */

require __DIR__ . '/display-tab.php';

// Add ItemNumber below SKU (SerialNumber)
add_action( 'woocommerce_product_options_sku', 'smile_woo_add_custom_SKU_fields', 10, 0 ); 
function smile_woo_add_custom_SKU_fields() {
	// Text Field
    woocommerce_wp_text_input(
        array(
            'id'          => '_product_serial_number',
            'label'       => __( 'Serial Number', 'woocommerce' ),
            'desc_tip'    => 'true',
            'description' => __( 'Enter the Serial Number necessary for the backoffice to process a sale.', 'woocommerce' )
        )
    );
    woocommerce_wp_text_input(
        array(
            'id'          => '_product_item_number',
            'label'       => __( 'Item Number', 'woocommerce' ),
            'desc_tip'    => 'true',
            'description' => __( 'Enter the Item Number necessary for the backoffice to process a sale.', 'woocommerce' )
        )
    );

	
}
// Add upload field to general tab
add_action( 'woocommerce_product_options_general_product_data', 'smile_woo_add_custom_general_fields' );
function smile_woo_add_custom_general_fields() {
    // Text Field
    woocommerce_wp_text_input(
        array(
            'id'          => '_product_brochure_url',
            'label'       => __( 'Brochure url', 'woocommerce' ),
            'placeholder' => 'http://',
            'desc_tip'    => 'true',
            'description' => __( 'Enter the url of a PDF here.', 'woocommerce' )
        )
    );

    // Text Field
    woocommerce_wp_text_input(
        array(
            'id'          => '_product_mysmile_url',
            'label'       => __( 'MySmile url', 'woocommerce' ),
            'placeholder' => 'http://',
            'desc_tip'    => 'true',
            'description' => __( 'Enter the url of MySmile here. Leave blank to use normal shopping flow.', 'woocommerce' )
        )
    );
}

add_action('woocommerce_product_options_related', 'smile_woo_add_custom_related_products_fields');
function smile_woo_add_custom_related_products_fields() {
    ?>
    <div class="options_group">

        <p class="form-field">
            <label for="bundled_product_ids">Bundled products</label>
            <select multiple="multiple" class="wc-product-search" style="width: 50%;" id="bundled_product_ids" name="bundled_product_ids[]" data-placeholder="<?php esc_attr_e( 'Search for a product&hellip;', 'woocommerce' ); ?>" data-action="woocommerce_json_search_products_and_variations" data-multiple="true" data-exclude="<?php echo intval( get_the_ID() ); ?>">
                <?php
                    $product_ids = array_filter( array_map( 'absint', (array) get_post_meta( get_the_ID(), 'smile_bundled_products_ids', true ) ) );
                    $json_ids    = array();

                    foreach ( $product_ids as $product_id ) {
                        $product = wc_get_product( $product_id );
                        if ( is_object( $product ) ) {
                            echo '<option value="' . esc_attr( $product_id ) . '"' . selected( true, true, false ) . '>' . wp_kses_post( $product->get_formatted_name() ) . '</option>';
                        }
                    }

                ?>
            </select> <img class="help_tip" data-tip='Bundled products are products bundled to eachother so the website knows what the user is buying' src="<?php echo WC()->plugin_url(); ?>/assets/images/help.png" height="16" width="16" />
        </p>
    </div>
    <?php
}

// Save Fields
add_action( 'woocommerce_process_product_meta', 'smile_woo_add_custom_general_fields_save' );
function smile_woo_add_custom_general_fields_save( $post_id ){
    if( isset( $_POST['_product_serial_number'] ) ) {
        update_post_meta( $post_id, '_product_serial_number', esc_attr( $_POST['_product_serial_number'] ) );
    }
	
		
    if( isset( $_POST['_product_item_number'] ) ) {
        update_post_meta( $post_id, '_product_item_number', esc_attr( $_POST['_product_item_number'] ) );
    }

	
    // Text Field
    if( isset( $_POST['_product_brochure_url'] ) ) {
        update_post_meta( $post_id, '_product_brochure_url', esc_attr( $_POST['_product_brochure_url'] ) );
    }

    if( isset( $_POST['_product_mysmile_url'] ) ) {
        update_post_meta( $post_id, '_product_mysmile_url', esc_attr( $_POST['_product_mysmile_url'] ) );
    }


    $bundled_products = isset( $_POST['bundled_product_ids'] ) ? array_filter( array_map( 'intval', (array) wp_unslash( $_POST['bundled_product_ids'] ) ) ) : array();
    update_post_meta( $post_id, 'smile_bundled_products_ids', $bundled_products );
}