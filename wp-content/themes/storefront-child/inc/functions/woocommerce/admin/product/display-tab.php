<?php
/**
 * Product display tab
 */

// Add the tab
add_action( 'woocommerce_product_write_panel_tabs', 'smile_product_display_tab' );
function smile_product_display_tab() {
    ?>
    <li class="smile_product_display_tab"><a href="#smile_product_display_data"><?php _e('Display', 'woocommerce'); ?></a></li>
<?php
}


// Add fields to the tab
add_action('woocommerce_product_data_panels', 'smile_product_display_data');
function smile_product_display_data() {

    ?>
    <div id="smile_product_display_data" class="panel woocommerce_options_panel">

        <?php

        global $woocommerce, $post;

        // Select product display
        echo '<div class="options_group">';
        woocommerce_wp_select(array(
                'id'      => '_product_display',
                'label'   => __( 'Product display', 'woocommerce' ),
                'options' => array(
                    'circle'   => __( 'Circle', 'woocommerce' ),
                    'card'   => __( 'Card', 'woocommerce' ),
                    'data-bundle' => __( 'Data Bundle', 'woocommerce' ),
                    //'rounded' => __( 'Rounded', 'woocommerce' ),
                    //'image' => __( 'Image', 'woocommerce' )
                )
            )
        );
        echo '</div>';

        // Select sticker
        echo '<div class="options_group">';
        woocommerce_wp_select(array(
                'id'      => '_product_sticker',
                'label'   => __( 'Product sticker', 'woocommerce' ),
                'options' => array(
                    'none'   => __( 'None', 'woocommerce' ),
                    'ribbon'   => __( 'Ribbon', 'woocommerce' ),
                    'christmas-hat' => __('Christmas Hat', 'woocommerce'),
                )
            )
        );

        // Select sticker position
        echo '<div class="options_group">';
        woocommerce_wp_select(array(
                'id'      => '_product_sticker_position',
                'label'   => __( 'Product sticker position', 'woocommerce' ),
                'options' => array(
                    'none'   => __( 'None', 'woocommerce' ),
                    'left'   => __( 'Left', 'woocommerce' ),
                    'right'   => __( 'Right', 'woocommerce' )
                )
            )
        );

        // Select sticker color
        echo '<div class="options_group">';
        woocommerce_wp_select(array(
                'id'      => '_product_sticker_color',
                'label'   => __( 'Product sticker color', 'woocommerce' ),
                'options' => array(
                    'none'   => __( 'None', 'woocommerce' ),
                    'green'   => __( 'Green', 'woocommerce' ),
                    'orange'   => __( 'Orange', 'woocommerce' ),
                    'red'   => __( 'Red', 'woocommerce' ),
                    'black'   => __( 'Black', 'woocommerce' )
                )
            )
        );

        echo '</div>';

        // Select product sticker title
        echo '<div class="options_group">';
        woocommerce_wp_select(array(
                'id'      => '_product_sticker_title',
                'label'   => __( 'Product sticker title', 'woocommerce' ),
                'options' => array(
                    'none'   => __( 'None', 'woocommerce' ),
                    'Best Choice'   => __( 'Best Choice', 'woocommerce' ),
                    'Best Value'   => __( 'Best Value', 'woocommerce' ),
                    'Best Product!'   => __( 'Best Product!', 'woocommerce' ),
                    'Guaranteed'   => __( 'Guaranteed', 'woocommerce' ),
                    'Premium'   => __( 'Premium', 'woocommerce' ),
                    'Top seller'   => __( 'Top seller', 'woocommerce' ),
                    'Best offer'   => __( 'Best offer', 'woocommerce' ),
                    'Order now'   => __( 'Order now', 'woocommerce' ),
                    'Buy now'   => __( 'Buy now', 'woocommerce' ),
                    'Buy today'   => __( 'Buy today', 'woocommerce' ),
                    'New!'   => __( 'New!', 'woocommerce' )
                )
            )
        );
        echo '</div>';

        ?>

    </div>

<?php

}


// Save fields
add_action( 'woocommerce_process_product_meta', 'smile_product_display_data_save' );
function smile_product_display_data_save( $post_id ){

    if( !empty( $_POST['_product_display'] ) ) {
        update_post_meta( $post_id, '_product_display', esc_attr( $_POST['_product_display'] ) );
    }

    if( !empty( $_POST['_product_sticker'] ) ) {
        update_post_meta( $post_id, '_product_sticker', esc_attr( $_POST['_product_sticker'] ) );
    }

    if( !empty( $_POST['_product_sticker_position'] ) ) {
        update_post_meta( $post_id, '_product_sticker_position', esc_attr( $_POST['_product_sticker_position'] ) );
    }

    if( !empty( $_POST['_product_sticker_color'] ) ) {
        update_post_meta( $post_id, '_product_sticker_color', esc_attr( $_POST['_product_sticker_color'] ) );
    }

    if( !empty( $_POST['_product_sticker_title'] ) ) {
        update_post_meta( $post_id, '_product_sticker_title', esc_attr( $_POST['_product_sticker_title'] ) );
    }
}
