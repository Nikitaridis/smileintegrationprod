<?php

/**
 * Price filter
 * Split up the price so css can put selector on it
 */
//add_filter( 'woocommerce_get_price_html', 'smile_price_filter', 100, 2 );
//function smile_price_filter( $price, $product ){
//
////    $price = str_replace('&#8358;', '<span class="currency">&#8358;</span>', $price);
//    if(get_option('smile_settings_country') == 'Nigeria') {
//        $price = str_replace('&#8358;', '', $price);
//    }
//
//    // If the price contains comma's, the only replace the first comma
//    // leave comma in for now
////    $pos = strpos($price, ',');
////    if($pos !== false) {
////        $price = substr_replace($price,'<span class="thousands">',$pos,strlen(','));
////        $price = $price . '</span>';
////        return $price;
////    }
//
//    return $price;
//}


function smile_currency_symbol_filter($currency_symbol, $currency ) {
    return '<span class="currency-symbol">' . $currency_symbol . '</span>';
}

add_filter('woocommerce_currency_symbol', 'smile_currency_symbol_filter', 10, 2);