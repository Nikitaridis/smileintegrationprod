<?php

/**
 * @desc This is the location to add more query vars
 * @param $vars
 * @return array
 */
function smile_query_vars($vars) {
    $vars[] = "category";
    return $vars;
}

function add_body_classes($classes) {

    preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);
    if (count($matches) < 2) {
        preg_match('/Trident\/\d{1,2}.\d{1,2}; rv:([0-9]*)/', $_SERVER['HTTP_USER_AGENT'], $matches);
    }

    if (count($matches) > 1) {
        //Then we're using IE
        $version = $matches[1];
        $classes[] = 'ie-' . (int)$version;
    }

    return $classes;
}

if (!function_exists('ninja_forms_change_enquiry_select')) {
    function ninja_forms_change_enquiry_select($data) {
        if ($data['class'] == 'enquiry-smileservices') {
            $enquiry_smileservice_options = get_option('smile_enquiry_smileservice', array());

            $enquiry_smileservice_ninja_forms = array();
            foreach ($enquiry_smileservice_options as $enquiry_smileservice_option) {
                $enquiry_smileservice_ninja_forms[] = array(
                    "label" => $enquiry_smileservice_option,
                    "value" => null,
                    "calc" => null,
                    "selected" => 0,
                );
            }

            $data['list']['options'] = $enquiry_smileservice_ninja_forms;
        } else if ($data['class'] == 'enquiry-bandwidth') {
            $enquiry_bandwidth_options = get_option('smile_enquiry_bandwidth', array());

            $enquiry_bandwidth_ninja_forms = array();
            foreach ($enquiry_bandwidth_options as $enquiry_bandwidth_option) {
                $enquiry_bandwidth_ninja_forms[] = array(
                    "label" => $enquiry_bandwidth_option,
                    "value" => null,
                    "calc" => null,
                    "selected" => 0,
                );
            }

            $data['list']['options'] = $enquiry_bandwidth_ninja_forms;
        } else if ($data['class'] == 'enquiry-locations') {
            $enquiry_locations_options = get_option('smile_enquiry_locations', array());

            $enquiry_locations_ninja_forms = array();
            foreach ($enquiry_locations_options as $enquiry_locations_option) {
                $enquiry_locations_ninja_forms[] = array(
                    "label" => $enquiry_locations_option,
                    "value" => null,
                    "calc" => null,
                    "selected" => 0,
                );
            }

            $data['list']['options'] = $enquiry_locations_ninja_forms;
        }

        return $data;
    }
}


if (!function_exists('smile_woocommerce_states')) {
    function smile_woocommerce_states($states) {
        $states[COUNTRY_CODE] = STATES_LIST;
        return $states;
    }
}

function smile_shipping_to_delivery($translated) {
    $translated = str_ireplace('Shipping', 'Delivery', $translated);
    return $translated;
}

function smile_wc_get_template($located, $template_name, $args) {
    if ($template_name === 'checkout/thankyou.php') {
        if (isset($args['order'])) {
            $temp_is_recharge = get_post_meta($args['order']->id, '_smile_is_recharge', true);
            if (!empty($temp_is_recharge) && $temp_is_recharge == 1) {
                return get_stylesheet_directory() . '/recharge-order-completed.php';
            }
        }
    }
    return $located;
}



function smile_woocommerce_form_field_select($field, $key, $args, $value) {

    $states = smile_woocommerce_states(array())[COUNTRY_CODE];

    $zones = STATES_ZONES_LIST;

    if ( is_array( $states ) && !empty( $states ) ) {

        $field .= '<select id="custom_' . $key . '" style="display: none;">';
        $field .= '<option value="">'.__( 'Select a state&hellip;', 'woocommerce' ) .'</option>';

        foreach ( $states as $ckey => $cvalue ) {
            $field .= "<option value='" . esc_attr( $ckey ) . "' data-zones='" . esc_attr(json_encode($zones[$ckey])) . "' ".selected( $value, $ckey, false ) .">".__( $cvalue, 'woocommerce' ) ."</option>";
        }

        $field .= '</select>';
    }

    return $field;
}


function add_bcc_all_emails( $headers, $email_id, $order ) {

    $headers .= "Bcc: Zooma <smile-bcc@zooma.nl>" . "\r\n";

    return $headers;
}

// Change new order email recipient for recharge express
function wc_change_admin_new_order_email_recipient( $recipient, $order ) {
    global $woocommerce;
    $temp_is_recharge = get_post_meta($order->id, '_smile_is_recharge', true);
	if($temp_is_recharge){
		$recipient = "smile-bcc@zooma.nl";
    } else {
        // do nothing
    }
    return $recipient;
}

// Change processing email recipient for recharge express
function wc_change_customer_processing_order_email_recipient( $recipient, $order ) {
    global $woocommerce;
    $temp_is_recharge = get_post_meta($order->id, '_smile_is_recharge', true);
	if($temp_is_recharge){
		$recipient = "smile-bcc@zooma.nl";
    } else {
        // do nothing
    }
    return $recipient;
}

add_filter( 'default_checkout_billing_country', 'change_default_checkout_country' );

function change_default_checkout_country() {
    return COUNTRY_CODE; // country code
}

add_filter('woocommerce_email_recipient_new_order', 'wc_change_admin_new_order_email_recipient', 1, 2);
add_filter('woocommerce_email_recipient_customer_processing_order', 'wc_change_customer_processing_order_email_recipient', 1, 2);

add_filter('query_vars', 'smile_query_vars');
add_filter('body_class', 'add_body_classes');
add_filter('ninja_forms_field', 'ninja_forms_change_enquiry_select');


add_filter('woocommerce_states', 'smile_woocommerce_states');
add_filter('gettext', 'smile_shipping_to_delivery');
add_filter('wc_get_template', 'smile_wc_get_template', 10, 3);
add_filter('woocommerce_form_field_state', 'smile_woocommerce_form_field_select', 10, 4);

add_filter('woocommerce_email_headers', 'add_bcc_all_emails', 10, 3);
