<?php

/**
 * Define constants
 */
if (!function_exists('smile_setup')) :
    function smile_setup() {

        define('SMILE_VERSION', '1.0.0');
    }
endif;


/**
 * Enqueue css files
 */
function theme_enqueue_styles() {

    wp_enqueue_style('storefront-style', get_template_directory_uri() . '/style.css');

    wp_enqueue_style('smile-css', get_stylesheet_directory_uri() . '/css/main.css', array());

    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    if(preg_match('/MSIE/i',$u_agent) || preg_match('/Trident\/7.0; rv:11.0/', $_SERVER['HTTP_USER_AGENT'])) {
        wp_enqueue_style('smile-css-ie', get_stylesheet_directory_uri() . '/css/ie.css', array());
    }

    wp_enqueue_style('smile-font', get_stylesheet_directory_uri() . '/css/font/font.css', array());
    wp_enqueue_style('smile-fonticons', get_stylesheet_directory_uri() . '/css/fonticons/style.css', array());

    // Plugins
    wp_enqueue_style('smile-plugin-magnific-popup', get_stylesheet_directory_uri() . '/css/plugin/magnific-popup.css', array());
    wp_enqueue_style('smile-plugin-circlehoverevents-popup', get_stylesheet_directory_uri() . '/css/plugin/circlehoverevents.css', array());
    wp_enqueue_style('smile-plugin-slick-carousel', get_stylesheet_directory_uri() . '/css/plugin/slick/slick.css', array());
    wp_enqueue_style('smile-plugin-slick-theme-carousel', get_stylesheet_directory_uri() . '/css/plugin/slick/slick-theme.css', array());
}

/**
 * Enqueue js files
 */
function theme_enqueue_scripts() {
    wp_enqueue_script('smile-main-script', get_stylesheet_directory_uri() . '/js/main.js', array(), SMILE_VERSION, true);

    // Libs
    wp_enqueue_script('smile-libs-modernizr.custom.36067', get_stylesheet_directory_uri() . '/js/libs/modernizr.custom.36067.js', array(), SMILE_VERSION, true);

    // Plugins
    wp_enqueue_script('smile-plugin-magnific-popup', get_stylesheet_directory_uri() . '/js/plugin/jquery.magnific-popup.min.js', array(), SMILE_VERSION, true);
    wp_enqueue_script('smile-plugin-slick-carousel', get_stylesheet_directory_uri() . '/js/plugin/slick.js', array(), SMILE_VERSION, true);
    wp_enqueue_script('smile-placeholder', get_stylesheet_directory_uri() . '/js/plugin/jquery.placeholder.min.js', array(), SMILE_VERSION, true);
    wp_enqueue_script('smile-waypoints', get_stylesheet_directory_uri() . '/js/plugin/jquery.waypoints.min.js', array(), SMILE_VERSION, true);
    wp_enqueue_script('smile-bday-picker', get_stylesheet_directory_uri() . '/js/bday-picker.min.js', array(), SMILE_VERSION, true);


    // General: make info available for js
    wp_localize_script('smile-main-script', 'WebsiteConfig',
        array(
            'URL' => get_site_url(),
            'REQUEST_URI' => $_SERVER['REQUEST_URI'],
            'ROOT_URL' => get_stylesheet_directory_uri(),
            'ajaxurl' => admin_url( 'admin-ajax.php', 'relative'),
            'environment' => ENV,
        )
    );
}

function theme_enqueue_admin_styles() {
    wp_enqueue_style('smile-admin-style', get_stylesheet_directory_uri() . '/css/admin.css');
}

function theme_enqueue_admin_scripts() {
    wp_enqueue_script('smile-admin-script', get_stylesheet_directory_uri() . '/js/admin.js', array(), SMILE_VERSION, true);
}



/**
 * Remove hook(s) from parent theme
 */
function remove_styles_from_parent_theme() {
    remove_action( 'wp_enqueue_scripts', 'storefront_add_customizer_css', 130);
}

if(!function_exists('remove_admin_bar')) {
    function remove_admin_bar()
    {
        if(wp_get_current_user()->user_email == 'kevin@zooma.nl' || wp_get_current_user()->user_email == 'tijmen@zooma.nl')
        {
            show_admin_bar(false);
        }
    }
}
