<div class="row">
    <div class="twelve columns">
        <div class="unfold-shops trigger" data-trigger-category="Shops" data-trigger-title="Show all shops">
            <a href="" title="Show all shops">Show all shops</a>
            <div class="icon-chevron-circle-down"></div>
        </div>
    </div>
</div>