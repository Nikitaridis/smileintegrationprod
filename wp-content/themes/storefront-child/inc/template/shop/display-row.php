<?php
/**
 * Display 1 shop
 *
 * @package storefront-child
 */
?>

<div class="row">
    <div class="twelve columns">

        <div class="shop-container<?php echo isset($DisplayClass) && !empty($DisplayClass) ? ' ' . $DisplayClass : '' ?>">

            <div class="twelve columns">
                <h3><?php echo $Shop->post_title; ?></h3>
            </div>

            <div class="four columns">
                <?php if(isset($ShopMetaData['shop_type'][0]) && !empty($ShopMetaData['shop_type'][0])) : ?>
                    <div class="column-title shop-type">
                        <?php if($ShopMetaData['shop_type'][0] == 'SHOP') : ?>
                            <span class="circle shop">S</span>
                        <?php elseif($ShopMetaData['shop_type'][0] == 'KIOSK') : ?>
                            <span class="circle kiosk">K</span>
                        <?php else : ?>
                            <span class="circle">AR</span>
                        <?php endif; ?>
                        </span>
                        <?php echo $ShopMetaData['shop_type'][0]; ?>
                    </div>
                <?php endif; ?>

                <?php if(isset($Shop->directionsUrl) && !empty($Shop->directionsUrl)) : ?>
                    <a class="directions trigger" data-trigger-category="Shops" target="_blank" href="<?php echo $Shop->directionsUrl; ?>" title="Directions">Directions<span class="icon-angle-right"></span></a>
                <?php endif; ?>

                <?php if(isset($Shop->displayDistance) && !empty($Shop->displayDistance)) : ?>
                    <p class="distance"><?php echo $Shop->displayDistance . ' from <b>' . $searchQuery . '</b>'; ?></p>
                <?php endif; ?>

            </div>

            <div class="four columns">
                <div class="column-title">Address</div>
                <address><?php echo !empty($ShopMetaData['shop_address'][0]) ? nl2br($ShopMetaData['shop_address'][0]) : ''; ?></address>
            </div>

            <div class="four columns">
                <div class="column-title">Opening hours</div>
                <p><?php echo !empty($ShopMetaData['shop_opening_hours'][0]) ? nl2br($ShopMetaData['shop_opening_hours'][0]) : ''; ?></p>
            </div>

        </div>
    </div>


</div>