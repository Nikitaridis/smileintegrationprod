<?php
/**
 * Product stickers
 */

$productSticker = get_post_meta( $product->get_id(), '_product_sticker', true );
$productStickerPosition = get_post_meta( $product->get_id(), '_product_sticker_position', true );
$productStickerColor = get_post_meta( $product->get_id(), '_product_sticker_color', true );
$productStickerTitle = get_post_meta( $product->get_id(), '_product_sticker_title', true );

if($productSticker == 'ribbon' && $productStickerPosition != 'none' && $productStickerColor != 'none' && $productStickerTitle != 'none') {

    echo '<div class="ribbon-wrapper '. $productStickerPosition .'">
              <div class="ribbon '. $productStickerPosition .' '. $productStickerColor .'">'. $productStickerTitle .'</div>
          </div>';
} else if($productSticker == 'christmas-hat') {
    echo '<div class="christmas-hat '. $productStickerPosition .'">
              <img src="'. get_stylesheet_directory_uri() .'/images/products/christmas-hat.png">
          </div>';
}
