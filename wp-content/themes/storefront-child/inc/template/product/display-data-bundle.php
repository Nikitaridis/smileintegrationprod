<?php
/**
 * Display 1 product (data bundle)
 *
 * @package storefront-child
 */
$product_count = $Products->found_posts;
?>

<?php if($product_count == 1) : ?>
<div class="twelve columns">

<?php elseif($product_count == 2) : ?>
<div class="six columns">

<?php elseif($product_count == 3) : ?>
<div class="four columns">

<?php elseif($product_count > 3) : ?>
<div class="three columns data-bundle-item">

    <?php endif; ?>

    <div id="product-item-<?php the_ID(); ?>" class="product-circle data-bundle-circle" data-databundles-page-url="<?php echo PageType::getPageUrl('data-bundles'); ?>">

        <?php
        $product = new WC_Product( get_the_ID() );

        // Databundle
        $dataBundleHTML = '';
        $dataBundleGB = $product->get_attribute('pa_data-bundle-gb');
        $dataBundleMB = $product->get_attribute('pa_data-bundle-mb');
        $dataBundleUnlimited = $product->get_attribute('pa_data-bundle-unlimited');
        if(!empty($dataBundleGB)) {
            $dataBundleHTML = $dataBundleGB . '<span class="data-bundle-size">GB</span>';
        }
        elseif(!empty($dataBundleMB)) {
            $dataBundleHTML = $dataBundleMB . '<span class="data-bundle-size">MB</span>';
        }
        elseif(!empty($dataBundleUnlimited)) {
            $dataBundleHTML = '<span class="text">' . $dataBundleUnlimited . '</span>';
        }

        $circleColor = $product->get_attribute('pa_data-bundle-color');

        // Product sticker
        include(locate_template('inc/template/product/sticker.php'));
        ?>
        <div class="image-container data-bundle-container">
            <h3 class="title <?php echo $circleColor ?>"><!--<?php echo $product->get_attribute('pa_data-bundle-title'); ?>-->&nbsp</h3>
            <span class="data-bundle <?php echo $circleColor ?>"><?php echo $dataBundleHTML; ?></span>

            <p class="validity">Valid <?php echo $product->get_attribute('pa_data-bundle-validity'); ?></p>

            <?php echo $product->get_price_html(); ?>
        </div>

    </div><!-- End product-item -->
</div>