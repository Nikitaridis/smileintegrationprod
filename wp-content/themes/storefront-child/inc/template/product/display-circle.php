<?php
/**
 * Display 1 product
 *
 * @package storefront-child
 */
$product_count = $Products->found_posts;
?>


<?php if($product_count == 1) : ?>
    <div class="twelve columns">

<?php elseif($product_count == 2 || $product_count == 4) : ?>
    <div class="six columns">

<?php elseif($product_count > 2) : ?>
    <div class="four columns">

<?php endif; ?>

    <div id="product-item-<?php the_ID(); ?>" class="product-circle">

        <?php
            $product = new WC_Product( get_the_ID() );

            // Product sticker
            include(locate_template('inc/template/product/sticker.php'));
        ?>

        <?php if ( has_post_thumbnail() ): ?>
            <div class="image-container">
                <?php  echo $product->get_image('full'); ?>

                <?php if($product->get_attribute('data-speed') != '') : ?>
                    <div class="special">
                        <span class="superscript">UP TO</span>
                        <div class="value"><?php echo $product->get_attribute('data-speed'); ?></div>
                    </div>
                <?php elseif($product->get_attribute('data-bundle-gb') != '') : ?>
                    <div class="special">
                        <?php echo $product->get_attribute('data-bundle-gb'); ?><span class="gb">GB</span>
                        <div class="included">INCLUDED</div>
                    </div>
                <?php elseif($product->get_attribute('data-bundle-mb') != '') : ?>
                    <div class="special">
                        <?php echo $product->get_attribute('data-bundle-mb'); ?><span class="gb">MB</span>
                        <div class="included">INCLUDED</div>
                    </div>
                <?php endif; ?>

                <?php echo $product->get_price_html(); ?>
            </div>
        <?php endif; ?>

        <h2><?php echo $product->get_title(); ?></h2>

        <p><?php echo $product->get_attribute('product-subtitle'); ?></p>

        <a class="link green" href="<?php echo $product->get_permalink(); ?>">Buy now</a>

    </div><!-- End product-item -->
</div>