<?php
/**
 * Display 1 product
 *
 * @package storefront-child
 */
?>


<div class="twelve columns">
    <div id="product-item-<?php the_ID(); ?>" class="product-card-item columns twelve">

        <?php
            $product = new WC_Product( get_the_ID() );

            // Product sticker
            include(locate_template('inc/template/product/sticker.php'));
        ?>

        <div class="five columns">
            <?php if ( has_post_thumbnail() ): ?>
                <figure>
                    <?php  echo $product->get_image('full'); ?>
                </figure>
            <?php endif; ?>
        </div>

        <div class="seven columns">
            <div class="details-container">

                <h2><?php echo $product->get_title(); ?></h2>
                <p><?php echo $product->get_attribute('product-subtitle'); ?></p>

                <div class="six columns collapsed">
                    <?php echo $product->get_price_html(); ?>
                    <?php if($product->get_attribute('data-bundle') != '') : ?>
                        <div class="special">
                            <span class="gb"><?php echo $product->get_attribute('data-bundle'); ?>GB</span> INCLUDED
                        </div>
                    <?php endif; ?>
                </div>
                <div class="six columns collapsed">
                    <a class="link" href="<?php echo $product->get_permalink(); ?>">Buy now</a>
                    <?php
                        include(locate_template('woocommerce/single-product/add-to-cart/simple.php'));
                    ?>
                </div>

            </div>
        </div>
    </div><!-- End product-item -->
</div>