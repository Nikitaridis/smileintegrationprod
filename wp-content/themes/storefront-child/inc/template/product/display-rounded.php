<?php
/**
 * Display 1 product
 *
 * @package storefront-child
 */
$product_count = $Products->found_posts;
?>



<?php
// Niet in gebruik op dit moment!
?>

<?php if($product_count == 1) : ?>
    <div class="twelve columns">

<?php elseif($product_count == 2) : ?>
    <div class="six columns">

<?php elseif($product_count > 2) : ?>
    <div class="four columns">

<?php endif; ?>

    <div id="product-item-<?php the_ID(); ?>" class="product-rounded">

        <?php
        $product = new WC_Product( get_the_ID() );

        // Product sticker
        include(locate_template('inc/template/product/sticker.php'));
        ?>

        <?php if ( has_post_thumbnail() ): ?>
            <figure>
                <span class="helper"></span>
                <?php  echo $product->get_image('full'); ?>
            </figure>
        <?php endif; ?>

        <h2><?php echo $product->get_title(); ?></h2>
        <p><?php echo $product->get_attribute('product-subtitle'); ?></p>
        <a class="link green" href="<?php echo $product->get_permalink(); ?>">Buy now</a>

        <div class="special green-background">
            <span class="gb"><?php echo $product->get_attribute('data-bundle'); ?>GB</span> INCLUDED
        </div>

        <?php echo $product->get_price_html(); ?>
    </div><!-- End product-item -->
</div>