<?php
/**
 * Show data bundles as horizontal circles (shortlist)
 */
?>


<section id="data-bundles">

    <div class="header">
        <div class="row">
            <div class="twelve columns">
                <h1 class="green">Data bundles</h1>
                <p>These are the most popular data bundles on the Smile network</p>
            </div>
        </div>
    </div>


    <div class="bundles">
        <div class="row ">
            <div class="twelve columns">
                <ul class="ch-grid">


                    <?php
                    $bundlesHTML = '';
                    foreach($upsells as $key => $upsellID) {

                        $linkedProduct = new WC_Product($upsellID);

                        // Databundle
                        $dataBundleHTML = '';
                        $dataBundleGB = $linkedProduct->get_attribute('pa_data-bundle-gb');
                        $dataBundleMB = $linkedProduct->get_attribute('pa_data-bundle-mb');
                        $dataBundleUnlimited = $linkedProduct->get_attribute('pa_data-bundle-unlimited');
                        if(!empty($dataBundleGB)) {
                            $dataBundleHTML = $dataBundleGB . '<span class="data-bundle-size">GB</span>';
                        }
                        elseif(!empty($dataBundleMB)) {
                            $dataBundleHTML = $dataBundleMB . '<span class="data-bundle-size">MB</span>';
                        }
                        elseif(!empty($dataBundleUnlimited)) {
                            $dataBundleHTML = '<span class="text">' . $dataBundleUnlimited . '</span>';
                        }

                        // Circle color
                        $cssClasses = array('starter', 'basic', 'premier', 'deluxe');
                        $circleColor = $linkedProduct->get_attribute('pa_data-bundle-color');
                        $outOfBundle = $linkedProduct->get_attribute('pa_data-bundle-out-of-bundle');

                        $bundlesHTML .= '
                <li>
                    <div class="ch-item '. $circleColor .'">
                        <div class="ch-info-wrap">
                            <div class="ch-info">
                                <div class="ch-info-front '. $circleColor .'">
                                    <h3><!--'. $linkedProduct->get_attribute('pa_data-bundle-title') .'-->&nbsp</h3>
                                    <span class="data-bundle">'. $dataBundleHTML .'</span>
                                    <!--<p class="subtitle">'. $linkedProduct->get_attribute('pa_data-bundle-subtitle') .'</p>-->
                                </div>
                                <div class="ch-info-back">
                                    <h3>'. $linkedProduct->get_attribute('pa_data-bundle-title') .'</h3>
                                    <ul>
                                        <li>'. $linkedProduct->get_price_html() .'</li>
                                        <li>'. $linkedProduct->get_attribute('pa_data-bundle-validity') .' validity</li>'.
                                        ((!empty($outOfBundle)) ? '<li>'. $outOfBundle .'/GB Out of Bundle</li>' : '') .'
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>';
                    }
                    echo $bundlesHTML;
                    ?>



                </ul>
            </div>
        </div>
        <div class="row ">
            <div class="six columns offset-3">
                <a class="all-bundles" href="<?php echo PageType::getPageUrl('data-bundles'); ?>">Show me the full list of data plans</a>
                <div class="support">
                    <p>Can’t find what you are looking for?</p>
                    <a href="" class="open-chat">Chat with us (24/7)</a>
                </div>
            </div>
        </div>
    </div>


</section>