<?php
/**
 * Various site tracking options
 */


if ( ! function_exists( 'smile_google_analytics' ) ) {
    /**
     * Google analytics js code
     * @since  1.0.0
     */
    function smile_google_analytics (){

        echo  "
        <script>
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', '". GA_CODE ."', 'auto');
          ga('send', 'pageview');

        </script>
        ";
    }
}


if ( ! function_exists( 'smile_google_analytics_ecommerce' ) ) {
    /**
     * Google analytics ecommerce js code
     * @since  1.0.0
     */
    function smile_google_analytics_ecommerce (){

        if(isset($_GET['key'])) {

            $args = array(
                'post_type' => 'shop_order',
                'post_status' => 'wc-processing',
                'meta_key' => '_order_key',
                'meta_value' => $_GET['key'],
                'posts_per_page' => '1'
            );
            $my_query = new WP_Query($args);
            $customer_orders = $my_query->posts;

            if(isset($customer_orders[0])) {

                $post = $customer_orders[0];

                if(isset($post->ID)) {

                    $order = new WC_Order($post->ID);

                    // Transaction Data
                    $transaction = <<<HTML
                    ga('ecommerce:addTransaction', {
                        'id': '{$order->id}',
                        'affiliation': 'Smile',
                        'revenue': '{$order->order_total}',
                        'shipping': '{$order->order_shipping}',
                        'tax': '{$order->order_tax}'
                    });
HTML;

                    // List of Items Purchased.
                    $puchasedItems = '';
                    foreach($order->get_items() as $item_id => $item) {

                        $productId = wc_get_order_item_meta($item_id, '_product_id');

                        $product = new WC_Product($productId);
                        $quantity = wc_get_order_item_meta($item_id, '_qty');

                        $puchasedItems .= <<<HTML

                        ga('ecommerce:addItem', {
                          'id': '{$order->id}',
                          'name': '{$item['name']}',
                          'sku': '{$productId}',
                          'category': 'Product',
                          'price': '{$product->price}',
                          'quantity': '{$quantity}'
                        });

HTML;
                    }

?>
                    <script>
                    ga('require', 'ecommerce');
                    <?php
                    echo $transaction;
                    echo $puchasedItems;
                    ?>
                    ga('ecommerce:send');
                    </script>
<?php

                }
            }
        }
    }
}



if ( ! function_exists( 'smile_google_analytics_conversion_tracking' ) ) {

    /**
     * Google conversion tracking
     */
    function smile_google_analytics_conversion_tracking() {

        if(isset($_GET['key'])) {

            if(COUNTRY == 'Tanzania') {

                // Google Code for Online Order Placed Conversion Page
                echo '<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 923633005;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "0-vJCPf_tGcQ7Yq2uAM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="// www.googleadservices.com/pagead/conversion/923633005/?label=0-vJCPf_tGcQ7Yq2uAM&amp;guid=ON&amp;script=0 "/>
</div>
</noscript>';
            }
        }
    }
}



if ( ! function_exists( 'smile_google_adwords' ) ) {
    /**
     * Google adwords js code
     * @since  1.0.0
     */
    function smile_google_adwords (){

        if(isset($_GET['key'])) {

            if(COUNTRY == 'Nigeria') {

                echo '<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 935959530;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "YqqECJHX7GQQ6remvgM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/935959530/?label=YqqECJHX7GQQ6remvgM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>';
            }
        }
    }
}




if ( ! function_exists( 'smile_vwo' ) ) {
    /**
     * VWO js code
     * @since  1.0.0
     */
    function smile_vwo (){

        echo "
        <script type='text/javascript'>
        var _vwo_code=(function(){
        var account_id=172043,
        settings_tolerance=2000,
        library_tolerance=2500,
        use_existing_jquery=false,
        // DO NOT EDIT BELOW THIS LINE
        f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
        </script>
        ";
    }
}


if ( ! function_exists( 'smile_hotjar' ) ) {
    /**
     * Hotjar js code
     * @since  1.0.0
     */
    function smile_hotjar (){

        echo "
<script>
(function(h,o,t,j,a,r){
    h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:79537,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>
        ";
    }
}

