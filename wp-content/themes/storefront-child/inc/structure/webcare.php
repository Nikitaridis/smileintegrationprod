<?php
/**
 * Template functions used for the displaying webcare (right) menu
 *
 * @package storefront-child
 */


if (!function_exists('smile_webcare_menu')) {
    /**
     * Display webcare menu
     * @since  1.0.0
     */
    function smile_webcare_menu()
    {
        ?>

        <?php
        // Page post object
        $Page = get_post();
        $PageMetaData = get_post_meta($Page->ID);
        $ClassSelected = '';
        $CurrentPageType = '';
        if (isset($PageMetaData['smile_page_type'][0]) && !empty($PageMetaData['smile_page_type'][0])) {
            $CurrentPageType = $PageMetaData['smile_page_type'][0];
        }
        ?>

        <section id="webcare-menu">
            <h3>Customer service</h3>
            <nav>
                <ul>
                    <li>
                        <a href="#" class="open-chat trigger" data-trigger-category="Webcare-menu" title="Chat with us">
                            <span class="icon-bubbles icon-left"></span>
                            <span>Chat with us</span>
<!--                            <span class="subtitle">We are online</span>-->
                            <span class="icon-angle-right icon-right"></span>
                        </a>
                    </li>

                    <?php if ($CurrentPageType != 'contact') : ?>
                    <li>
                        <a href="<?php echo PageType::getPageUrl('contact'); ?>" class="trigger" data-trigger-category="Webcare-menu" title="Send an email">
                            <span class="icon-envelop icon-left"></span>
                            Send an email
                            <span class="icon-angle-right icon-right"></span>
                        </a>
                    </li>
                    <?php endif; ?>

                    <?php if($CurrentPageType != 'help') : ?>
                    <li>
                        <a href="<?php echo PageType::getPageUrl('help'); ?>" class="trigger" data-trigger-category="Webcare-menu" title="View Knowledge Base">
                            <span class="icon-cog icon-left"></span>
                            View Knowledge Base
                            <span class="icon-angle-right icon-right"></span>
                        </a>
                    </li>
                    <?php endif; ?>

<!--                    <li>-->
<!--                        <a target="_blank" href="https://twitter.com/smilecomsng" class="trigger" data-trigger-category="Webcare-menu" title="Ask your question on Twitter">-->
<!--                            <span class="icon-twitter icon-left"></span>-->
<!--                            Ask your question on Twitter-->
<!--                            <span class="icon-angle-right icon-right"></span>-->
<!--                        </a>-->
<!--                    </li>-->

<!--                    <li>-->
<!--                        <a target="_blank" href="https://www.facebook.com/SmileCommunicationsNigeria" class="trigger" data-trigger-category="Webcare-menu" title="Ask your question on Facebook">-->
<!--                            <span class="icon-facebook icon-left"></span>-->
<!--                            Ask your question on Facebook-->
<!--                            <span class="icon-angle-right icon-right"></span>-->
<!--                        </a>-->
<!--                    </li>-->
                    <?php foreach (WEBCARE_LINKS as $link):?>
                        <li>
                            <a href="<?php echo $link['href'];?>" class="big trigger" data-trigger-category="Webcare-menu" title="<?php echo $link['title'];?>">
                                <span class="<?php echo $link['icon'];?> icon-left"></span>
                                <span><?php echo $link['title'];?></span>
                                <span class='subtitle'><?php echo $link['subtitle'];?></span>
                                <span class="icon-angle-right icon-right"></span>
                            </a>
                        </li>
                    <?php endforeach;?>
                                                
                   
                </ul>
            </nav>
        </section>

        <?php
    }
}


if (!function_exists('smile_webcare_faq')) {
    /**
     * Display webcare faq
     * @since  1.0.0
     */
    function smile_webcare_faq()
    {
        //Display questions in category
        $category = get_query_var('category');
        if (isset($category) && !empty($category)) {
            $category_id = get_term_by('slug', $category, 'faq_category')->term_id;

            $ordering = new Smile_FAQ_Ordering();

            //Display popular questions
            $popular_posts = get_posts(array(
                    'post_type' => 'smile_faq',
                    'numberposts' => -1,
                    'meta_query' => array(
                        array(
                            'key' => 'faq_popular',
                            'value' => true
                        )
                    ),
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'faq_category',
                            'field' => 'slug',
                            'terms' => $category)
                    )
                )
            );

            if (!empty($popular_posts)) {
                $popular_posts = $ordering->redo_faq_question_ordering($category_id, $popular_posts);
                ?>

                <h3>Popular questions</h3>

                <div class="accordion">
                    <?php foreach ($popular_posts as $key => $popular_post) {
                        $post_id = $popular_post->ID;
                        ?>
                        <div class="accordion-section">
                            <a class="accordion-section-title <?php echo $popular_post->post_title; ?>" href="#accordion-<?php echo $key;?>"><?php echo $popular_post->post_title; ?></a>
                            <div id="accordion-<?php echo $key;?>" class="accordion-section-content">
                                <p><?php echo wpautop(get_post_meta($post_id, 'faq_answer', true)); ?></p>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php
            }

            $term = get_term_by('slug', $category, 'faq_category');
            echo '<h3>' . $term->name . '</h3>';
            // show faq
            $posts = $ordering->redo_faq_question_ordering($category_id, get_posts(array(
                    'post_type' => 'smile_faq',
                    'numberposts' => -1,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'faq_category',
                            'field' => 'slug',
                            'terms' => $category)
                    ))
            ));
            ?>

            <div class="accordion">
                <?php foreach ($posts as $key => $mypost) {
                    $post_id = $mypost->ID;

                    $class = '';
                    if ($key % 2 == 0) {
                        $class = 'even';
                    }
                    ?>
                    <div class="accordion-section">
                        <a class="accordion-section-title <?php echo $class?>" href="#accordion-<?php echo $key;?>"><?php echo $mypost->post_title; ?></a>
                        <div id="accordion-<?php echo $key;?>" class="accordion-section-content <?php echo $class?>">
                            <p><?php echo wpautop(get_post_meta($post_id, 'faq_answer', true)); ?></p>
                        </div>
                    </div>
                <?php } ?>
            </div>

<!--            <div id="st-accordion" class="st-accordion">-->
<!--                <ul>-->
<!---->
<!--                    --><?php //foreach ($posts as $key => $mypost) {
//                        $post_id = $mypost->ID;
//
//                        $class = '';
//                        if ($key % 2 == 0) {
//                            $class = 'even';
//                        }
//                    ?>
<!---->
<!--                        <li class="--><?php //echo $class; ?><!--">-->
<!--                            <a class="trigger" data-trigger-category="FAQ" title="--><?php //echo $mypost->post_title; ?><!--" href="#">-->
<!--                                --><?php //echo $mypost->post_title; ?>
<!--                            </a>-->
<!---->
<!--                            <div class="st-content" style="display: none;">-->
<!--                                <p>--><?php //echo wpautop(get_post_meta($post_id, 'faq_answer', true)); ?><!--</p>-->
<!--                            </div>-->
<!--                        </li>-->
<!--                    --><?php //} ?>
<!--                </ul>-->
<!--            </div>-->
            <?php
        }
    }
}
