<?php
/**
 * Template functions used for the displaying products
 *
 * @package storefront-child
 */

if ( ! function_exists( 'smile_products_block' ) ) {
    /**
     * Display the footer copyright
     * @since  1.0.0
     */
    function smile_products_block() {

        $CategoryObj = new Category();
        $CategoryObj->show_count = 0;
        $Categories = $CategoryObj->getCategory();
        $isBusiness = PageType::isPageType('business');
        $PlatformMeta = $isBusiness ? 'business' : 'consumer';
        ?>

        <?php if(count($Categories) > 0) : ?>
            <section id="products" class="section-title">
        <?php endif; ?>

        <?php
        foreach($Categories as $CategoryKey => $Category) {

            $ProductObj = new Product();
            $ProductObj->setTaxQuery('product_cat', 'term_id', $Category->term_id, 'IN');
            $ProductObj->setTaxQuery('product_visibility', 'slug', 'exclude-from-catalog', 'NOT EXISTS');
            //$ProductObj->setMetaQuery('_visibility', array('catalog', 'visible'), 'IN');
            $ProductObj->setMetaQuery('smile_platform_type', $PlatformMeta, '=');

            if($Category->slug == 'data-bundle')
            {
                $Products = $ProductObj->getProduct(4);
            }else
            {
                $Products = $ProductObj->getProduct(4);
            }

            ?>
            <?php if ( $Products->have_posts() ) : ?>

                <?php if($CategoryKey == 0) : ?>
                    <div class="products-promotion fill-screen">
                <?php else : ?>
                    <div class="products-block<?php echo ' ' . $Category->slug ?> fill-screen">
                <?php endif; ?>

                    <div class="row">
                        <div class="twelve columns section-title section-text-center">
                            <h1><?php echo $Category->name; ?></h1>
                        </div>
                        <?php while ( $Products->have_posts() ) : $Products->the_post(); ?>

                            <?php
                            $productDisplay = get_post_meta( get_the_ID(), '_product_display', true );
                            switch ($productDisplay) {
                                case 'card':
                                    $template = 'inc/template/product/display-card.php';
                                    if($Category->count == 1) {
                                        $template = 'inc/template/product/display-card-item.php';
                                    }
                                    break;
                                case 'data-bundle':
                                    $template = 'inc/template/product/display-data-bundle.php';
                                    break;
                                case 'circle':
                                    $template = 'inc/template/product/display-circle.php';
                                    break;
                                case 'rounded':
                                    $template = 'inc/template/product/display-rounded.php';
                                    break;
                                case 'image':
                                    $template = 'inc/template/product/display-image.php';
                                    break;
                                default:
                                    $template = 'inc/template/product/display-card.php';
                                    if($Category->count == 1) {
                                        $template = 'inc/template/product/display-card-item.php';
                                    }
                            }
                            include(locate_template($template));
                            ?>

                        <?php endwhile; ?>
                    </div>

                    <?php if($Category->slug == 'data-bundle') : ?>
                    <div id="data-bundles">
                        <div class="row bundles">
                            <div class="six columns offset-3">
                                <a class="all-bundles" href="<?php echo $isBusiness ? PageType::getPageUrl('data-bundles-business') : PageType::getPageUrl('data-bundles'); ?>">Show me the full list of data plans</a>
                                <div class="support">
                                    <p>Can’t find what you are looking for?</p>
                                    <a href="" class="open-chat">Chat with us (24/7)</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>

                </div>

            <?php endif; ?>
        <?php } ?>

        </section> <!-- Products Section End-->


    <?php
    }
}

if(!function_exists('smile_show_order_meta')) {
    /**
     * Show extra fields for customer
     * @param WC_Order $order
     */
    function smile_show_order_meta($order) {
        echo '<p><strong>'.__('Customer id').':</strong> ' . get_post_meta( $order->get_id(), 'smile_order_customer_id', true ) . '</p>';
        echo '<p><strong>'.__('Recharge account id').':</strong> ' . get_post_meta( $order->get_id(), 'smile_recharge_account_id', true ) . '</p>';
        echo '<p><strong>'.__('Recharge product instance id').':</strong> ' . get_post_meta( $order->get_id(), 'smile_recharge_product_instance_id', true ) . '</p>';
    }
}