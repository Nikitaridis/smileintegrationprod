<?php
/**
 * Template functions used for the displaying smile coverage
 *
 * @package storefront-child
 */

if ( ! function_exists( 'google_maps_js' ) ) {
    /**
     * Load google maps js
     * @since  1.0.0
     */
    function google_maps_js()
    {
        if (is_page('coverage') || is_product()) {
            ?>
                <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?<?php if(defined('MAPS_API_KEY')){
            echo('key=' . MAPS_API_KEY);
        } ?> " xmlns="http://www.w3.org/1999/html"></script>
            <?php
        }
    }
}


if ( ! function_exists( 'smile_coverage_maps' ) ) {
    /**
     * Display the coverage map
     * @since  1.0.0
     */
    function smile_coverage_maps() {

        $location = '';
        if(isset($_GET['location']) && !empty($_GET['location'])) {
            $location = $_GET['location'];

            wp_localize_script('smile-coverage-script', 'Coverage',
                array(
                    'location' => $location
                )
            );
        }

        add_action( 'wp_footer', 'smile_coverage_map_init', 100, 1);

        do_action('smile_coverage_map_html');
    }
}


if(!function_exists('smile_coverage_map_init'))
{
    function smile_coverage_map_init()
    {
        if (is_page('coverage')) {
            ?>
            <script type="text/javascript">
                jQuery(document).ready(function($) {
                    google.maps.event.addDomListener(window, 'load', Smile_Coverage.loadMap);
                });
            </script>
            <?php
        }
    }
}

if(!function_exists('smile_coverage_map_html'))
{
    function smile_coverage_map_html()
    {
        $cities_utils = new Smile_Cities_Utils();
        $cities_query = $cities_utils->getCitiesQuery();
    ?>
        <section id="coverage-map" class="fill-screen">
            <div id="map"></div>
            <div id="form-container" class="row hidden">

                <div class="coverage-search-container twelve columns">
                    <form id="coverage-search" method="GET" name="coverage-search">

                            <select id="coverage-city" class="dropdown">
                                <option value="">Select your city</option>
                                <?php
                                if($cities_query->have_posts()) {
                                    while($cities_query->have_posts()) :
                                        $cities_query->the_post(); ?>
                                        <option value="<?php echo get_post()->post_name; ?>"><?php echo get_the_title(); ?></option>
                                    <?php endWhile;
                                    wp_reset_postdata();
                                } ?>
                            </select>
                            <div class="search-container">
                                <input type="text" placeholder="Enter your location" id="location" name="location" value="">
                                <button class="button" type="submit" form="coverage-search">
                                    <span class="icon-search"></span>
                                    <span class="button-txt">Check coverage</span>
                                </button>
                            </div>

                        <div class="use-my-location-container">
                            <span><span class="use-my-location-or">or</span> <a id="use-my-location" href="">use my location</a></span>
                        </div>
                    </form>
                </div>

            </div>
        </section>

        <div id="fullscreen-loader">
            <div class="spinner"></div>
            <div class="message">Loading map</div>
        </div>
    <?php
    }
}




if ( ! function_exists( 'smile_coverage_block' ) ) {
    /**
     * Display the coverages from smile-coverage plugin
     * @since  1.0.0
     */
    function smile_coverage_block() {

        $args = array(
            'post_status'       => 'publish',
            'posts_per_page'    => 10,
            'post_type'         => 'smile_coverage',
            'orderby'           => 'menu_order',
            'order'             => 'ASC'
        );
        $Coverage = new WP_Query($args);
    ?>

        <section id="coverage" class="fill-screen">
            <div class="row">
                <div class="twelve columns section-title section-text-center">
                    <h1>We've got you covered</h1>
                    <div class="coverage-carousel">

                        <?php
                        $coverageSlide= '';
                        while ( $Coverage->have_posts() ) {

                            $Coverage->the_post();
                            $Post = get_post();
                            $PostMetaData = get_post_meta($Post->ID);

                            if(isset($PostMetaData['coverage_description'][0]) &&
                                has_post_thumbnail() &&
                                !empty($Post->post_title)
                            ) {

                                $PostImageData = wp_get_attachment_image_src( get_post_thumbnail_id( $Post->ID ), 'full' );

                                $coverageSlide .= '
                                <div>
                                    <div class="nine columns">
                                        <img src="'. $PostImageData[0] .'">
                                    </div>
                                    <div class="three columns">
                                        <h2>' . $Post->post_title . '</h2>
                                        <p>' . nl2br($PostMetaData['coverage_description'][0]) . '</p>
                                    </div>
                                </div>';
                            }
                        }
                        echo $coverageSlide;
                        ?>

                    </div>
                </div>
            </div>
        </section> <!-- Coverage Section End-->

    <?php
    }
}