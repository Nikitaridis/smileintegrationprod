<?php
/**
 * Template functions used for the site header.
 *
 * @package storefront-child
 */


if ( ! function_exists( 'smile_header_banner' ) ) {
    /**
     * Display smile header banner (for homepage)
     * @since  1.0.0
     */
    function smile_header_banner() {
    ?>

        <?php
            $args = array(
                'post_status'       => 'publish',
                'posts_per_page'    => 10,
                'post_type'         => 'smile_carousel',
                'meta_key'          => 'smile_platform_type',
                'meta_value'        => PageType::isPageType('business') ? 'business' : 'consumer',
                'orderby'           => 'menu_order',
                'order'             => 'ASC'
            );
            $Carousels = new WP_Query($args);
        ?>


        <div class="header-carousel">

            <?php
                $carouselItems = '';
                $counter=1;
                while ( $Carousels->have_posts() ) {

                    $Carousels->the_post();
                    // Carousel post object
                    $Post = get_post();
                    $PostMetaData = get_post_meta($Post->ID);

                    if( isset($PostMetaData['desktop-thumbnail-src'][0]) && !empty($PostMetaData['desktop-thumbnail-src'][0]) &&
                        isset($PostMetaData['tablet-thumbnail-src'][0]) && !empty($PostMetaData['tablet-thumbnail-src'][0]) &&
                        isset($PostMetaData['smartphone-thumbnail-src'][0]) && !empty($PostMetaData['smartphone-thumbnail-src'][0]) &&
                        isset($PostMetaData['headline'][0]) && !empty($PostMetaData['headline'][0]) &&
                        isset($PostMetaData['button-label'][0]) && !empty($PostMetaData['button-label'][0]) &&
                        isset($PostMetaData['button-link'][0]) && !empty($PostMetaData['button-link'][0]) &&
                        isset($PostMetaData['subtitle'][0]) && !empty($PostMetaData['subtitle'][0])
                    ){

                        $carouselItems .= '
                            <div class="header-banner slide-'. $counter .'" data-desktop-image="'. apply_filters('smile_full_image_url', $PostMetaData['desktop-thumbnail-src'][0]) .
                                '" data-tablet-image="'. apply_filters('smile_full_image_url', $PostMetaData['tablet-thumbnail-src'][0]) .
                                '" data-smartphone-image="'. apply_filters('smile_full_image_url', $PostMetaData['smartphone-thumbnail-src'][0]) .'">
                                <div class="hero">
                                    <h2 class="responsive-headline">
                                        '. $PostMetaData['headline'][0] .'
                                    </h2>
                                    <p>'. $PostMetaData['subtitle'][0] .'</p>
                        ';

                        $ExtraClass = '';
                        if(strpos($PostMetaData['button-link'][0], '#') !== false) {
                            $ExtraClass = 'smoothscroll';
                        }

                        $carouselItems .= '<a class="'. $ExtraClass .' button trigger" title="'. $PostMetaData['button-label'][0] .'" data-trigger-category="Homepage header" href="'. $PostMetaData['button-link'][0] .'">'. $PostMetaData['button-label'][0] .'</a>';

                        if(isset($PostMetaData['show-logos'][0]) && $PostMetaData['show-logos'][0] == 'Yes') {
                            $carouselItems .= '
                                    <ul class="logos">
                                        <li><img src="' . PAYMENT_LOGOS_URL .  '"></li>
                                    </ul>';
                        }

                        $carouselItems .= '
                                </div>
                            </div>
                        ';

                        $counter++;
                    }
                }
                wp_reset_query();
                echo $carouselItems;
            ?>

        </div>

    <?php
    }
}


if ( ! function_exists( 'smile_usp' ) ) {
    /**
     * Display USPs
     * @since  1.0.0
     */
    function smile_usp() {

    ?>
        <div id="usp">
            <div class="row usp-container">
                <ul id="desktop">
                    <li><a class="trigger" data-trigger-category="Homepage USP" title="Fastest in Nigeria"><i class="icon-rocket"></i>Fastest in Nigeria</a></li>
                    <li><a class="trigger" data-trigger-category="Homepage USP" title="Customer support"><i class="icon-bubbles"></i>Customer support</a></li>
                    <li><a class="trigger" data-trigger-category="Homepage USP" title="Manage your data"><i class="icon-bar-chart"></i>Manage your data</a></li>
                    <li><a class="trigger" data-trigger-category="Homepage USP" title="Secure payment options"><i class="icon-lock"></i>Secure payment options</a></li>
                </ul>
                <ul id="mobile">
                    <li><a class="trigger" data-trigger-category="Homepage USP" title="Fastest in Nigeria"><i class="icon-rocket"></i>Fastest</a></li>
                    <li><a class="trigger" data-trigger-category="Homepage USP" title="Customer support"><i class="icon-bubbles"></i>Support</a></li>
                    <li><a class="trigger" data-trigger-category="Homepage USP" title="Manage your data"><i class="icon-bar-chart"></i>Manage</a></li>
                    <li><a class="trigger" data-trigger-category="Homepage USP" title="Secure payment options"><i class="icon-lock"></i>Secure</a></li>
                </ul>
            </div>
        </div> <!-- Usp End -->
    <?php
    }
}


if ( ! function_exists( 'smile_site_navigation' ) ) {
    /**
     * Display main navigation + mobile navigation
     * @since  1.0.0
     */
    function smile_site_navigation() {

    ?>
        <section id="site-nav">
            <div class="row">
                <div class="twelve columns">
                    <h1>
                        <a class="trigger" data-trigger-category="Mainnav" title="Smile logo" href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                            <img src="<?php echo get_stylesheet_directory_uri() . '/images/smile-logo.svg'; ?>" alt="Smile logo">
                        </a>
                    </h1>

                    <!-- Site mainmenu -->
                    <nav class="main-nav" role="navigation">

                        <?php echo smile_header_cart(); ?>

                        <?php wp_nav_menu( array(
                            'theme_location' => 'primary',
                            'walker' => new Walker_Nav_Menu_Smile
                            )
                        ); ?>
                    </nav>

                    <!-- Mobile menu -->
                    <button class="mobile-btn"><i class="icon-bars"></i></button>
                    <nav class="mobile-nav">
                        <button class="close-menu"><i class="icon-close"></i></button>
                        <?php wp_nav_menu( array(
                                'theme_location' => 'handheld',
                                'container' => '',
                                'walker' => new Walker_Nav_Menu_Smile
                            )
                        ); ?>
                    </nav><!-- end mobile-nav -->

                </div><!-- end columns -->
            </div><!-- end row -->
        </section> <!-- end #site-nav -->
    <?php
    }
}