<?php
/**
 * Template functions used for the site footer.
 *
 * @package storefront-child
 */


if ( ! function_exists( 'smile_footer_sitemap' ) ) {
    /**
     * Display the footer sitemap
     * @since  1.0.0
     */
    function smile_footer_sitemap() {
        $menu_items_tmp = wp_get_nav_menu_items('Footer');

        $menu = array();
        $counter = 0;
        $counter_childs = 0;
        foreach ((array)$menu_items_tmp as $key => $menu_item_tmp) {
            if ($menu_item_tmp->menu_item_parent > 0) {
                foreach ($menu as $menu_key => $menu_item) {
                    if ($menu_item['id'] == $menu_item_tmp->menu_item_parent) {

                        $menu[$menu_key]['childs'][$counter_childs]['id'] = $menu_item_tmp->ID;
                        $menu[$menu_key]['childs'][$counter_childs]['title'] = $menu_item_tmp->title;
                        $menu[$menu_key]['childs'][$counter_childs]['url'] = $menu_item_tmp->url;
                        $menu[$menu_key]['childs'][$counter_childs]['object_id'] = $menu_item_tmp->object_id;
                        $counter_childs++;
                        break;
                    }
                }
            } else {
                $menu[$counter]['id'] = $menu_item_tmp->ID;
                $menu[$counter]['title'] = $menu_item_tmp->title;
                $menu[$counter]['url'] = $menu_item_tmp->url;
                $menu[$counter]['object_id'] = $menu_item_tmp->object_id;
                $menu[$counter]['childs'] = array();
            }
            $counter++;
        }
        ?>
        <div class="sitemap">
            <div class="row">
        <?php
        foreach ($menu as $menu_key => $menu_item) {
            ?>
            <div class="four columns">

                <h2><?php echo $menu_item['title']; ?></h2>
                <ul>
                    <?php
                    if (count($menu_item['childs'])) {
                        foreach ($menu_item['childs'] as $menu_child_key => $child) {
                            ?>
                            <li>
                                <a href="<?php echo $child['url']; ?>" title="<?php echo $child['title']; ?>">
                                    <?php echo $child['title']; ?>
                                </a>
                            </li>
                            <?php
                        }
                    }
                    ?>

                </ul>
            </div>
            <?php
        }
    ?>
                <div class="four columns">
                    <h2>Stay in touch</h2>
                    <ul class="socials">
                        <?php if(SOCIAL_FACEBOOK_URL != null):?><li class="facebook"><a href="<?php echo SOCIAL_FACEBOOK_URL;?>" target="_blank"><i class="icon-facebook"></i></a></li><?php endif;?>
                        <?php if(SOCIAL_TWITTER_URL != null):?><li class="twitter"><a href="<?php echo SOCIAL_TWITTER_URL;?>" target="_blank"><i class="icon-twitter"></i></a></li><?php endif;?>
                        <?php if(SOCIAL_INSTAGRAM_URL != null):?><li class="instagram"><a href="<?php echo SOCIAL_INSTAGRAM_URL;?>" target="_blank"><i class="icon-instagram"></i></a></li><?php endif;?>
                        <?php if(SOCIAL_YOUTUBE_URL != null):?><li class="youtube"><a href="<?php echo SOCIAL_YOUTUBE_URL;?>" target="_blank"><i class="icon-youtube-play"></i></a></li><?php endif;?>
                    </ul>
                </div>
            </div>
        </div>

    <?php
    }
}


if ( ! function_exists( 'smile_footer_copyright' ) ) {
    /**
     * Display the footer copyright
     * @since  1.0.0
     */
    function smile_footer_copyright() {
    ?>

        <div class="copyright">
            <div class="row">
                <p>Company Registration <?php echo REGISTRATION_NUMBER; ?>.<br>Copyright © <?php echo date('Y'); ?> Smile Communications <?php echo COUNTRY;?> Ltd.  All Rights Reserved.</p>
                <ul>
                    <!-- <li><a href="http://smile.com.ng/home/sitemap/" title="Sitemap">Sitemap</a></li> -->
<!--                    <li><a href="--><?php //echo PageType::getPageUrl('terms-and-conditions'); ?><!--" title="Terms & conditions">Terms &amp; conditions</a></li>-->
                    <li><a href="<?php echo PageType::getPageUrl('privacy-policy'); ?>" title="Privacy Policy">Privacy Policy</a></li>
                </ul>
            </div>
        </div>

    <?php
    }
}




