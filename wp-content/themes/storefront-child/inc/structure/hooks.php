<?php

/**
 * General
 * @see  smile_setup()
 * @see  theme_enqueue_styles()
 * @see  theme_enqueue_scripts()
 * @see  remove_styles_from_parent_theme()
 */
add_action( 'after_setup_theme',			'smile_setup' );
add_action( 'wp_enqueue_scripts',           'theme_enqueue_styles');
add_action( 'wp_enqueue_scripts',           'theme_enqueue_scripts');
add_action( 'admin_enqueue_scripts',        'theme_enqueue_admin_styles');
add_action( 'admin_enqueue_scripts',        'theme_enqueue_admin_scripts');
add_action( 'after_setup_theme',            'remove_styles_from_parent_theme', 0 ); // removes all kind of inline css
add_action( 'after_setup_theme',            'remove_admin_bar', 0 ); // removes the admin bar for specific users


/**
 * Homepage
 * @see  smile_header_banner()
 * @see  smile_usp()
 */
add_action( 'smile_homepage',       'smile_header_banner',		        10 );
//add_action( 'smile_homepage',       'smile_usp',	                    20 );


/**
 * Header
 * @see  smile_site_navigation()
 */
add_action( 'smile_header',         'smile_site_navigation', 	            0 );


/**
 * Footer
 * @see  smile_footer_sitemap()
 * @see  smile_footer_copyright()
 */
add_action( 'smile_footer',             'smile_footer_sitemap', 		        0 );
add_action( 'smile_footer',             'smile_footer_copyright', 		        0 );


/**
 * Products
 * @see  smile_products_block()
 * @see smile_show_order_meta()
 */
add_action( 'smile_products_block',             'smile_products_block', 		        0 );
add_action( 'woocommerce_admin_order_data_after_billing_address', 'smile_show_order_meta', 0 );


/**
 * Services
 * @see  smile_services_block()
 */
add_action( 'smile_services_block',     'smile_services_block', 		        0 );


/**
 * Contact
 * @see  smile_contact_form()
 */
add_action( 'smile_contact_page',       'smile_contact_form', 		                0 );


/**
 * Shop
 * @see  smile_shop_search()
 * @see  smile_shop_info()
 * @see  smile_shop_service()
 */

add_action( 'smile_shop_page',          'smile_shop_info', 		                0 );
//add_action( 'smile_shop_page',          'smile_shop_service',                   10 );
add_action( 'smile_shop_page',          'smile_shop_search',                    20 );


/**
 * Coverage
 * @see  smile_coverage_block()
 * @see  smile_coverage_maps()
 */

// @TODO hier nog een betere manier voor verzinnen
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if (is_plugin_active('smile-coverage/smile-coverage-list.php') ) {
    add_action( 'smile_coverage_block',     'smile_coverage_block', 		        0 );
}

add_action( 'google_maps_js',               'google_maps_js',                       0 );
add_action( 'smile_coverage_page',          'smile_coverage_maps',                  0 );
add_action( 'smile_coverage_map_html',      'smile_coverage_map_html',              0 );



/**
 * Webcare
 * @see  smile_webcare_menu()
 * @see  smile_webcare_faq()
 */
add_action( 'smile_webcare_menu',          'smile_webcare_menu', 		                0 );
add_action( 'smile_webcare_faq',          'smile_webcare_faq', 		                0 );



/**
 * Analytics codes
 * @see  smile_google_analytics()
 * @see  smile_google_adwords()
 * @see  smile_vwo()
 * @see  smile_hotjar()
 */
add_action( 'smile_google_analytics',               'smile_google_analytics', 		        0 );
add_action( 'smile_google_analytics_ecommerce',     'smile_google_analytics_ecommerce',     0 );
add_action( 'smile_google_analytics_conversion_tracking',     'smile_google_analytics_conversion_tracking',     0 );
add_action( 'smile_google_adwords',                 'smile_google_adwords',                 0 );
add_action( 'smile_vwo_analytics',                  'smile_vwo', 		                    0 );
add_action( 'smile_hotjar_analytics',               'smile_hotjar', 		                0 );