<?php
/**
 * Template functions used for the displaying services
 *
 * @package storefront-child
 */


if ( ! function_exists( 'smile_services_block' ) ) {
    /**
     * Display the footer copyright
     * @since  1.0.0
     */
    function smile_services_block() {
    ?>

        <section id="webcare-home">
            <div class="row">
                <div class="twelve columns section-title section-text-center">
                    <div class="helpdesk"></div>
                    <h1>Please contact us</h1>
                </div>

                <ul>
                    <li>
                        <a class="trigger" data-trigger-category="Homepage Webcare" title="Contact us" href="<?php echo PageType::getPageUrl('help'); ?>">
                            <i class="icon-bubbles"></i>
                            <div class="service">
                                <h2>Contact us</h2>
                                <p>24/7 available</p>
                            </div>
                        </a>
                    </li>

                    <li>
                        <a class="trigger" data-trigger-category="Homepage Webcare" title="Come to our shop" href="<?php echo PageType::getPageUrl('shops'); ?>">
                            <i class="icon-open"></i>
                            <div class="service">
                                <h2>Come to our shop</h2>
                                <p>We are open</p>
                            </div>
                        </a>
                    </li>
                    <?php if (SOCIAL_TWITTER_URL != null) :?>
                    <li>
                        <a class="trigger" data-trigger-category="Homepage Webcare" title="Twitter" target="_blank" href="<?php echo SOCIAL_TWITTER_URL;?>">
                            <i class="icon-twitter"></i>
                            <div class="service">
                                <h2>Twitter</h2>
                                <p>Tweet your question</p>
                            </div>
                        </a>
                    </li>
                    <?php endif;?>
                    <?php if (SOCIAL_FACEBOOK_URL != null) :?>
                    <li>
                        <a class="trigger" data-trigger-category="Homepage Webcare" title="Facebook" target="_blank" href="<?php echo SOCIAL_FACEBOOK_URL;?>">
                            <i class="icon-facebook"></i>
                            <div class="service">
                                <h2>Facebook</h2>
                                <p>Post your question</p>
                            </div>
                        </a>
                    </li>
                    <?php endif; ?>
                </ul>


            </div>
        </section> <!-- Webcare Section End-->

    <?php
    }
}