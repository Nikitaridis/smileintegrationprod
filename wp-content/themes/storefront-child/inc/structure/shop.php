<?php
/**
 * Template functions used for the displaying shop info
 *
 * @package storefront-child
 */


if ( ! function_exists( 'smile_shop_info' ) ) {
    /**
     * Display shop info
     * @since  1.0.0
     */
    function smile_shop_info() {
        $cities_utils = new Smile_Cities_Utils();
        $cities_query = $cities_utils->getCitiesOnMapQuery();
        ?>

        <section id="shops-info" class="fill-screen">
            <?php
                // Page post object
                $Page = get_post();
                $PageMetaData = get_post_meta($Page->ID);
            ?>

            <div class="page-container row">

                <div class="twelve columns section-title section-text-center">
                    <h1><?php echo get_post()->post_title; ?></h1>
                    <?php echo get_the_subtitle(); ?>
                </div>

                <div class="page-image-container six columns">
                    <?php
                    if($cities_query->have_posts()) {
                        while($cities_query->have_posts()) :
                            $cities_query->the_post();
                            $pan_title = get_post_meta(get_the_ID(), 'smile_cities_pan_title', true);
                            $map_title = get_post_meta(get_the_ID(), 'smile_cities_map_title', true);
                            if(empty($map_title)) {
                                $map_title = get_the_title();
                            }
                            ?>
                            <div data-pan-title="<?php echo $pan_title; ?>"
                                 class="city <?php echo get_post()->post_name; ?>">
                                <div class="city-title"><?php echo $map_title; ?></div>
                                <img class="city-dot" src="<?php echo get_stylesheet_directory_uri(); ?>/images/map/city-dot.png"/>
                            </div>
                        <?php endWhile;
                        wp_reset_postdata();
                    } ?>

<!--                    <a class="button abuja"></a>-->
<!--                    <a class="button ibadan"></a>-->
<!--                    <a class="button lagos"></a>-->
<!--                    <a class="button benin-city"></a>-->
<!--                    <a class="button port-harcourt"></a>-->
<!--                    <a class="button kaduna"></a>-->
<!--                    <a class="button onitsha-asaba"></a>-->
                    <?php the_post_thumbnail( 'full' ); ?>
                </div>

                <div class="six columns content">
                    <p><?php echo $Page->post_content; ?></p>
                    <p class="sub-text">
                        <a class="button find-shop" href="#shops"><span class="icon-search"></span> Find outlets</a>
                    </p>
                </div>

            </div>

        </section> <!-- Shops info Section End -->

    <?php
    }
}


if ( ! function_exists( 'smile_shop_service' ) ) {
    /**
     * Display shop service
     * @since  1.0.0
     */
    function smile_shop_service() {
        ?>

        <?php
            // Page post object
            $Page = get_post();
            $PageMetaData = get_post_meta($Page->ID);
        ?>

        <?php if(isset($PageMetaData['_easy_image_gallery'][0]) && !empty($PageMetaData['_easy_image_gallery'][0])) : ?>
            <section id="shops-service">
                <div class="row">
                    <?php
                    $ImageIDs = explode(',', $PageMetaData['_easy_image_gallery'][0]);
                    $ImageHtml = '';
                    foreach($ImageIDs as $key => $ImageID) {

                        $UploadPath = wp_upload_dir();
                        $Image = wp_get_attachment_metadata($ImageID);
                        $ImageMetaData = get_post( $ImageID );

                        $ImageHtml .= '<div class="three columns">';
                        $ImageHtml .= '<img src="' . $UploadPath['baseurl'] . '/' . $Image['file'] . '" alt="'. $ImageMetaData->post_title .'">';
                        $ImageHtml .= '<p>' . $ImageMetaData->post_title . '</p>';
                        $ImageHtml .= '</div>';
                    }

                    if(count($ImageIDs) == 3) {
                        $ImageHtml .= '<div class="three columns"></div>';
                    }
                    elseif(count($ImageIDs) == 2) {
                        $ImageHtml .= '<div class="six columns"></div>';
                    }
                    echo $ImageHtml;
                    ?>
                </div>
            </section> <!-- Shops service Section End -->
        <?php endif; ?>

    <?php
    }
}


if ( ! function_exists( 'smile_shop_search' ) ) {
    /**
     * Display shop search
     * @since  1.0.0
     */
    function smile_shop_search() {
        ?>

        <?php
            // Page post object
            $Page = get_post();
            $PageMetaData = get_post_meta($Page->ID);
        ?>

        <section id="shops">
<!--            <div class="title">-->
<!--                <div class="row">-->
<!--                    <div class="twelve columns section-title section-text-center">-->
<!--                        <h1>Find your Smile shops</h1>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->

            <div class="row form-container">
                <div class="twelve columns">
                    <a id="user-geo-location" href=""><span class="icon-map-marker"></span>Use my location</a>
                    <p class="show-search-form">or <a id="show-search-form" href="" title="show search form">search</a></p>
                </div>
                <div class="six columns">
                    <form class="find-shop-form trigger" data-trigger-category="Shops" method="GET">
                        <input type="search" placeholder="Enter your location" name="search" id="search">
                        <button class="green" type="submit">Search</button>
                    </form>
                </div>
                <div class="six columns">
                    <p class="view-all-stores">or <a class="trigger" data-trigger-category="Shops" id="view-all-stores" href="" title="View all stores">view all stores</a></p>
                </div>
            </div>

            <div class="shops-container">
                <?php
                $args = array(
                    'post_status'       => 'publish',
                    'posts_per_page'    => 10,
                    'post_type'         => 'smile_shops',
                    'orderby'           => 'menu_order',
                    'order'             => 'ASC'
                );
                $Shops = new WP_Query($args);

                while ( $Shops->have_posts() ) {

                    $Shops->the_post();
                    // Shop post object
                    $Shop = get_post();
                    $ShopMetaData = get_post_meta($Shop->ID);

                    // Html of shop rows
                    //set_query_var( 'Shop', $Shop );
                    //get_template_part( 'inc/template/shop/display', 'row' );
                }

                // Add a row to unfold more shops
                //get_template_part('inc/template/shop/display-unfold', 'shops');
                ?>
            </div>

        </section>

        <div id="fullscreen-loader">
            <div class="spinner"></div>
            <div class="message">Loading shops</div>
        </div>


        <?php wp_reset_postdata(); ?>

    <?php
    }
}
