<?php
/**
 * Smile - Child theme
 *
 * storefront engine room
 *
 * @package storefront
 */

/**
 * Setup.
 * Enqueue styles, register widget regions, etc.
 */
require get_stylesheet_directory() . '/inc/functions/setup.php';
require get_stylesheet_directory() . '/inc/functions/filters.php';
require get_stylesheet_directory() . '/inc/functions/woocommerce/filters.php';
require get_stylesheet_directory() . '/inc/functions/woocommerce/admin/product/functions.php';
require get_stylesheet_directory() . '/inc/functions/settings.php';


/**
 * Structure.
 * Template functions used throughout the theme.
 */
require get_stylesheet_directory() . '/inc/structure/hooks.php';
require get_stylesheet_directory() . '/inc/structure/header.php';
require get_stylesheet_directory() . '/inc/structure/footer.php';
require get_stylesheet_directory() . '/inc/structure/products.php';
require get_stylesheet_directory() . '/inc/structure/services.php';
require get_stylesheet_directory() . '/inc/structure/analytics.php';
require get_stylesheet_directory() . '/inc/structure/shop.php';
require get_stylesheet_directory() . '/inc/structure/coverage.php';
require get_stylesheet_directory() . '/inc/structure/webcare.php';


/**
 * Woocommerce
 * Load Woocommerce files
 */
require get_stylesheet_directory() . '/woocommerce/cart/cart-header.php';



/**
 * Class extensions
 */
require get_stylesheet_directory() . '/inc/extensions/Walker_Nav_Menu_Smile.php';
require get_stylesheet_directory() . '/inc/extensions/CheckoutForm.php';
require get_stylesheet_directory() . '/inc/extensions/Category.php';
require get_stylesheet_directory() . '/inc/extensions/Product.php';
require get_stylesheet_directory() . '/inc/extensions/Shop.php';
require get_stylesheet_directory() . '/inc/extensions/Device.php';
require get_stylesheet_directory() . '/inc/extensions/Recharge.php';
require get_stylesheet_directory() . '/inc/extensions/Geocode.php';

/**
 * Classes
 */
require get_stylesheet_directory() . '/inc/classes/utils.php';





