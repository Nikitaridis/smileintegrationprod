<?php
/*
 * Plugin Name: Smile Communication API
 * Description: Talk to the back end!
 * Plugin URI: http://www.zooma.nl
 * Author: Zooma
 * Author URI: http://www.zooma.nl
 * Version: 1.0
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

require_once( plugin_dir_path( __FILE__ ) . 'classes/class-smile-communication-api.php' );

/**
 * Begins execution of the plugin.
 */
function run() {

   new Smile_Communication_API();
}
run();