<?php
/**
 * Created by PhpStorm.
 * User: Zooma
 * Date: 21/08/15
 * Time: 10:28
 */

class Smile_Customer{

    function __construct(){
        //Constructor
        $this->init_hooks();
         
        $this->payment_code         = SALE_PARAM_PAYMENTCODE;
    }

    /**
     * Add hooks for our filters and actions
     */
    function init_hooks(){
        add_filter('woocommerce_email_customer_details_fields', array( $this,'add_customer_id_to_new_order_email'),0,3);
        add_action('woocommerce_admin_order_data_after_order_details', array($this, 'add_customer_id_to_order_details_overview'));
    }

    /**
     * Modify the admin-new-order.php template filter so it adds customerID to the email output.
     * @param WC_Order $order
     */
    function add_customer_id_to_new_order_email( $fields, $sent_to_admin, $order){

        $fields['customer_id'] = array(
            'label' => __( 'Customer ID', 'woocommerce' ),
            'value' => wptexturize( get_post_meta( $order->get_id(), 'customer_id',true) )
        );
        return $fields;
    }

    /**
     * Show an extra field at the admin order overview that shows the customerID
     * @param WC_Order $order
     */
    function add_customer_id_to_order_details_overview($order){  ?>
        <!--//Add the customer_id field to the already existing form-->

        <div class="order_data_column">
        <h4><?php _e( 'Extra Details' ); ?></h4>
        <?php echo '<p><strong>' . __( 'CustomerID' ) . ':</strong></br>' . get_post_meta( $order->get_id(), 'customer_id', true ) . '</p>'; ?>
        </div><?php
    }

    /**
     * Get customer by email or register it by email when it does not exist.
     * Return value = customer data
     */
    function get_customer_by_email($formDetails){
		$token = $this->get_token();
        $uri = SMILE_API_URL . 'customers/'.$formDetails['billing_email'].'?by=email';
        $headers = array('X-token' => $token, 'Content-Type' => 'application/json');
		
        $response = wp_remote_get( $uri, array(
            'timeout'   => 30,
            'sslverify' => false,
            'headers'	=> $headers,
        ) );

        $responseArray = json_decode( wp_remote_retrieve_body( $response ));

        write_log(print_r('ResponseCustomer =', true));
        write_log(print_r($responseArray, true));

        //TODO check if responseArray is empty or not an object

        //Check if the customer exists, if the customer does not exist yet, add it to the back-end
        if(property_exists($responseArray, 'errorCode')){
            if($responseArray->errorCode == 'SCAD-0007'){
                write_log('Customer does not exist yet!');
                //Add customer, it does not exist yet.
                $responseArray = apply_filters('smile_communication_api_add_customer',$formDetails);
                write_log("New user!!");
                write_log($responseArray);
                return $responseArray;
            }
        }else{
            //Return customer data
            return $responseArray;
        }
    }

    /**
     * Add customer by email
     * Return value = customer data
     */
    function add_customer_by_email($formDetails){
		$token = $this->get_token();
		
        //Strip all forward slashes from the date of brith, else the data will be invalid
        $dateOfBirth = $formDetails['billing_date_of_birth']; // YYYYMMDD
		
        write_log($formDetails);
		$differentShippingAddress = false;
		if(isset($formDetails['ship_to_different_address']) && $formDetails['ship_to_different_address']){
			$differentShippingAddress = true;
		}
		
		// physical/billing address
		$addresses = array(array(
                        "line1" =>$formDetails['billing_address_1'],
                        "line2"=> "",
                        "zone"=> $formDetails['billing_zone'],
                        "town"=> $formDetails['billing_district'],
                        "state"=> $formDetails['billing_state'],
                        "country"=> COUNTRY,
                        "code"=> "",
                        "type"=> "Postal Address",
                        "postalMatchesPhysical"=> !$differentShippingAddress));
						
		// shipping address
		if($differentShippingAddress){
			$addresses[] = array(
                        "line1" =>$formDetails['shipping_address_1'],
                        "line2"=> "",
                        "zone"=> $formDetails['shipping_zone'],
                        "town"=> $formDetails['shipping_district'],
                        "state"=> $formDetails['shipping_state'],
                        "country"=> COUNTRY,
                        "code"=> "",
                        "type"=> "Shipping Address",
                        "postalMatchesPhysical"=> !$differentShippingAddress);
		}
						
		
        $customerArray = array(
            'customer' => array(
                "language" => "en",
                "firstName" => $formDetails['billing_first_name'],
                "middleName" => $formDetails['billing_middle_name'],
                "lastName" => $formDetails['billing_last_name'],
                "SSOIdentity" => $formDetails['billing_email'],
                "identityNumber" => $formDetails['billing_identification_number'],
                "identityNumberType" => $formDetails['billing_identification_type'],
                "dateOfBirth"=> $dateOfBirth,
                "gender"=> $formDetails['billing_gender'],
                "classification"=> "customer",
                "emailAddress"=> $formDetails['billing_email'],
                "alternativeContact1"=> $formDetails['billing_phone'],
                "alternativeContact2"=> "",
                "mothersMaidenName"=> $formDetails['billing_mother_maiden_name'],
                "nationality"=> $formDetails['billing_country'],
                "passportExpiryDate"=> "", //TODO: temp solution
                'addresses' => $addresses
            )
        );

        //Set post parameters
        $uri = SMILE_API_URL . 'customers/';
        $headers = array('X-token' => $token, 'Content-Type' => 'application/json');
        $body = json_encode($customerArray);

        write_log($body);

        //Create POST
        $response = wp_remote_post( $uri, array(
                'method' => 'POST',
                'timeout' => 30,
                'headers' => $headers,
                'body' => $body
            )
        );

        //Decode and return response value
        $responseArray = json_decode(wp_remote_retrieve_body( $response ));
        return $responseArray;
    }

    /**
     * Get customer by email or register it by email when it does not exist.
     * Return value = customer data
     */
    function get_customer_by_email_with_services($email){
		error_log('in get_customer_by_email_with_services:'.$email);
		$token = $this->get_token();

        $uri = SMILE_API_URL . 'customers/' . $email . '?by=email&verbosity=CUSTOMER_ADDRESS_PRODUCTS_SERVICES';
        $headers = array(
            'X-token' => $token,
            'Content-Type' => 'application/json'
        );

        $response = wp_remote_get( $uri, array(
            'timeout'   => 30,
            'sslverify' => false,
            'headers'	=> $headers,
        ) );

        $responseArray = json_decode( wp_remote_retrieve_body( $response ));
		
		error_log('get_customer_by_email_with_services response :');
		write_log($responseArray);
		
		
        if(!is_object($responseArray)) {
            $error = new stdClass();
            $error->error = 'Something went wrong';
			write_log($responseArray);
            return $error;
        }

        //Check if the customer exists, if the customer does not exist yet, add it to the back-end
        if(property_exists($responseArray, 'errorCode')){
            if($responseArray->errorCode == 'SCAD-0007'){
                $error = new stdClass();
                $error->error = 'This email does not exist';
				write_log($responseArray);
                return $error;
            }
            $error = new stdClass();
            $error->error = 'Something went wrong';
			write_log($responseArray);
            return $error;
        }else{
            //Return customer data
            return $responseArray;
        }
    }

    /**
     * Get customer by email or register it by email when it does not exist.
     * Return value = customer data
     */
    function get_customer_by_msisdn_with_services($msisdn){
        error_log('in get_customer_by_email_with_msisdn:'.$msisdn);
        $token = $this->get_token();

        $uri = SMILE_API_URL . 'customers/' . $msisdn . '?by=smilevoice&verbosity=CUSTOMER_ADDRESS_PRODUCTS_SERVICES';
        $headers = array(
            'X-token' => $token,
            'Content-Type' => 'application/json'
        );

        $response = wp_remote_get( $uri, array(
            'timeout'   => 30,
            'sslverify' => false,
            'headers'	=> $headers,
        ) );

        $responseArray = json_decode( wp_remote_retrieve_body( $response ));

        error_log('get_customer_by_msisdn_with_services response :');
        write_log($responseArray);


        if(!is_object($responseArray)) {
            $error = new stdClass();
            $error->error = 'Something went wrong';
            write_log($responseArray);
            return $error;
        }

        //Check if the customer exists, if the customer does not exist yet, add it to the back-end
        if(property_exists($responseArray, 'errorCode')){
            if($responseArray->errorCode == 'SCAD-0007'){
                $error = new stdClass();
                $error->error = 'This msisdn does not exist';
                write_log($responseArray);
                return $error;
            }
            $error = new stdClass();
            $error->error = 'Something went wrong';
            write_log($responseArray);
            return $error;
        }else{
            //Return customer data
            return $responseArray;
        }
    }




    /**
     * Get token, so that we can start making requests with it.
     */
    function get_token( ){

        if(ENV == 'DEVELOPMENT'){
            return "openaccess";
        }else {
            error_log('in get_token');
            $token = WC()->session->get('API_token');
            error_log($token);
            if ($token == null) {
                // try to get token from API
                $username = SMILE_API_USERNAME;
                $password = SMILE_API_PASSWORD;
                $token_url = SMILE_API_URL . 'tokens?' . 'username=' . $username . '&password=' . $password . '&srav=1.0';
                $response = wp_remote_get($token_url,array(
		            'timeout'   => 30,
		            'sslverify' => false)
				);
                write_log($response);
                if (is_wp_error($response))
                    throw new Exception(__('We are currently experiencing problems trying to connect to the token API. Sorry for the inconvenience.', 'wcspg-smile-payment'));

                if (empty($response['body']))
                    throw new Exception(__('API\'s Response was empty.', 'wcspg-smile-payment'));

                // Retrieve the body's resopnse if no errors found
                $response_body = wp_remote_retrieve_body($response);
                $response_array = json_decode($response_body, true);
                error_log(print_r($response_array, true));
                if (isset($response_array['token']) && isset($response_array['token']['tokenUUID'])) {
                    return $response_array['token']['tokenUUID'];
                }
            }
        }
		return null;
    }

}
