<?php
/**
 * Created by PhpStorm.
 * User: Zooma
 * Date: 26/08/15
 * Time: 15:41
 */

class Smile_Payment{

	const UNKNOWN = 'UNKNOWN';

    private $environment_url;
    private $environment;
    private $payment_code;
	private $channel;
	private $warehouseId;
	private $creditAccountNumber;
	private $salesPersonAccountId;
	
    function __construct(){
		$this->channel                  = SALE_PARAM_CHANNEL;
		$this->warehouseId              = SALE_PARAM_WAREHOUSEID;
        $this->creditAccountNumber      = SALE_PARAM_CREDITACCOUNTNUMBER;
	    $this->salesPersonAccountId     = SALE_PARAM_SALESPERSONACCOUNTID;
        $this->salesPersonCustomerId    = SALE_PARAM_SALESPERSONCUSTOERID;
         
        $this->payment_code             = SALE_PARAM_PAYMENTCODE;

        $this->environment_url          = SALE_ENDPOINT;

        //Constructor
        $this->init_hooks();
    }

    /**
     * Add hooks for our filters and actions
     */
    function init_hooks(){

    }

    /**
     * Get payment gateway.
     */
     
     /* Example payload
	  *  
	  *{
	    "sale": {
	        "channel": "41WDIA01",
	        "warehouseId": "",
	        "creditAccountNumber": "DIA003",
	        "tenderedCurrency": "NGN",
	        "paymentTransactionData": "",
	        "extraInfo": "",
	        "recipientCustomerId": 9995,
	        "salesPersonCustomerId": 1,
	        "recipientOrganisationId": 0,
	        "recipientAccountId": 1402000567,
	        "salesPersonAccountId": 1795541255,
	        "saleTotalCentsIncl": 100000,
	        "amountTenderedCents": 0,
	        "saleLines": [
	            {
	                "LineNumber": 1,
	                "InventoryItem": {
	                    "SerialNumber": "AIRTIME",
	                    "ItemNumber": "AIR1004"
	                },
	                "Quantity": 1000,
	                "SubSaleLines": [
	                    {
	                        "InventoryItem": {
	                            "SerialNumber": "",
	                            "ItemNumber": "ROU7006"
	                        }
	                    },
	                    {
	                        "InventoryItem": {
	                            "SerialNumber": "",
	                            "ItemNumber": "SIM8001"
	                        }
	                    },
	                    {
	                        "InventoryItem": {
	                            "SerialNumber": "BUNDLE",
	                            "ItemNumber": "BUNK1010"
	                        }
	                    }
	                ]
	            }
	        ],
	        "paymentMethod": "Payment Gateway",
	        "paymentGatewayCode": "Diamond",
	        "landingURL": "http://41.160.12.235/scp/PaymentGateway.action?processBankTransaction",
	        "purchaseOrderData": "",
	        "promotionCode": ""
	    }
	} */
	
    function get_payment_gateway(WC_Order $customer_order, $token){
        $transactionAmount = $customer_order->order_total * 100;

		$is_cashpayment = false;
		$paymentMethod = get_post_meta( $customer_order->get_id(), '_payment_method', true );
		if($paymentMethod == 'cod' || $paymentMethod == 'cap'){
			$is_cashpayment = true;
		}
		
		$is_recharge = false;

		$temp_is_recharge = get_post_meta($customer_order->get_id(), '_smile_is_recharge', true);
		if(!empty($temp_is_recharge) && $temp_is_recharge == 1) {
			$is_recharge = true;
		}
		
		$shipping = $customer_order->get_shipping_method();
		$shipping_option = 'Shop Pickup'; // default?
		$shop_name = '';
		if(strpos($shipping, 'Local Delivery') === 0){
			$shipping_option = 'Delivery Service';
			$billingState = get_post_meta($customer_order->get_id(), '_billing_district', true);
			$warehouseId = 'AUTO-' . $billingState;
		}
		elseif(strpos($shipping, 'Local Pickup') === 0){
			$shipping_option = 'Shop Pickup';
			$shop_name = get_post_meta($customer_order->get_id(), 'Pickup location', true);
			$warehouseId = 'AUTO-' . $shop_name;
		}
		
		if($is_recharge) {
			$warehouseId = $this->warehouseId;
		} 
		
		
//		if($customer_order->get_item_count() == 1) {
//			$temp_order_product = $customer_order->get_items();
//			$last_item = array_pop($temp_order_product);
//			$temp_product = new WC_Product($last_item['product_id']);
//			if (!empty($temp_product->get_attribute('data-bundle-title'))) {
//				$is_recharge = true;
//			}
//		}

		$recipientCustomerId = get_post_meta($customer_order->get_id(), 'smile_order_customer_id', true);

		$recipientAccountID = get_post_meta($customer_order->get_id(), 'smile_recharge_account_id', true);
		
		$recipientOrganisationId = get_post_meta($customer_order->get_id(), 'smile_recharge_recipient_organisation_id', true ); // default = 0

		$salesPersonAccountId = $this->salesPersonAccountId; // accountId Smile at Diamond bank
		$salesPersonCustomerId = $this->salesPersonCustomerId;
		
		$channel = $this->channel; // payment channel Diamond bank
		$creditAccountNumber = $this->creditAccountNumber; // accountNumber at Diamond Bank

		$tenderedCurrency = SALE_PARAM_COUNTRY_CODE;
	
		$items = $customer_order->get_items();
		$saleLines = array();
		foreach($items as $order_item_id => $item){
			write_log($item);
			
			$productId = $item['product_id'];
			$quantity = $item['qty'];
			
			$productSerialNumber = ""; // only for subsales
			$productItemNumber = wc_get_product($productId)->get_sku();

			$subSaleLines = $this->get_bundled_products($productId);

			$saleLinesTempArray = array(
				'lineNumber' => count($saleLines)+1,
				'inventoryItem' => array(
					'serialNumber' => $productSerialNumber,
					'itemNumber' => $productItemNumber
				),
				'quantity' => intval($quantity),
				'subSaleLines' => $subSaleLines
			);

			if($is_recharge) {
				$productInstanceId = get_post_meta($customer_order->get_id(), 'smile_recharge_product_instance_id', true);
				$saleLinesTempArray['ProvisioningData'] = 'ToAccountId=' . $recipientAccountID . "\nProductInstanceId=" . $productInstanceId;
			}
			
			$saleLines[] = $saleLinesTempArray;
		}
		

        $payload = array(
        	"sale" => array(
	        	"channel" => $channel,
		        "warehouseId" => $warehouseId,
		        "creditAccountNumber" => $creditAccountNumber,
		        "tenderedCurrency" => $tenderedCurrency,
		        "paymentTransactionData" => "",
		        "extraInfo" => "",
		        "recipientCustomerId" => intval($recipientCustomerId),
		        "salesPersonCustomerId" => intval($salesPersonCustomerId),
		        "recipientOrganisationId" => intval($recipientOrganisationId),
		        "recipientAccountId" => floatval($recipientAccountID),
		        "salesPersonAccountId" => floatval($salesPersonAccountId),
		        "saleTotalCentsIncl" => $transactionAmount,
		        "amountTenderedCents" => 0,
		        "saleLines" => $saleLines,
				"paymentMethod" => ($is_cashpayment) ? $shipping_option : "Payment Gateway",
		        "paymentGatewayCode" => ($is_cashpayment) ? "" : $this->payment_code, // Diamond
		        "callbackURL" => ($is_cashpayment) ? "" : add_query_arg( array('wc-api'=>'wcspg_smile_payment','key'=>$customer_order->order_key), home_url( '/' ) ),
		        "landingURL" => ($is_cashpayment) ? "" : $customer_order->get_checkout_payment_url(true) . '&checkPayment=true',
		        "purchaseOrderData" => "",
		        "promotionCode" => ""
			)
	    );

		write_log("payload payment");
		write_log($payload);
		write_log(wp_json_encode($payload));
        write_log('token = ' . $token);
		
        $response = wp_remote_post( $this->environment_url, array(
            'method'    => 'POST',
            'body'      => wp_json_encode($payload),
            'timeout'   => 30,
            'sslverify' => false,
            'headers'	=> array('X-token'=>$token,'content-type'=>'application/json'),
        ) );

        if ( is_wp_error( $response ) ){
			$customer_order->add_order_note('Smile Payment Error 100');
			error_log('ERROR 100' . print_r($response, true));
			if($is_cashpayment) return "";
            throw new Exception( __( 'We are currently experiencing problems trying to connect to this payment gateway. Sorry for the inconvenience. [ERROR 100]', 'wcspg-smile-payment' ) );

        }
        if ( empty( $response['body'] ) ){
			$customer_order->add_order_note('Smile Payment Error 200');
			error_log('ERROR 200');
			if($is_cashpayment) return "";
            throw new Exception( __( 'We are currently experiencing problems trying to connect to this payment gateway. Sorry for the inconvenience. [ERROR 200]', 'wcspg-smile-payment' )  );

        }
        // Retrieve the body's resopnse if no errors found
        $response_body = wp_remote_retrieve_body( $response );
        $response_array = json_decode($response_body, true);
		write_Log('Is cashpayment = ' . $is_cashpayment);
        write_log($response_array);
        if(isset($response_array['errorCode']) || !isset($response_array['sale']) || (!$is_cashpayment && !isset($response_array['sale']['paymentGatewayURLData']))){
        	error_log('ERROR 300');
        	$customer_order->add_order_note('Smile Payment Error 300 : ' . print_r($response_array, true));
			if($is_cashpayment) return "";
            throw new Exception( __( 'We are currently experiencing problems trying to connect to this payment gateway. Sorry for the inconvenience. [ERROR 300] ['.print_r($response_array, true).']', 'wcspg-smile-payment' ) );
        }
		
		// save SaleId with order
		$saleId = $response_array['sale']['saleId'];
		update_post_meta( $customer_order->get_id(), '_smile_saleId', $saleId );
		$customer_order->add_order_note('Smile Payment Redirect ' . print_r($response_array['sale'], true));
        return $response_array['sale'];
    }

    /**
     * Get payment status info.
     */
    function get_payment_status_information($token){

        // callback is called with OrderID and key, so check status with API
        // TODO: get payment complete, so API is not called twice
        
        $saleId = $_REQUEST['saleId']; // get_post_meta($orderId, '_smile_saleId', true);
        
        write_log("Get payment status from SMILE API");
        $response = wp_remote_get( $this->environment_url . $saleId,
            array(	            'timeout'   => 30,
                'sslverify' => false,
                'headers'	=> array('X-token'=>$token,'content-type'=>'application/json')));

        if ( is_wp_error( $response ) )
            throw new Exception( __( 'We are currently experiencing problems trying to check your payment. Sorry for the inconvenience. [ERROR 100]', 'wcspg-smile-payment' ) );

        if ( empty( $response['body'] ) )
            throw new Exception( __( 'We are currently experiencing problems trying to check your payment. Sorry for the inconvenience. [ERROR 200]', 'wcspg-smile-payment' )  );

        // Retrieve the body's resopnse if no errors found
        $response_body = wp_remote_retrieve_body( $response );
        $response_array = json_decode($response_body, true);
		write_log(print_r($response_array, true));
        return $response_array;
    }

    /**
     * Get token, so that we can start making requests with it.
     */
    function get_token( ){

        write_log('in get_token');
	    $token = WC()->session->get( 'API_token');
		if($token == null){
			// try to get token from API
			$username = SMILE_API_USERNAME;
			$password = SMILE_API_PASSWORD;
			$token_url = SMILE_API_URL . 'tokens?' . 'username='. $username . '&password=' . $password .'&srav=1.0';
			$response = wp_remote_get($token_url,array(
		            'timeout'   => 30,
		            'sslverify' => false)
				);

	        if ( is_wp_error( $response ) )
	            throw new Exception( __( 'We are currently experiencing problems trying to connect to the token API. Sorry for the inconvenience.', 'wcspg-smile-payment' ) );

	        if ( empty( $response['body'] ) )
	            throw new Exception( __( 'API\'s Response was empty.', 'wcspg-smile-payment' ) );

	        // Retrieve the body's resopnse if no errors found
	        $response_body = wp_remote_retrieve_body( $response );
			$response_array = json_decode($response_body, true);
			write_log($response_array);
			if(isset($response_array['token']) && isset($response_array['token']['tokenUUID'])){
				return $response_array['token']['tokenUUID'];
			}
		}
		return null;
    }

	/**
	 * Get bundled products to create SubSaleLines
	 */
	 // TODO: move to other class
	function get_bundled_products($productId){
		$subSaleLines = array();
		$bundledProductIds = get_post_meta($productId, 'smile_bundled_products_ids');
		if(is_array($bundledProductIds) && count($bundledProductIds) && count($bundledProductIds[0])){
			// found at least 1 bundled product
			write_log($bundledProductIds[0]);
			foreach($bundledProductIds[0] as $bundledProductId){
				$productSerialNumber = $this::UNKNOWN;
				if(get_post_meta($bundledProductId, '_product_serial_number')) {
					$productSerialNumberValues = get_post_meta($bundledProductId, '_product_serial_number');
					if(count($productSerialNumberValues)){
						$productSerialNumber = $productSerialNumberValues[0];
					}
				}

				$productItemNumber = $this::UNKNOWN;
				if(get_post_meta($bundledProductId, '_product_item_number')) {
					$productItemNumberValues = get_post_meta($bundledProductId, '_product_item_number');
					if(count($productItemNumberValues)){
						$productItemNumber = $productItemNumberValues[0];
					}
				}		
				$subSaleLines[] = 	array('inventoryItem' => 
										array('serialNumber' => $productSerialNumber, 'itemNumber' =>  $productItemNumber)
									);
			}
		}
		return $subSaleLines;
	}
}
